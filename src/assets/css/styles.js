import { StyleSheet, Dimensions } from "react-native";
const styles = StyleSheet.create({
  // Flex
  flex1: {
    flex: 1,
  },
  w100: {
    width: "100%",
  },
  h100: {
    height: "100%",
  },
  // Flex
  // flex direction
  fdrjcsb:{
    flexDirection:'row',
    justifyContent:'space-between'
  },
  // flex direction
  // Font Sizes
  fs55: {
    fontSize: 55,
  },
  fs45: {
    fontSize: 45,
  },
  fs50: {
    fontSize: 50,
  },
  fs40: {
    fontSize: 40,
  },
  fs35: {
    fontSize: 35,
  },
  fs30: {
    fontSize: 30,
  },
  fs25: {
    fontSize: 25,
  },
  fs22: {
    fontSize: 22,
  },
  fs20: {
    fontSize: 20,
  },
  fs18: {
    fontSize: 18,
  },
  fs16: {
    fontSize: 16,
  },
  fs14: {
    fontSize: 14,
  },
  fs12: {
    fontSize: 12,
  },
  fs10: {
    fontSize: 10,
  },
  // Font Sizes
  // Font Families
  ffb: {
    fontFamily: "UberMoveText-Bold",
  },
  ffl: {
    fontFamily: "UberMoveText-Light",
  },
  ffr: {
    fontFamily: "UberMoveText-Regular",
  },
  ffm: {
    fontFamily: "UberMoveText-Medium",
  },
  // Font Families
  // Colors

  colorwhite: {
    color: "#ffffff",
  },
  colorblack: {
    color: "#1a1a1a",
  },
  colorgrey: {
    color: "#f2f2f2",
  },
  colorlightblack: {
    color: "#666666",
  },
  coloryellow: {
    color: "#f9af16",
  },
  colorred: {
    color: "#ff2d00",
  },
  // Colors
  // Background Colors
  bgcolorgrey: {
    backgroundColor: "#f2f2f2",
  },
  bgcoloryellow: {
    backgroundColor: "#f0ad34",
  },
  // Background Colors
  // line Height
  lh5: {
    lineHeight: 5,
  },
  lh10: {
    lineHeight: 10,
  },
  lh15: {
    lineHeight: 15,
  },
  lh20: {
    lineHeight: 20,
  },
  lh25: {
    lineHeight: 25,
  },
  lh30: {
    lineHeight: 30,
  },
  // line Height
  // border Radius
  br5: {
    borderRadius: 5,
  },
  br8: {
    borderRadius: 8,
  },
  br10: {
    borderRadius: 10,
  },
  br12: {
    borderRadius: 12,
  },
  br15: {
    borderRadius: 15,
  },
  br18: {
    borderRadius: 18,
  },
  br20: {
    borderRadius: 20,
  },
  br25: {
    borderRadius: 25,
  },
  br30: {
    borderRadius: 30,
  },
  br30: {
    borderRadius: 30,
  },
  br35: {
    borderRadius: 35,
  },
  br40: {
    borderRadius: 40,
  },
  br45: {
    borderRadius: 45,
  }
  ,br50: {
    borderRadius: 50,
  },
  br55: {
    borderRadius: 55,
  },
  br60: {
    borderRadius: 60,
  },
  // border Radius

  // Padding 
  p5:{ padding:5 },
  p10:{ padding:10 },
  p15:{ padding:15 },
  p20:{ padding:20 },
  p25:{ padding:25 },
  p30:{ padding:30 },
  p35:{ padding:35 },
  p40:{ padding:40 },
  p45:{ padding:45 },
  // Padding 
  // paddingRight 
  pr5:{ paddingRight:5 },
  pr10:{ paddingRight:10 },
  pr15:{ paddingRight:15 },
  pr20:{ paddingRight:20 },
  pr25:{ paddingRight:25 },
  pr30:{ paddingRight:30 },
  pr35:{ paddingRight:35 },
  pr40:{ paddingRight:40 },
  pr45:{ paddingRight:45 },
  // paddingRight
  // paddingTop 
  pt5:{ paddingTop:5 },
  pt10:{ paddingTop:10 },
  pt15:{ paddingTop:15 },
  pt20:{ paddingTop:20 },
  pt25:{ paddingTop:25 },
  pt30:{ paddingTop:30 },
  pt35:{ paddingTop:35 },
  pt40:{ paddingTop:40 },
  pt45:{ paddingTop:45 },
  // paddingTop
  // paddingBottom 
  pb5:{ paddingBottom:5 },
  pb10:{ paddingBottom:10 },
  pb15:{ paddingBottom:15 },
  pb20:{ paddingBottom:20 },
  pb25:{ paddingBottom:25 },
  pb30:{ paddingBottom:30 },
  pb35:{ paddingBottom:35 },
  pb40:{ paddingBottom:40 },
  pb45:{ paddingBottom:45 },
  // paddingBottom
  // paddingLeft 
  pl5:{ paddingLeft:5 },
  pl10:{ paddingLeft:10 },
  pl15:{ paddingLeft:15 },
  pl20:{ paddingLeft:20 },
  pl25:{ paddingLeft:25 },
  pl30:{ paddingLeft:30 },
  pl35:{ paddingLeft:35 },
  pl40:{ paddingLeft:40 },
  pl45:{ paddingLeft:45 },
  // paddingLeft


  // margin 
  m10:{ margin:10 },
  m15:{ margin:15 },
  m20:{ margin:20 },
  m25:{ margin:25 },
  m30:{ margin:30 },
  m35:{ margin:35 },
  m40:{ margin:40 },
  m45:{ margin:45 },
  // margin
  // paddingRight 
  mr5:{ marginRight:5 },
  mr10:{ marginRight:10 },
  mr15:{ marginRight:15 },
  mr20:{ marginRight:20 },
  mr25:{ marginRight:25 },
  mr30:{ marginRight:30 },
  mr35:{ marginRight:35 },
  mr40:{ marginRight:40 },
  mr45:{ marginRight:45 },
  // marginRight
  // marginTop 
  mt5:{ marginTop:5 },
  mt10:{ marginTop:10 },
  mt15:{ marginTop:15 },
  mt20:{ marginTop:20 },
  mt25:{ marginTop:25 },
  mt30:{ marginTop:30 },
  mt35:{ marginTop:35 },
  mt40:{ marginTop:40 },
  mt45:{ marginTop:45 },
  // marginTop
  // marginBottom 
  mb5:{ marginBottom:5 },
  mb10:{ marginBottom:10 },
  mb15:{ marginBottom:15 },
  mb20:{ marginBottom:20 },
  mb25:{ marginBottom:25 },
  mb30:{ marginBottom:30 },
  mb35:{ marginBottom:35 },
  mb40:{ marginBottom:40 },
  mb45:{ marginBottom:45 },
  // marginBottom
  // marginLeft 
  ml5:{ marginLeft:5 },
  ml10:{ marginLeft:10 },
  ml15:{ marginLeft:15 },
  ml20:{ marginLeft:20 },
  ml25:{ marginLeft:25 },
  ml30:{ marginLeft:30 },
  ml35:{ marginLeft:35 },
  ml40:{ marginLeft:40 },
  ml45:{ marginLeft:45 },
  // marginLeft


  // Shadow of box
  shadow: {
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  shadowdark: {
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 10,
  },
  // shadow of box

  Container: {
    flex: 1,
    padding: 16,
  },
  mapcontainer: {
    flex: 1,
  },
  map: {
    flex: 1,
  },
  cardview: {},
  WelcomeContent: {
    flex: 0.9,
    alignItems: "center",
    justifyContent: "flex-end",
    paddingHorizontal: 32,
  },
  LoginContainer: {
    paddingHorizontal: 40,
  },
  LoginContent:{
    paddingTop: 20,
  },
  RegisterContent: {
    flex: 1,
    padding: 48,
  },
  VerifyContent: {
    flex: 1,
    padding: 48,
  },
  borderStyleBase: {
    width: 40,
    height: 65,
    backgroundColor: "red",
  },
  borderStyleHighLighted: {
    borderColor: "red",
  },
  underlineStyleBase: {
    width: 30,
    height: 45,
    borderWidth: 0,
    borderBottomWidth: 1,
  },
  underlineStyleHighLighted: {
    borderColor: "grey",
  },
  // Welcome screen CSS
  headingwelcome: {
    paddingVertical: 8,
  },
  descwelcome: {
    paddingVertical: 20,
    textAlign: "center",
  },
  // Welcome screen CSS
  // Help Screen CSS
  cardhelp: {
    padding: 20,
    marginLeft:10,
    marginRight:10,
    backgroundColor: "#fff",
    borderRadius: 20,
    shadowColor: "#000",
    marginBottom: 20,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
  shadow2: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
  },
  shadow5: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    borderRadius: 40,
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  // Help Screen CSS
  inputfeild: {
    flexDirection: "row",
    backgroundColor:'#fff',
    width:'100%',
    borderColor: "#d8dfe4",
    borderWidth: 1,
    height: 60,
    borderRadius: 30,
    alignItems: "center",
    marginBottom: 5,
    marginTop:10,
    overflow:'hidden'
  },
  icon: {
    marginLeft: 12,
  },
  errorField: {
    fontSize: 14,
    color: "red",
    fontFamily: "Roboto-Regular",
  },
  inputfeildtxt: {
    height: 40,
    width: "80%",
    fontSize: 16,
    marginLeft: 20,
    color: "#31363b",
    fontFamily: "Roboto-Regular",
  },
  dropdown: {
    width: (Dimensions.get('screen').width-130)/2,
    borderWidth: 0,
    marginTop: -15,
    paddingLeft: 10,
  },
  Dropdown: {
    fontSize: 16,
    fontFamily: "Roboto-Regular",
    color:'#31363b'
  },
  title: {
    color: '#191919', fontWeight: "bold", fontSize: 26, marginTop: 24
  },
  titlecare: {
    color: '#f9af16', fontWeight: "bold", fontSize: 16 
  },
  description: {
      color: '#191919', fontSize: 18, marginTop: 8
  },
  menuview: {
    padding: 15,
    paddingLeft: 0,
    paddingRight: 0,
    flexDirection:'row',
    justifyContent:'space-between',
    alignContent:'center',
    alignItems:'center'
  },
  menutxt: {
    color: "#1a1a1a",
    fontSize: 14,
    fontFamily: "UberMoveText-Bold",
    
  },
  menutxtteam:{
    color: "#1a1a1a",
    fontSize: 12,
    fontFamily: "UberMoveText-Medium",
  },
  menutxt2: {
    color: "#999999",
    fontSize: 11,
    fontFamily: "UberMoveText-Medium",
    marginTop:4
  },
  faqsview: {
    padding: 13,
    paddingLeft: 0,
    paddingRight: 0,
    flexDirection:'row',
    justifyContent:'space-between',
    alignContent:'center',
    alignItems:'center',
    borderBottomWidth:1,
    borderBottomColor:'#ddd'
  },
  faqstxt:{
    color: "#1a1a1a",
    fontSize: 14,
    fontFamily: "UberMoveText-Medium",
  },
  faqstxt2:{
    color: "#1a1a1a",
    fontSize: 17,
    fontFamily: "UberMoveText-Bold",
    lineHeight:25,
    marginBottom:20,
    paddingRight:30
  },
  faqsdetailview: {
    padding: 13,
    paddingLeft: 0,
    paddingRight: 0,
  },
  faqsdetailtxt:{
    color: "#1a1a1a",
    fontSize: 14,
    fontFamily: "UberMoveText-Bold",
  },
  faqsdetailtxt2:{
    color: "#1a1a1a",
    fontSize: 20,
    fontFamily: "UberMoveText-Bold",
    // lineHeight:25,
    paddingRight:30
  },
  faqsdetailtxt3:{
    color: "#1a1a1a",
    fontSize: 14,
    fontFamily: "UberMoveText-Regular",
    // marginTop:10
  },
  bookmarkview:{
    padding: 15,
    paddingLeft: 0,
    paddingRight: 0,
    flexDirection:'row',
    alignContent:'center',
    alignItems:'center',
    borderBottomColor:'#d9d9d9',borderBottomWidth:1,paddingBottom:20
  },
  bookmarktxt:{
    color: "#1a1a1a",
    fontSize: 12,
    fontFamily: "UberMoveText-Bold",
    paddingRight:30,
    
  },
  bookmarktxt2:{
    color: "#1a1a1a",
    fontSize: 10,
    fontFamily: "UberMoveText-Regular",
    marginTop:8
  },
  bookmarktxt3:{
    color: "#1a1a1a",
    fontSize: 15,
    fontFamily: "UberMoveText-Bold",
    marginTop:27
  },
  bookmarkimg:{
    borderRadius:12,
    overflow: 'hidden',
  },
  chatconv:{
    color: "#666666",
    fontSize: 10,
    fontFamily: "UberMoveText-Bold",
  },
  chatview:{
    padding: 15,
    paddingLeft: 0,
    paddingRight: 0,
    flexDirection:'row',
    alignContent:'center',
    alignItems:'center',
  },
  chatimg:{
    borderRadius:25,
    overflow: 'hidden',
  },
  chattxt:{
    color: "#1a1a1a",
    fontSize: 14,
    fontFamily: "UberMoveText-Bold",
    // lineHeight:20
  },
  chattxt2:{
    color: "#999999",
    fontSize: 11,
    fontFamily: "UberMoveText-Bold",
    lineHeight:14
  },
  chattxt3:{
    color: "#1a1a1a",
    fontSize: 9,
    fontFamily: "UberMoveText-Bold"
  },
  homeview:{
    backgroundColor:'#f7f7f7',
    flexDirection:'column',
    width:'48%',
    flexWrap:'wrap',
    borderRadius:6,
    overflow:'hidden',
    marginBottom:15
  },
  homeview2:{
    backgroundColor:'#ddd',
    flexDirection:'column',
    width:'48%',
    height:225,
    flexWrap:'wrap',
    justifyContent:'center',
    alignContent:'center',
    alignItems:'center',
    borderRadius:6,
    overflow:'hidden',marginBottom:15
  },
  hometxt:{
    color: "#1a1a1a",
    fontSize: 9,
    fontFamily: "UberMoveText-Regular",
    marginTop:8
  },
  hometxt2:{
    color: "#1a1a1a",
    fontSize: 12,
    fontFamily: "UberMoveText-Medium",
    marginTop:5
  },
  hometxt3:{
    color: "#1a1a1a",
    fontSize: 15,
    fontFamily: "UberMoveText-Bold",
    marginTop:5,paddingBottom:10
  },
  homeimg:{
    overflow:'hidden'
  },
  hometxtfilter:{
    color: "#1a1a1a",
    fontSize: 11,
    fontFamily: "UberMoveText-Bold",
    paddingHorizontal:14,
    paddingVertical:8
  },
  homefilterview:{
    borderColor:'#333333',
    borderWidth:1,
    marginTop:15,
    borderRadius:16,
    marginRight:5,
    flexDirection:'row',
    justifyContent:'center',
    alignContent:'center',
    alignSelf:'center',
    alignItems:'center'
  },
  chatviewsender:{
    backgroundColor:'#f9af16',
    marginBottom:5,
    paddingHorizontal:10,
    paddingVertical:10,
    borderRadius:10,
    maxWidth:250,
    alignSelf: 'flex-end'
  },
  chatviewreciever:{
    backgroundColor:'#ccc',
    marginBottom:5,
    paddingHorizontal:10,
    paddingVertical:10,
    borderRadius:10,
    maxWidth:250,
    alignSelf: 'flex-start'
  },
  chatviewtxt:{
    fontSize:14,
    fontFamily:'UberMoveText-Medium'
  },
  chatviewdate:{
    fontSize:10,
    fontFamily:'UberMoveText-Regular'
  },
  backgroundVideo: {
    width:230,
    height:230
  },
});
export default styles;
