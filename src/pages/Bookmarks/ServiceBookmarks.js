import React, { Component } from "react";
import {
  Text,
  View,
  SafeAreaView,
  Image,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  RefreshControl,
} from "react-native";
import styles from "../../assets/css/styles";
import axios from 'axios';
import BackButtonComponent from "../../components/BackButtonComponent";
export default class ServiceBookmarks extends Component {
    constructor(props) {
        super(props);
        this.state = {
          bookmarkdata:[],
          refreshing:false,
        };
        this.getBookmarks()
        this.props.navigation.addListener('tabPress', e => {
          this.getBookmarks()
        });
      }
      reload=()=>{
        this.getBookmarks()
      }
      getBookmarks=()=>{
        this.setState({refreshing:true})
        var header={
          "Content-Type":"application/json",
          "Authorization":global.usertoken
        }
        axios.get( global.url + '/api/bookmark/service?limit=99&offset=0',{headers:header}).then(response => {
          console.log(response.data.result.data);
          this.setState({
            bookmarkdata:response.data.result.data
          })
          this.setState({refreshing:false})
        }).catch(error => {
          this.setState({refreshing:false})
          console.log(error.response);
        });
      }
      componentDidMount() {
        this.props.navigation.addListener('didFocus', this.onScreenFocus)
      }
      onScreenFocus = () => {
        this.getBookmarks()
      }
      render() {
        return (
          <SafeAreaView style={{ flex: 1,backgroundColor: '#fff',}}>
            <View style={{flexDirection:"row"}}>
              <BackButtonComponent navigation={this.props.navigation}/>
              <View  style={{flexDirection:"row" ,width:Dimensions.get('screen').width-100,justifyContent:'center'}}>
                  <Text style={[styles.menutxt,styles.mt20, styles.fs16]}>{global.lang=='en'?'Service Bookmarks':'بُک مارکس'}</Text>
              </View>
            </View>
            <View style={{ padding:24,paddingTop:0}}>
              <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{paddingBottom:100}}>
                  {this.state.bookmarkdata.map((item,index)=>{
                  return(
                    <TouchableOpacity style={styles.bookmarkview} key={index} onPress={()=>{this.props.navigation.navigate('ServiceDetail',{data:item.Service})}}>
                      <Image source={{uri:item.Service.resources[0].awsUrl}} style={[styles.bookmarkimg,{ width: 94, height: 94 }]} resizeMode={'cover'}/>
                      <View style={{marginLeft:10}}>
                        <Text style={styles.bookmarktxt}>{item.Service.ServiceCategory.name}</Text>
                        <Text style={styles.bookmarktxt2}>{item.Service.Shop.name}</Text>
                        <Text style={styles.bookmarktxt2} numberOfLines={2} >{item.Service.description}</Text>
                      </View>
                    </TouchableOpacity>
                  )
                })}
              </ScrollView>
            </View>
          </SafeAreaView>
        );
      }
}
