
import React from 'react'
import {Text, Image, Linking,View, TouchableOpacity,ScrollView,KeyboardAvoidingView,Dimensions,SafeAreaView } from 'react-native'
import OTPInputView from '@twotalltotems/react-native-otp-input'
import styles from '../../assets/css/styles';
import Phone from '../../assets/images/phone.png';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import BackArrow from '../../assets/images/backarrow.png';
import messaging from '@react-native-firebase/messaging';
export default class OtpVerification extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      code:'',
      phone:this.props.route.params.phone,
      otp:'',
      timer:false,
      loading:false
    };
    this.handleNotification();
    console.log(this.props.route.params.phone);
    console.log(this.props.route.params.role);
    setTimeout(() => {
      this.setState({timer:true})
    }, 30000);
  }
  handleNotification = () => {
    messaging().onMessage(async (remoteMessage) => {
      if(remoteMessage?.data?.code){
        console.log('code',remoteMessage);
        this.setState({loading:true})
        this.setState({code:remoteMessage?.data?.code})
        setTimeout(() => {
          if(this.state.code){
            this.verifyOTP();
          }
        }, 500);
      }
    });
  };
  verifyOTP=()=>{
    this.setState({loading:true})
    var header={
      'Content-Type':'application/json'
    }
    var body={
      "code": this.state.code,
      "phoneNumber":this.props.route.params.phone,
      "deviceId": new Date().getTime().toString(),
      "fbToken":global.fbtoken
    }
    console.log(body);
    axios.post( global.url + '/api/user/verify-otp',body,{headers:header} ).then(async (response) => {
      this.setState({code:''})
      console.log(response.data.result.data.token);
      try {
        await AsyncStorage.setItem('userToken', response.data.result.data.token);
        await AsyncStorage.setItem('userRole', this.props.route.params.role);
        global.usertoken = response.data.result.data.token;
        global.userrole = this.props.route.params.role;
        if(this.props.route.params.role=='buyer'){
          this.props.navigation.replace('Buyer')
        }
        else{
          var header={
            "Authorization":response.data.result.data.token
          }
          axios.get( global.url + '/api/user/self',{headers:header} ).then(response => {
            console.log(response.data);
            if(response.data.result.data.Shop==null){
              this.props.navigation.replace('SellerProfile',{phone:response.data.result.data.phoneNumber});
            }
            else{
              this.props.navigation.replace('Seller')
            }
          }).catch(error => {
            console.log(error.response);
          });
        }
        this.setState({loading:false})
      } catch (e) {
        this.setState({loading:false})
        alert('Error in saving session');
      }
    }).catch(error => {
      alert(error.response.data.message);
      this.setState({code:''})
      this.setState({loading:false})
      console.log(error.response);
    });
  }
  resendOTP=()=>{
    var header={
      'Content-Type':'application/json'
    }
    var body={
      "phoneNumber": this.props.route.params.phone
    }
    console.log(body);
    axios.post( global.url + '/api/user/resend-otp',body,{headers:header} ).then(response => {
      console.log(response.data);
      this.setState({timer:false});
      setTimeout(() => {
        this.setState({timer:true})
      }, 30000);
      this.setState({code:''})
    }).catch(error => {
      console.log(error);
    });
  }
  onCall=()=>{
    Linking.openURL(`tel:03100040003`)
  }
  render() {
    return (
      <KeyboardAvoidingView style={{flex:1}} enabled={Platform.OS == "ios"} behavior={Platform.OS == "ios" ? "padding" : null} 
        keyboardVerticalOffset={40}>
        <SafeAreaView style={{ flex: 1 }}>
          <View style={{flexDirection:"row",justifyContent:'space-between',marginRight:20}}>
            <TouchableOpacity style={{ marginTop: 15 , marginLeft:20}} onPress={()=>{this.props.navigation.navigate('RoleSelection')}}>
                <Image source={BackArrow} style={{ width: 32, height: 32,marginLeft:-5 }} resizeMode={'contain'}/>
            </TouchableOpacity>
            <TouchableOpacity style={{ marginTop: 25 , marginLeft:20,flexDirection:"row" }}  onPress={()=>{this.onCall()}}>
                <Text style={[styles.titlecare,{marginRight:15}]}>{global.lang=='en'?'Customer Care':'صارفین کی سہولت'}</Text>
                <Image source={Phone} style={{ width: 24, height: 24 }} resizeMode={'contain'}/>
            </TouchableOpacity>
          </View>
            <ScrollView style={{padding:20}}>
                <Text style={styles.title}>{global.lang=='en'?'Number Verification':'نمبر کی تصدیق'}</Text>

                <Text style={styles.description}>{global.lang=='en'?'Enter 6 digit code sent to':'بھیجے گئے چھ ہندسوں کے کوڈ کا اندراج کریں'} +{this.state.phone}</Text>
                <View style={{ width: Dimensions.get('window').width-40, marginTop: 20,marginLeft:20,marginBottom:10}}>
                    <OTPInputView
                    style={{ width: Dimensions.get('window').width-80, height: 100 }}
                    pinCount={6}
                    code={this.state.code}
                    editable={true}
                    onCodeChanged={(code) => {
                      this.setState({ code:code });
                    }}
                    autoFocusOnLoad={true}
                    returnKeyType="done"
                    codeInputFieldStyle={{
                      width: 45,
                      height: 60,
                      borderWidth: 0,
                      color: "#000000",
                      backgroundColor:'#f2f2f2',
                      fontSize: 25,
                    }}
                    codeInputHighlightStyle={{
                      borderColor: "grey",
                      borderWidth:1,
                      color: "black",
                      fontSize: 25,
                    }}
                  />
                </View>
                {/* <Text style={[styles.description,{fontSize: 16,color: '#ff7272'}]}>*Note : Converted numbers don't recieved OTP</Text> */}
            </ScrollView>
            <View style={{ flexDirection: "row", alignItems: "center",justifyContent: "space-between",paddingRight:15,paddingBottom:20,marginLeft:20 }}>
                <View style={{flexDirection:"column",justifyContent:"space-between"}}>    
                    <Text style={[styles.ffm,{ color: '#191919', fontSize: 14, fontWeight: "bold",marginBottom:5 }]}>
                    {global.lang=='en'?"Didn't receive the code ?":'کوڈ موصول نہیں ہوا؟'}
                    </Text>
                    <View style={{flexDirection:'row'}}>
                      {(this.state.timer) && (
                        <TouchableOpacity onPress={()=>{this.resendOTP()}}>
                          <Text style={[styles.ffm,{ color: '#f9af16', fontSize: 13, fontWeight: "600",paddingTop:1 }]}>{global.lang=='en'?"Resend Code":'کوڈ دوبارہ حاصل کریں'}</Text>
                        </TouchableOpacity>
                      )}
                    </View>
                </View>
                <TouchableOpacity onPress={() => {this.verifyOTP()}}
                    disabled={this.state.loading} 
                    style={{backgroundColor:'#f9af16', borderRadius:50, width:56, height:56, 
                    justifyContent:"center", alignItems:"center" }}>
                    <Image source={require('../../assets/images/arrowright.png')} style={{ width:16, height:15 }} resizeMode={'contain'}/>
                </TouchableOpacity>
            </View>
        </SafeAreaView>
      </KeyboardAvoidingView>
    );
  }
}
