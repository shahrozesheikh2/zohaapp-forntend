import React, { Component } from 'react';
import {
  Text,
  View,
  SafeAreaView,
  Image,
  TouchableOpacity,
  ScrollView,
  TextInput,
  Dimensions,
  FlatList,
  KeyboardAvoidingView,
  Linking,
  Alert
} from "react-native";
import io from "socket.io-client";
import styles from "../../assets/css/styles";
import BackButtonComponent from '../../components/BackButtonComponent';
import axios from 'axios';
import RBSheet from "react-native-raw-bottom-sheet";
import ImagePicker from 'react-native-customized-image-picker';
import Video from 'react-native-video';
import moment from 'moment';
import GetLocation from 'react-native-get-location';
import openMap from 'react-native-open-maps';
import { Overlay } from 'react-native-elements';
import ImageResizer from 'react-native-image-resizer';
// import AudioRecorderPlayer from 'react-native-audio-recorder-player';
// const audioRecorderPlayer = new AudioRecorderPlayer();
const options = {
  maxWidth: 500,
  maxHeight: 500,
  quality: 1
};
const optionsvideo = {
  mediaType: 'video',
  videoQuality:'low',
  durationLimit:30
};

export default class ChatDetail extends Component {
  socket;
  constructor(props) {
    super(props);
    this.state = {
      chatMessage: "",
      chatMessages: [],
      role:'',
      sendId:'',
      receiveId:'',
      chatroomId:'',
      startChatId:'',
      buyerName:'',
      sellerName:'',
      buyerUrl:'',
      sellerUrl:'',
      asset:'',
      image:"",
      video:"",
      showOverlay:false,
      showOverlayV:false,
      paused:false,
      sellerPhone:'',
      BuyerPhone:'',
      blocked:false,
      blockedbyother:false
    };
    console.log('====================================');
    console.log(this.props.route.params?.data);
    console.log(this.props.route.params?.startChatId);
    console.log('====================================');
    this.getuserdata();
    // this.audioRecorderPlayer = new AudioRecorderPlayer();
    // this.audioRecorderPlayer.setSubscriptionDuration(0.09);
  }
  toggleOverlay = () => {
    this.setState({showOverlay:!this.state.showOverlay})
  }
  toggleOverlayV = () => {
    this.setState({showOverlayV:!this.state.showOverlayV})
  }
  getuserdata=()=>{
    var header={
      "Authorization":global.usertoken
    }
    axios.get( global.url + '/api/user/self',{headers:header} ).then(response => {
      console.log(response.data.result.data.role);
      if(response.data.result.data.role=="Buyer"){
        this.setState({role:response.data.result.data.role});
        if(this.props.route.params?.fromChat==false){
          this.setState({startChatId:this.props.route.params?.data?.userId});
        }
        else{
          this.setState({startChatId:this.props.route.params?.startChatId});
        }
      }
      else{
        this.setState({role:"Seller"});
        if(this.props.route.params?.fromChat==false){
          this.setState({startChatId:this.props.route.params?.data?.id});
        }
        else{
          this.setState({startChatId:this.props.route.params?.startChatId});
        }
      }
      setTimeout(() => {
        this.connectSocket()
      }, 200);
    }).catch(error => {
      console.log(error.response);
    });
  }
  onCall=()=>{
    Linking.openURL(`tel:+${this.state.role=='Buyer'?this.state.sellerPhone:this.state.BuyerPhone}`)
  }
  onCallReport=()=>{
    Linking.openURL(`tel:03100040003`)
  }
  blockUser=()=>{
    var header={
      "Authorization":global.usertoken
    }
    Alert.alert(
      this.state.blocked?"Unblock User":'Block user',
      this.state.blocked?"Do you want to unblock this user?":"Do you want to block this user?",
      [
          {
            text: "Cancel",
            onPress: ()=>{}
            },
            {
            text: this.state.blocked?"Unblock":'Block',
            onPress: () => {
              axios.patch( global.url + '/api/user/toggle-block/'+this.state.sendId,{},{headers:header} ).then(response => {
                console.log(response.data);
                this.props.navigation.goBack();
              }).catch(error => {
                console.log(error.response);
              });
            }
          }
      ],
      {cancelable: true}
    );
    
  }
//Chat Socket functions
  componentWillUnmount(){
    this.socket.disconnect();
  }
  componentDidMount() {
    var usertoken= global.usertoken.replace('Bearer ', '');
    this.socket = io(global.url, {
      reconnectionDelayMax: 10000,
      query: {
        token: usertoken
      },
      reconnection: true,
    });
    console.log('====================================');
    console.log(usertoken);
    console.log('====================================');
    this.socket.on('connect_error', err => {
      console.log(' : ', 'connect_error');
      console.log('err: ', err);
      console.log('err: ', err.stack);
      console.log('err: ', err.message);
    });
    this.socket.on('connect_failed', err => {
      console.log(' : ', 'connect_failed');
      console.log('err: ', err);
    });
    this.socket.on('disconnect', err => {
      console.log('  ', 'disconnect');
      console.log('err: ', err);
    });
    
    this.socket.on('connect_timeout', function (err) {
      console.log("client connect_timeout: ", err);
    });

    this.socket.on('connection', (data, callback) => {
      console.log('data: ', data);
      console.log(`I'm connected with the back-end`);
      this.socket.on('receive-message', (data) => {
        console.log('receive-message:', data);
        if(data.chatroomId==this.state.chatroomId && data.message.userChatDetailId==this.state.receiveId){
          console.log('inside');
          this.state.chatMessages.unshift(data.message);
          this.markRead(this.state.chatroomId,this.state.sendId,this.state.receiveId);
        }
        this.setState({extra:true})
      });
      // this.socket.on('receive-read', (data) => {
      //   console.log('receive-read: ', data);
      // });
      // this.socket.on('receive-chat', (data) => {
      //   console.log('receive-chat: ', data);
      // });
      // this.socket.on('receive-unread', (data) => {
      //   console.log('receive-unread: ', data);
      // });
      callback({
        status: 200
      })
    });
  }
  connectSocket=()=>{
    if(this.props.route.params?.fromChat==false){
      this.startChat();
    }
    else{
      this.getChatRoom(this.props.route.params?.startChatId);
    }
  }
  sendMsg=(msg)=>{
    this.setState({chatMessage:''})
    this.socket.emit('send-message', { chatroomId: this.state.chatroomId, userChatDetailId:this.state.sendId,"partnerUserId":this.state.receiveId, message: msg }, (response) => {
      console.log('send.message: ', response.result.data.message);
      this.state.chatMessages.unshift(response.result.data.message);
      setTimeout(() => {
        this.setState({extra:true});
      }, 50);
    });
  }
  startChat=()=>{
    console.log('===============startChatId=====================');
    console.log(this.state.startChatId);
    console.log('================startChatId====================');
    this.socket.emit('start-chat', { id: this.state.startChatId }, (response) => {
      console.log('response.result: ', response);
      this.getChatRoom(response.result.data.id);
    });
  }
  startTyping=()=>{
    this.socket.emit('start-typing', { chatroomId: this.state.chatroomId, userChatDetailId:this.state.sendId,"partnerUserId":this.state.receiveId }, (response) => {
      console.log('response.result: ', response);
    });
  }
  stopTyping=()=>{
    socket.emit('stop-typing', { chatroomId: this.state.chatroomId, userChatDetailId:this.state.sendId,"partnerUserId":this.state.receiveId }, (response) => {
    	console.log('response.result: ', response);
    });
  }
  markRead=(id,userid,userid2)=>{
    this.socket.emit('mark-read', { "chatroomId": id,"userChatDetailId": userid,"partnerUserId":userid2}, (response) => {
      console.log('markRead', response);
    });
  }
  getChatRoom=(id)=>{
    this.socket.emit('get-chatroom', { id: id, limit: 50 } , (response) => {
      console.log('response.get-chatroom: ', response);
      this.setState({chatroomId:response.result.data.id});
      this.setState({buyerName:response?.result?.data?.User1?.User?.fullName});
      this.setState({sellerName:response?.result?.data?.User2?.User?.Shop.name});
      this.setState({sellerPhone:response?.result?.data?.User2?.User?.Shop.phoneNumber});
      this.setState({BuyerPhone:response?.result?.data?.User1?.User?.phoneNumber});
      this.setState({buyerUrl:response?.result?.data?.User1?.User?.awsUrl});
      this.setState({sellerUrl:response.result.data.User2.User.awsUrl});
      
      if(this.state.role=="Buyer"){
        this.setState({sendId:response.result.data.userChatDetailId1});
        this.setState({receiveId:response.result.data.userChatDetailId2});
        this.setState({blocked:response.result.data.User1.blocked!=0});
        this.setState({blockedbyother:response.result.data.User2.blocked!=0});
        
      }
      else{
        this.setState({sendId:response.result.data.userChatDetailId2});
        this.setState({receiveId:response.result.data.userChatDetailId1});
        this.setState({blocked:response.result.data.User2.blocked!=0});
        this.setState({blockedbyother:response.result.data.User1.blocked!=0});

      }
      var temp=[]
      response.result.data.Messages.map((item)=>{
        temp.push(item);
      })
      setTimeout(() => {
        this.markRead(id,this.state.sendId,this.state.receiveId);
        this.setState({chatMessages:temp});
      }, 50);
    });
  }
//Chat Socket functions




  // Custom items in chat
  launchcamera = () => {
    setTimeout(() => {
      ImagePicker.openCamera({
        width: 500,
        height: 500,
        cropping: false
      }).then(response => {
        console.log(response);
        ImageResizer.createResizedImage(response[0].path, 500, 500,'JPEG', 100).then(res => {
          this.updatepic(res.uri);
        }).catch(err => {
          console.log(err);
          alert('this image is not allowed')
        });
      });
    }, 1000);
  };
  launchcameraVideo = () => {
    setTimeout(() => {
      ImagePicker.openPicker({
        imageLoader: 'UNIVERSAL',
        isCamera:true,
        isVideo:true,
        videoQuality:0
      }).then(response => {
        console.log(response);
        this.updatevid(response[0].path);
      });
    }, 1000);
  };
  opengallery = () => {
    setTimeout(() => {
      ImagePicker.openPicker({imageLoader: 'UNIVERSAL',isCamera:true, multiple: false}).then(response => {
        console.log(response);
        ImageResizer.createResizedImage(response[0].path, 500, 500,'JPEG', 100).then(res => {
          this.updatepic(res.uri);
        }).catch(err => {
          console.log(err);
          alert('this image is not allowed')
        });
      });
    }, 1000);
    
  };
  updatepic = (image) => {
    const config = {
      headers: {
        "Content-Type": "multipart/form-data"
      }
    };
    var formdata = new FormData();
    formdata.append("file", {
      uri: image,
      type: "image/jpg",
      name: new Date().getTime()+"image.jpg"
    });
    const url = global.url + "/api/upload-resource";
    axios.post(url, formdata, config ).then((response) => {
      console.log(response.data);
      var msg='@image:'+response.data.result.data[0].path;
      this.sendMsg(msg);
    }).catch((err) => {
      console.log(err.response);
    });
  };
  updatevid = (image) => {
    const config = { headers: { "Content-Type": "multipart/form-data" } };
    var formdata = new FormData();
    formdata.append("file", {
      uri: image,
      type: "video/mp4",
      name: new Date().getTime()+"video.mp4"
    });
    const url = global.url + "/api/upload-resource";
    axios.post(url, formdata, config ).then((response) => {
      console.log(response.data);
      var msg='@video:'+response.data.result.data[0].path;
      this.sendMsg(msg);
    }).catch((err) => {
      console.log(err.response);
    });
  };
  //custom icons in chat

  // get your location
  getLocation=()=>{
    GetLocation.getCurrentPosition({
      enableHighAccuracy: true,
      timeout: 15000,
    }).then(location => {
        console.log(location);
        var msg='@location:'+JSON.stringify(location)
        this.sendMsg(msg);
    }).catch(error => {
        const { code, message } = error;
        console.warn(code, message);
        alert('Turn on your location');
    })
  }
  openMap=(msg)=>{
    msg = msg.replace('@location:', '');
    msg=JSON.parse(msg);
    openMap({
      latitude: msg.latitude,
      longitude: msg.longitude,
      provider:'google',
      travelType:'drive',
      navigate_mode:'preview'
    });
  }
  // get your location


  //Audio Chat
  // onStartRecord = async () => {
  //   const result = await this.audioRecorderPlayer.startRecorder();
  //   this.audioRecorderPlayer.addRecordBackListener((e) => {
  //     this.setState({
  //       recordSecs: e.current_position,
  //       recordTime: this.audioRecorderPlayer.mmssss(
  //         Math.floor(e.current_position),
  //       ),
  //     });
  //     return;
  //   });
  //   console.log(result);
  // };
  // onStopRecord = async () => {
  //   const result = await this.audioRecorderPlayer.stopRecorder();
  //   this.audioRecorderPlayer.removeRecordBackListener();
  //   this.setState({
  //     recordSecs: 0,
  //   });
  //   console.log(result);
  // };
  // onStartPlay = async () => {
  //   console.log('onStartPlay');
  //   const msg = await this.audioRecorderPlayer.startPlayer();
  //   console.log(msg);
  //   this.audioRecorderPlayer.addPlayBackListener((e) => {
  //     this.setState({
  //       currentPositionSec: e.current_position,
  //       currentDurationSec: e.duration,
  //       playTime: this.audioRecorderPlayer.mmssss(Math.floor(e.current_position)),
  //       duration: this.audioRecorderPlayer.mmssss(Math.floor(e.duration)),
  //     });
  //     return;
  //   });
  // };
  // onPausePlay = async () => {
  //   await this.audioRecorderPlayer.pausePlayer();
  // };
  // onStopPlay = async () => {
  //   console.log('onStopPlay');
  //   this.audioRecorderPlayer.stopPlayer();
  //   this.audioRecorderPlayer.removePlayBackListener();
  // };
  //Audio Chat



  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={{flexDirection:"row",justifyContent:'space-between',paddingRight:20,borderBottomWidth: 1, borderBottomColor: 'rgb(230,230,230)',paddingBottom:10}}>
          <BackButtonComponent navigation={this.props.navigation}/>
          <View  style={{flexDirection:"row",justifyContent:'center'}}>
            {(this.state.role=='Buyer' && this.state.sellerUrl!='') && (
              <Image source={{uri:this.state.sellerUrl}} style={{ width: 40, height: 40,borderRadius:30,marginTop:10,marginRight:10,borderColor:'#ccc',borderWidth:1 }} resizeMode={'cover'}/>
            )}
            {(this.state.role!='Buyer' && this.state.buyerUrl!='') &&(
            <Image source={{uri:this.state.buyerUrl}} style={{ width: 40, height: 40,borderRadius:30,marginTop:10,marginRight:10,borderColor:'#ccc',borderWidth:1 }} resizeMode={'cover'}/>
            )}
            <Text numberOfLines={1} style={[styles.menutxt,styles.mt20, styles.fs16,{maxWidth:150}]}>{this.state.role=='Buyer'?this.state.sellerName:this.state.buyerName}</Text>
          </View>
          {(!this.state.blockedbyother) && (
            <TouchableOpacity style={{ marginTop: 20 , marginLeft:0 }} onPress={() => this.RBSheet.open()}>
              <Image source={require('../../assets/images/chaticons/dotmenu.png')} style={{ width: 24, height: 24 }} resizeMode={'contain'}/>
            </TouchableOpacity>
          )}
          {(this.state.blockedbyother) && (
            <View style={{ marginTop: 20 , marginLeft:0,width: 24, height: 24 }}>
              {/* <Image source={require('../../assets/images/chaticons/dotmenu.png')} style={{ width: 24, height: 24 }} resizeMode={'contain'}/> */}
            </View>
          )}
          
        </View>

        <FlatList
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
          // onEndReachedThreshold={0.01}
          // onEndReached={() => {
          //   this.loadMoreResults(this.state.offset+1);
          //   this.setState({offset:this.state.offset+1});
          // }}
          contentContainerStyle={{padding:10}}
          inverted={true}
          keyExtractor={(item,index) => index + ''}
          data={this.state.chatMessages}
          renderItem={({ item, index }) => {
            return (
              <View key={index}>
                {(item.userChatDetailId==this.state.sendId) && (
                  <View style={[styles.chatviewsender]}>
                    {(item.message.match('@image:'))&&(
                      <TouchableOpacity onPress={()=>{this.setState({showOverlay:true,image:item.message.replace('@image:', '')});}}>
                        <Image source={{uri:item.message.replace('@image:', '')}} style={{ width: 230, height: 230 }} resizeMode={'cover'}/>
                      </TouchableOpacity>
                    )}
                    {(item.message.match('@video:'))&&(
                      <TouchableOpacity onPress={()=>{this.setState({showOverlayV:true,video:item.message.replace('@video:', '')});}} style={{position:'relative'}}>
                        <View style={{ zIndex:99999,position: 'absolute',left:0,top:0 ,right:0,bottom:0,backgroundColor:'#0000003c'}} >
                          <Image source={require('../../assets/images/video.png')} style={{ width: 70, height: 70,alignSelf:'center',marginTop:80}} resizeMode={'cover'}/>
                        </View>
                        <Video resizeMode={'cover'} source={{uri: item.message.replace('@video:', '')}} paused={this.state.paused}
                          onLoad={() => {
                            this.setState({
                              paused: true
                            });
                          }}
                        style={[styles.backgroundVideo,{zIndex:0}]} />
                      </TouchableOpacity>
                    )}
                    {(item.message.match('@location:'))&&(
                    <TouchableOpacity onPress={()=>{this.openMap(item.message)}}>
                      <Image source={require('../../assets/images/chaticons/map.png')} style={{ width: 230, height: 230 }} resizeMode={'cover'}/>
                    </TouchableOpacity>
                    )}
                    {(!item.message.match('@image:') && (!item.message.match('@video:')) && (!item.message.match('@location:')) )&&(
                      <Text style={[styles.chatviewtxt]}>{item.message}</Text>
                    )}
                    <Text style={[styles.chatviewdate,{textAlign:'right',marginTop:4}]}>{moment(item.createdAt).fromNow()}</Text>
                  </View>
                )}


                {(item.userChatDetailId==this.state.receiveId) && (
                  <View style={[styles.chatviewreciever]}>
                  {(item.message.match('@image:'))&&(
                      <TouchableOpacity onPress={()=>{this.setState({showOverlay:true,image:item.message.replace('@image:', '')});}}>
                        <Image source={{uri:item.message.replace('@image:', '')}} style={{ width: 230, height: 230 }} resizeMode={'cover'}/>
                      </TouchableOpacity>
                    )}
                    {(item.message.match('@video:'))&&(
                      <TouchableOpacity onPress={()=>{this.setState({showOverlayV:true,video:item.message.replace('@video:', '')});}} style={{position:'relative'}}>
                        <View style={{ zIndex:99999,position: 'absolute',left:0,top:0 ,right:0,bottom:0,backgroundColor:'#0000003c'}} >
                          <Image source={require('../../assets/images/video.png')} style={{ width: 70, height: 70,alignSelf:'center',marginTop:80}} resizeMode={'cover'}/>
                        </View>
                        <Video resizeMode={'cover'} source={{uri: item.message.replace('@video:', '')}} 
                        style={[styles.backgroundVideo,{zIndex:0}]} />
                      </TouchableOpacity>
                    )}
                    {(item.message.match('@location:'))&&(
                    <TouchableOpacity onPress={()=>{this.openMap(item.message)}}>
                      <Image source={require('../../assets/images/chaticons/map.png')} style={{ width: 230, height: 230 }} resizeMode={'cover'}/>
                    </TouchableOpacity>
                    )}
                    {(!item.message.match('@image:') && (!item.message.match('@video:')) && (!item.message.match('@location:')) )&&(
                      <Text style={[styles.chatviewtxt]}>{item.message}</Text>
                    )}
                    <Text style={[styles.chatviewdate,{textAlign:'right',marginTop:4}]}>{moment(item.createdAt).fromNow()}</Text>
                </View>
                )}
              </View>
            );
          }}
        />

        {(!this.state.blocked) &&(
          <>
          {(!this.state.blockedbyother) &&(
            <View style={{ borderWidth: 0.5, borderColor: '#ddd', width: Dimensions.get('screen').width, height: 56,flexDirection: "row", alignItems: "center"}}>
              <TextInput style={{ width: Dimensions.get('screen').width-72,height:56, backgroundColor:'#DEDBDB',
                paddingHorizontal: 10, fontSize: 13,color:'black'}}
                  value={this.state.chatMessage}
                  onChangeText={(msg)=>this.setState({chatMessage:msg})}
                  placeholder={'Write here ...'}
                  placeholderTextColor={'#7c7c7c'}
              />
              <TouchableOpacity style={{ width: 72,height:56, justifyContent: "center", alignItems: "center", }} onPress={()=>{this.sendMsg(this.state.chatMessage)}}>
                  <Image source={require('../../assets/images/chaticons/send.png')} style={{ width: 49, height: 33 }} 
                  resizeMode={'contain'}/>
              </TouchableOpacity>
            </View>
          )}
          
          </>
        )}
        {(this.state.blocked) &&(
          <View style={{ borderWidth: 0.5, borderColor: '#ddd', width: Dimensions.get('screen').width, height: 56,flexDirection: "row", alignItems: "center"}}>
            <Text style={[styles.ffb,styles.fs16,styles.colorred,styles.p20,{textAlign:'center'}]}>{global.lang=='en'?'You Blocked this User.':'آپ نے اس صارف کو مسدود کردیا'}</Text>  
        </View>
        )}
        {(this.state.blockedbyother) &&(
          <View style={{ borderWidth: 0.5, borderColor: '#ddd', width: Dimensions.get('screen').width, height: 56,flexDirection: "row", alignItems: "center"}}>
            <Text style={[styles.ffb,styles.fs16,styles.colorred,styles.p20,{textAlign:'center'}]}>{global.lang=='en'?'You were Blocked by this User.':'آپ کو اس صارف کے ذریعہ مسدود کردیا گیا تھا'}</Text>  
        </View>
        )}
        
        <RBSheet ref={ref => { this.RBSheet = ref; }} height={460} openDuration={250}  >
            <View>
              <View style={{ borderBottomColor:'#d9d9d9',borderBottomWidth:1,position: 'relative', }}>
                <Text style={[styles.ffb,styles.fs16,styles.colorblack,styles.p20,{textAlign:'center'}]}>{global.lang=='en'?'Select Option':'آپشن منتخب کریں'}</Text>
                <View style={{position: 'absolute',right:0}}>
                  <TouchableOpacity onPress={() => this.RBSheet.close()}>
                    <Text style={[styles.ffb,styles.fs16,styles.coloryellow,styles.p20,{textAlign:'center'}]}>{global.lang=='en'?'Close':'بند کریں'}</Text>
                  </TouchableOpacity>
                </View>
              </View>
              <ScrollView style={{padding:20}}>
                {/* <TouchableOpacity onPress={() => {this.RBSheet.close();this.launchcamera();}} style={{flexDirection:'row',paddingBottom:15,alignItems:'center'}}>
                  <Image source={require('../../assets/images/chaticons/camera.png')} style={{ width: 34, height: 34 }} resizeMode={'contain'}/>
                  <Text style={[styles.ffm,styles.fs14,styles.colorblack,styles.pl10]}>{global.lang=='en'?'Camera':'کیمرہ'}</Text>
                </TouchableOpacity> */}
                <TouchableOpacity onPress={() => {this.RBSheet.close();this.opengallery();}} style={{flexDirection:'row',paddingBottom:15,alignItems:'center'}}>
                  <Image source={require('../../assets/images/chaticons/gallery.png')} style={{ width: 34, height: 34 }} resizeMode={'contain'}/>
                  <Text style={[styles.ffm,styles.fs14,styles.colorblack,styles.pl10]}>{global.lang=='en'?'Camera/Gallery':'گیلری'}</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => {this.RBSheet.close();this.launchcameraVideo();}} style={{flexDirection:'row',paddingBottom:15,alignItems:'center'}}>
                  <Image source={require('../../assets/images/chaticons/video.png')} style={{ width: 34, height: 34 }} resizeMode={'contain'}/>
                  <Text style={[styles.ffm,styles.fs14,styles.colorblack,styles.pl10]}>{global.lang=='en'?'Video':'ویڈیو'}</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => {this.RBSheet.close();this.getLocation();}} style={{flexDirection:'row',paddingBottom:15,alignItems:'center'}}>
                  <Image source={require('../../assets/images/chaticons/location.png')} style={{ width: 34, height: 34 }} resizeMode={'contain'}/>
                  <Text style={[styles.ffm,styles.fs14,styles.colorblack,styles.pl10]}>{global.lang=='en'?'Location':'مقام'}</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => {this.RBSheet.close();this.onCall();}} style={{flexDirection:'row',paddingBottom:15,alignItems:'center'}}>
                  <Image source={require('../../assets/images/chaticons/phone.png')} style={{ width: 34, height: 34 }} resizeMode={'contain'}/>
                  <Text style={[styles.ffm,styles.fs14,styles.colorblack,styles.pl10]}>{global.lang=='en'?'Call':'کال کریں'}</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => {this.RBSheet.close();this.onCallReport();}} style={{flexDirection:'row',paddingBottom:15,alignItems:'center'}}>
                  <Image source={require('../../assets/images/chaticons/report.png')} style={{ width: 34, height: 34 }} resizeMode={'contain'}/>
                  <Text style={[styles.ffm,styles.fs14,styles.colorred,styles.pl10]}>{global.lang=='en'?'Report':'رپورٹ کریں'}</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => {this.RBSheet.close();this.blockUser();}} style={{flexDirection:'row',paddingBottom:15,alignItems:'center'}}>
                  <Image source={require('../../assets/images/chaticons/block.png')} style={{ width: 34, height: 34 }} resizeMode={'contain'}/>
                  
                  {(!this.state.blocked) &&(
                    <Text style={[styles.ffm,styles.fs14,styles.colorred,styles.pl10]}>{global.lang=='en'?'Block':'بلاک کریں'}</Text>
                  )}
                  {(this.state.blocked) &&(
                    <Text style={[styles.ffm,styles.fs14,styles.colorred,styles.pl10]}>{global.lang=='en'?'Unblock':'مسدود کریں'}</Text>
                  )}
                  
                </TouchableOpacity>
              </ScrollView>
            </View>
          </RBSheet>
        <Overlay overlayStyle={{padding:0,borderRadius:8,}} isVisible={this.state.showOverlay} onBackdropPress={this.toggleOverlay}>
          <View style={{width:Dimensions.get('screen').width,height:Dimensions.get('screen').height,overflow: 'hidden',backgroundColor:'black'}}>
            <View style={{flexDirection:'row',justifyContent:'flex-end',marginTop:50,marginLeft:30}}>
              <TouchableOpacity onPress={()=>{this.toggleOverlay()}} >
                <Image source={require('../../assets/images/close.png')} style={[{ width: 30, height: 30}]} resizeMode={'contain'}/>
              </TouchableOpacity>
            </View>
            <Image source={{uri:this.state.image}} style={[{ width: Dimensions.get('screen').width, height: Dimensions.get('screen').height-90}]} resizeMode={'contain'}/>
          </View>
        </Overlay>
        {(this.state.showOverlayV) && (
          <View style={{width:Dimensions.get('screen').width,height:Dimensions.get('screen').height,position: 'absolute',backgroundColor:'black'}}>
            <View style={{flexDirection:'row',justifyContent:'flex-end'}}>
              <TouchableOpacity onPress={()=>{this.setState({showOverlayV:false})}} >
                <Image source={require('../../assets/images/close.png')} style={[{ width: 30, height: 30,margin:20}]} resizeMode={'contain'}/>
              </TouchableOpacity>
            </View>
            <Video source={{uri: this.state.video}} controls={true} resizeMode={'cover'} style={{position: 'absolute',
              top: 100,
              left: 0,
              bottom: 150,
              right: 0,}}
            />
          </View>
        )}
      </SafeAreaView>
    
    );
  }
}
