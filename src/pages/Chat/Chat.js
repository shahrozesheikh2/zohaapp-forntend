import React, { Component } from "react";
import {
  Text,
  View,
  SafeAreaView,
  Image,
  TouchableOpacity,
  ScrollView,
  RefreshControl,
} from "react-native";
import styles from "../../assets/css/styles";
import axios from 'axios';
import messaging from '@react-native-firebase/messaging';
export default class Chat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      chatMessages: [],
      role:'',
      refreshing:false,
   };
    this.handleNotification();
    this.getuserdata();
    this.props.navigation.addListener('tabPress', e => {
      this.getchats();
    });
  }
  handleNotification = () => {
    messaging().onMessage(async (remoteMessage) => {
      
      if(!remoteMessage?.data?.code){
        console.log('getchats',remoteMessage);
        this.getchats();
      }
    });
  };
  componentWillUnmount(){
    this._unsubscribe();
  }
  componentDidMount() {
    this._unsubscribe = this.props.navigation.addListener('focus', () => {
      this.getchats();
    });
  }
  reload=()=>{
    this.getchats();
  }
  getuserdata=()=>{
    var header={ "Authorization":global.usertoken }
    axios.get( global.url + '/api/user/self',{headers:header} ).then(response => {
      console.log(response.data);
      if(response.data.result.data.role=="Buyer"){
        this.setState({role:"Buyer"});
      }
      else{
        this.setState({role:"Seller"});
      }
      this.getchats();
    }).catch(error => {
      this.setState({refreshing:false})
      console.log(error.response);
    });
  }
  getchats=()=>{
    console.log('getchats');
    this.setState({refreshing:true})
    var header={
      "Content-Type":"application/json",
      "Authorization":global.usertoken
    }
    axios.get( global.url + '/api/chat-self?offset=0&limit=999',{headers:header}).then(response => {
      console.log(response.data);
      this.setState({chatMessages:response.data.result.data});
      this.setState({refreshing:false})
    }).catch(error => {
      console.log(error.response.data.message);
      alert(error.response.data.message);
      this.setState({refreshing:false})
    });
  }
  
  render() {
    return (
      <SafeAreaView style={{ flex: 1,backgroundColor: '#fff',}}>
        <View style={{ padding:24,paddingTop:0}}>
          <Text style={[styles.title,styles.mb30]}>{global.lang=='en'?'My Chats':'میرے چیٹس'}</Text>
          <Text style={[styles.chatconv ]}>{this.state.chatMessages.length} {global.lang=='en'?'Conversations':'گفتگو'}</Text>
          <ScrollView 
          contentContainerStyle={{paddingBottom:100}}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
            refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={()=>{this.reload()}}
            />
            }>
            {this.state.chatMessages.map((item,index)=>{
              return(
              <TouchableOpacity key={index} style={styles.chatview} onPress={()=>{
                if(this.state.role=="Buyer" && item?.User2?.blocked!=0){
                  alert('You were blocked by user');
                }
                else if(this.state.role!="Buyer" && item.User1?.blocked!=0){
                  alert('You were blocked by user');
                }
                else{
                  this.props.navigation.navigate("ChatDetail",{fromChat:true,startChatId:item.id})
                }
                }}> 
                <Image source={{uri:this.state.role=="Buyer"?item.User2.User.awsUrl:item.User1.User.awsUrl}} style={[styles.chatimg,{ width: 50, height: 50 }]} resizeMode={'cover'}/>
                <View style={{marginLeft:10,width:'70%'}}>
                  <Text style={styles.chattxt}>{this.state.role=="Buyer"?item?.User2?.User?.Shop?.name:item.User1?.User?.fullName} <Text style={{color:'red',marginLeft:10}}> {item?.User2?.blocked!=0?'(Blocked)':item.User1?.blocked!=0?'(Blocked)':''}</Text> </Text>
                  <Text style={styles.chattxt2}>{item.Messages[0]?.message.match('@image:')?'Photo':item.Messages[0]?.message.match('@video:')?'Video':item.Messages[0]?.message.match('@location:')?'Location':item.Messages[0]?.message}</Text>
                </View>
                {(this.state.role=="Buyer" && item.User1.unread!=0) && (
                  <View style={[styles.bgcoloryellow,{marginLeft:10,padding: 7,borderRadius:9}]}>
                    <Text style={styles.chattxt3}>{item.User1.unread}</Text>
                  </View>
                )}
                {(this.state.role!="Buyer" && item.User2.unread!=0) && (
                  <View style={[styles.bgcoloryellow,{marginLeft:10,padding: 7,borderRadius:9}]}>
                    <Text style={styles.chattxt3}>{item.User2.unread}</Text>
                  </View>
                )}
              </TouchableOpacity>
              )
            })}
          </ScrollView>
        </View>
      </SafeAreaView>
    );
  }
}
