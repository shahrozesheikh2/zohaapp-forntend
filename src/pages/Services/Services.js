import React from 'react'
import { SafeAreaView, TextInput, TouchableOpacity, RefreshControl,Text, Image, View, ScrollView, FlatList } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import styles from '../../assets/css/styles';
import BackArrow from '../../assets/images/backarrow.png'
import Search from '../../assets/images/search.png';
import axios from 'axios';
import RBSheet from "react-native-raw-bottom-sheet";
import FastImage from 'react-native-fast-image';
import EmergencyBtn from '../../components/EmergencyBtn';
import GetLocation from 'react-native-get-location';
export default class Services extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          productdata:[],
          categorydata:[],
          branddata:[],
          modeldata:[],
          refreshing:false,
          city:'',
          cityid:'',
          citiesdata:[
            {
              "id":462,
              "cityName":"Rawalpindi",
              "countryId":1,
              "createdAt":"2021-05-20T21:31:34.000Z",
              "updatedAt":"2021-05-20T21:31:34.000Z",
              "deletedAt":null
            },
            {
              "id":467,
              "cityName":"Islamabad",
              "countryId":1,
              "createdAt":"2021-05-20T21:32:04.000Z",
              "updatedAt":"2021-05-20T21:32:04.000Z",
              "deletedAt":null
            },
            {
              "id":460,
              "cityName":"Lahore",
              "countryId":1,
              "createdAt":"2021-05-20T21:31:22.000Z",
              "updatedAt":"2021-05-20T21:31:22.000Z",
              "deletedAt":null
            },
            {
              "id":464,
              "cityName":"Peshawar",
              "countryId":1,
              "createdAt":"2021-05-20T21:31:47.000Z",
              "updatedAt":"2021-05-20T21:31:47.000Z",
              "deletedAt":null
            },
            {
              "id":468,
              "cityName":"Quetta",
              "countryId":1,
              "createdAt":"2021-05-20T21:32:10.000Z",
              "updatedAt":"2021-05-20T21:32:10.000Z",
              "deletedAt":null
            },
            {
              "id":459,
              "cityName":"Karachi",
              "countryId":1,
              "createdAt":"2021-05-20T21:31:14.000Z",
              "updatedAt":"2021-05-20T21:31:14.000Z",
              "deletedAt":null
            },
            {
            "id":500,
            "cityName":"Abbotabad",
            "countryId":1,
            "createdAt":"2021-05-20T21:35:25.000Z",
            "updatedAt":"2021-05-20T21:35:25.000Z",
            "deletedAt":null
            },
            {
            "id":529,
            "cityName":"Ahmedpur East",
            "countryId":1,
            "createdAt":"2021-05-20T21:37:27.000Z",
            "updatedAt":"2021-05-20T21:37:27.000Z",
            "deletedAt":null
            },
            {
            "id":548,
            "cityName":"Arif Wala",
            "countryId":1,
            "createdAt":"2021-05-20T21:39:02.000Z",
            "updatedAt":"2021-05-20T21:39:02.000Z",
            "deletedAt":null
            },
            {
            "id":521,
            "cityName":"Attock",
            "countryId":1,
            "createdAt":"2021-05-20T21:36:52.000Z",
            "updatedAt":"2021-05-20T21:36:52.000Z",
            "deletedAt":null
            },
            {
            "id":547,
            "cityName":"Badin",
            "countryId":1,
            "createdAt":"2021-05-20T21:38:58.000Z",
            "updatedAt":"2021-05-20T21:38:58.000Z",
            "deletedAt":null
            },
            {
            "id":514,
            "cityName":"Bahawalnagar",
            "countryId":1,
            "createdAt":"2021-05-20T21:36:23.000Z",
            "updatedAt":"2021-05-20T21:36:23.000Z",
            "deletedAt":null
            },
            {
            "id":469,
            "cityName":"Bahawalpur",
            "countryId":1,
            "createdAt":"2021-05-20T21:32:18.000Z",
            "updatedAt":"2021-05-20T21:32:18.000Z",
            "deletedAt":null
            },
            {
            "id":546,
            "cityName":"Bhakkar",
            "countryId":1,
            "createdAt":"2021-05-20T21:38:55.000Z",
            "updatedAt":"2021-05-20T21:38:55.000Z",
            "deletedAt":null
            },
            {
            "id":559,
            "cityName":"Bhalwal",
            "countryId":1,
            "createdAt":"2021-05-20T21:40:02.000Z",
            "updatedAt":"2021-05-20T21:40:02.000Z",
            "deletedAt":null
            },
            {
            "id":493,
            "cityName":"Burewala",
            "countryId":1,
            "createdAt":"2021-05-20T21:34:45.000Z",
            "updatedAt":"2021-05-20T21:34:45.000Z",
            "deletedAt":null
            },
            {
            "id":525,
            "cityName":"Chakwal",
            "countryId":1,
            "createdAt":"2021-05-20T21:37:08.000Z",
            "updatedAt":"2021-05-20T21:37:08.000Z",
            "deletedAt":null
            },
            {
            "id":536,
            "cityName":"Chaman",
            "countryId":1,
            "createdAt":"2021-05-20T21:38:04.000Z",
            "updatedAt":"2021-05-20T21:38:04.000Z",
            "deletedAt":null
            },
            {
            "id":545,
            "cityName":"Charsadda",
            "countryId":1,
            "createdAt":"2021-05-20T21:38:49.000Z",
            "updatedAt":"2021-05-20T21:38:49.000Z",
            "deletedAt":null
            },
            {
            "id":487,
            "cityName":"Chiniot",
            "countryId":1,
            "createdAt":"2021-05-20T21:34:11.000Z",
            "updatedAt":"2021-05-20T21:34:11.000Z",
            "deletedAt":null
            },
            {
            "id":519,
            "cityName":"Chishtian",
            "countryId":1,
            "createdAt":"2021-05-20T21:36:43.000Z",
            "updatedAt":"2021-05-20T21:36:43.000Z",
            "deletedAt":null
            },
            {
            "id":512,
            "cityName":"Dadu",
            "countryId":1,
            "createdAt":"2021-05-20T21:36:16.000Z",
            "updatedAt":"2021-05-20T21:36:16.000Z",
            "deletedAt":null
            },
            {
            "id":553,
            "cityName":"Daharki",
            "countryId":1,
            "createdAt":"2021-05-20T21:39:35.000Z",
            "updatedAt":"2021-05-20T21:39:35.000Z",
            "deletedAt":null
            },
            {
            "id":510,
            "cityName":"Daska",
            "countryId":1,
            "createdAt":"2021-05-20T21:36:07.000Z",
            "updatedAt":"2021-05-20T21:36:07.000Z",
            "deletedAt":null
            },
            {
            "id":478,
            "cityName":"Dera Ghazi Khan",
            "countryId":1,
            "createdAt":"2021-05-20T21:33:31.000Z",
            "updatedAt":"2021-05-20T21:33:31.000Z",
            "deletedAt":null
            },
            {
            "id":497,
            "cityName":"Dera Ismail Khan",
            "countryId":1,
            "createdAt":"2021-05-20T21:35:09.000Z",
            "updatedAt":"2021-05-20T21:35:09.000Z",
            "deletedAt":null
            },
            {
            "id":461,
            "cityName":"Faisalabad",
            "countryId":1,
            "createdAt":"2021-05-20T21:31:29.000Z",
            "updatedAt":"2021-05-20T21:31:29.000Z",
            "deletedAt":null
            },
            {
            "id":524,
            "cityName":"Ferozwala",
            "countryId":1,
            "createdAt":"2021-05-20T21:37:04.000Z",
            "updatedAt":"2021-05-20T21:37:04.000Z",
            "deletedAt":null
            },
            {
            "id":549,
            "cityName":"Ghotki",
            "countryId":1,
            "createdAt":"2021-05-20T21:39:08.000Z",
            "updatedAt":"2021-05-20T21:39:08.000Z",
            "deletedAt":null
            },
            {
            "id":511,
            "cityName":"Gojra",
            "countryId":1,
            "createdAt":"2021-05-20T21:36:11.000Z",
            "updatedAt":"2021-05-20T21:36:11.000Z",
            "deletedAt":null
            },
            {
            "id":463,
            "cityName":"Gujranwala",
            "countryId":1,
            "createdAt":"2021-05-20T21:31:42.000Z",
            "updatedAt":"2021-05-20T21:31:42.000Z",
            "deletedAt":null
            },
            {
            "id":479,
            "cityName":"Gujrat",
            "countryId":1,
            "createdAt":"2021-05-20T21:33:35.000Z",
            "updatedAt":"2021-05-20T21:33:35.000Z",
            "deletedAt":null
            },
            {
            "id":490,
            "cityName":"Hafizabad",
            "countryId":1,
            "createdAt":"2021-05-20T21:34:22.000Z",
            "updatedAt":"2021-05-20T21:34:22.000Z",
            "deletedAt":null
            },
            {
            "id":552,
            "cityName":"Haroonabad",
            "countryId":1,
            "createdAt":"2021-05-20T21:39:31.000Z",
            "updatedAt":"2021-05-20T21:39:31.000Z",
            "deletedAt":null
            },
            {
            "id":544,
            "cityName":"Hasilpur",
            "countryId":1,
            "createdAt":"2021-05-20T21:38:39.000Z",
            "updatedAt":"2021-05-20T21:38:39.000Z",
            "deletedAt":null
            },
            {
            "id":509,
            "cityName":"Hub",
            "countryId":1,
            "createdAt":"2021-05-20T21:36:03.000Z",
            "updatedAt":"2021-05-20T21:36:03.000Z",
            "deletedAt":null
            },
            {
            "id":466,
            "cityName":"Hyderabad",
            "countryId":1,
            "createdAt":"2021-05-20T21:32:00.000Z",
            "updatedAt":"2021-05-20T21:32:00.000Z",
            "deletedAt":null
            },
            
            {
            "id":503,
            "cityName":"Jacobabad",
            "countryId":1,
            "createdAt":"2021-05-20T21:35:39.000Z",
            "updatedAt":"2021-05-20T21:35:39.000Z",
            "deletedAt":null
            },
            {
            "id":518,
            "cityName":"Jaranwala",
            "countryId":1,
            "createdAt":"2021-05-20T21:36:40.000Z",
            "updatedAt":"2021-05-20T21:36:40.000Z",
            "deletedAt":null
            },
            {
            "id":551,
            "cityName":"Jatoi",
            "countryId":1,
            "createdAt":"2021-05-20T21:39:27.000Z",
            "updatedAt":"2021-05-20T21:39:27.000Z",
            "deletedAt":null
            },
            {
            "id":477,
            "cityName":"Jhang",
            "countryId":1,
            "createdAt":"2021-05-20T21:33:23.000Z",
            "updatedAt":"2021-05-20T21:33:23.000Z",
            "deletedAt":null
            },
            {
            "id":504,
            "cityName":"Jhelum",
            "countryId":1,
            "createdAt":"2021-05-20T21:35:43.000Z",
            "updatedAt":"2021-05-20T21:35:43.000Z",
            "deletedAt":null
            },
            {
            "id":542,
            "cityName":"Kabal",
            "countryId":1,
            "createdAt":"2021-05-20T21:38:30.000Z",
            "updatedAt":"2021-05-20T21:38:30.000Z",
            "deletedAt":null
            },
            {
            "id":527,
            "cityName":"Kamalia",
            "countryId":1,
            "createdAt":"2021-05-20T21:37:19.000Z",
            "updatedAt":"2021-05-20T21:37:19.000Z",
            "deletedAt":null
            },
            {
            "id":556,
            "cityName":"Kamber Ali Khan",
            "countryId":1,
            "createdAt":"2021-05-20T21:39:49.000Z",
            "updatedAt":"2021-05-20T21:39:49.000Z",
            "deletedAt":null
            },
            {
            "id":489,
            "cityName":"Kāmoke",
            "countryId":1,
            "createdAt":"2021-05-20T21:34:18.000Z",
            "updatedAt":"2021-05-20T21:34:18.000Z",
            "deletedAt":null
            },
            {
            "id":558,
            "cityName":"Kandhkot",
            "countryId":1,
            "createdAt":"2021-05-20T21:39:57.000Z",
            "updatedAt":"2021-05-20T21:39:57.000Z",
            "deletedAt":null
            },
            
            {
            "id":483,
            "cityName":"Kasur",
            "countryId":1,
            "createdAt":"2021-05-20T21:33:56.000Z",
            "updatedAt":"2021-05-20T21:33:56.000Z",
            "deletedAt":null
            },
            {
            "id":506,
            "cityName":"Khairpur",
            "countryId":1,
            "createdAt":"2021-05-20T21:35:51.000Z",
            "updatedAt":"2021-05-20T21:35:51.000Z",
            "deletedAt":null
            },
            {
            "id":495,
            "cityName":"Khanewal",
            "countryId":1,
            "createdAt":"2021-05-20T21:34:54.000Z",
            "updatedAt":"2021-05-20T21:34:54.000Z",
            "deletedAt":null
            },
            {
            "id":505,
            "cityName":"Khanpur",
            "countryId":1,
            "createdAt":"2021-05-20T21:35:47.000Z",
            "updatedAt":"2021-05-20T21:35:47.000Z",
            "deletedAt":null
            },
            {
            "id":539,
            "cityName":"Khushab",
            "countryId":1,
            "createdAt":"2021-05-20T21:38:17.000Z",
            "updatedAt":"2021-05-20T21:38:17.000Z",
            "deletedAt":null
            },
            {
            "id":507,
            "cityName":"Khuzdar",
            "countryId":1,
            "createdAt":"2021-05-20T21:35:55.000Z",
            "updatedAt":"2021-05-20T21:35:55.000Z",
            "deletedAt":null
            },
            {
            "id":494,
            "cityName":"Kohat",
            "countryId":1,
            "createdAt":"2021-05-20T21:34:49.000Z",
            "updatedAt":"2021-05-20T21:34:49.000Z",
            "deletedAt":null
            },
            {
            "id":523,
            "cityName":"Kot Abdul Malik",
            "countryId":1,
            "createdAt":"2021-05-20T21:37:00.000Z",
            "updatedAt":"2021-05-20T21:37:00.000Z",
            "deletedAt":null
            },
            {
            "id":530,
            "cityName":"Kot Addu",
            "countryId":1,
            "createdAt":"2021-05-20T21:37:30.000Z",
            "updatedAt":"2021-05-20T21:37:30.000Z",
            "deletedAt":null
            },
            {
            "id":488,
            "cityName":"Kotri",
            "countryId":1,
            "createdAt":"2021-05-20T21:34:15.000Z",
            "updatedAt":"2021-05-20T21:34:15.000Z",
            "deletedAt":null
            },
            
            {
            "id":474,
            "cityName":"Larkana",
            "countryId":1,
            "createdAt":"2021-05-20T21:33:06.000Z",
            "updatedAt":"2021-05-20T21:33:06.000Z",
            "deletedAt":null
            },
            {
            "id":533,
            "cityName":"Layyah",
            "countryId":1,
            "createdAt":"2021-05-20T21:37:49.000Z",
            "updatedAt":"2021-05-20T21:37:49.000Z",
            "deletedAt":null
            },
            {
            "id":543,
            "cityName":"Lodhran",
            "countryId":1,
            "createdAt":"2021-05-20T21:38:34.000Z",
            "updatedAt":"2021-05-20T21:38:34.000Z",
            "deletedAt":null
            },
            {
            "id":501,
            "cityName":"Mandi Bahauddin",
            "countryId":1,
            "createdAt":"2021-05-20T21:35:30.000Z",
            "updatedAt":"2021-05-20T21:35:30.000Z",
            "deletedAt":null
            },
            {
            "id":532,
            "cityName":"Mansehra",
            "countryId":1,
            "createdAt":"2021-05-20T21:37:44.000Z",
            "updatedAt":"2021-05-20T21:37:44.000Z",
            "deletedAt":null
            },
            {
            "id":482,
            "cityName":"Mardan",
            "countryId":1,
            "createdAt":"2021-05-20T21:33:52.000Z",
            "updatedAt":"2021-05-20T21:33:52.000Z",
            "deletedAt":null
            },
            {
            "id":541,
            "cityName":"Mianwali",
            "countryId":1,
            "createdAt":"2021-05-20T21:38:26.000Z",
            "updatedAt":"2021-05-20T21:38:26.000Z",
            "deletedAt":null
            },
            {
            "id":485,
            "cityName":"Mingora",
            "countryId":1,
            "createdAt":"2021-05-20T21:34:04.000Z",
            "updatedAt":"2021-05-20T21:34:04.000Z",
            "deletedAt":null
            },
            {
            "id":534,
            "cityName":"Mirpur",
            "countryId":1,
            "createdAt":"2021-05-20T21:37:54.000Z",
            "updatedAt":"2021-05-20T21:37:54.000Z",
            "deletedAt":null
            },
            {
            "id":492,
            "cityName":"Mirpur Khas",
            "countryId":1,
            "createdAt":"2021-05-20T21:34:40.000Z",
            "updatedAt":"2021-05-20T21:34:40.000Z",
            "deletedAt":null
            },
            {
            "id":557,
            "cityName":"Mirpur Mathelo",
            "countryId":1,
            "createdAt":"2021-05-20T21:39:53.000Z",
            "updatedAt":"2021-05-20T21:39:53.000Z",
            "deletedAt":null
            },
            {
            "id":465,
            "cityName":"Multan",
            "countryId":1,
            "createdAt":"2021-05-20T21:31:55.000Z",
            "updatedAt":"2021-05-20T21:31:55.000Z",
            "deletedAt":null
            },
            {
            "id":513,
            "cityName":"Muridke",
            "countryId":1,
            "createdAt":"2021-05-20T21:36:19.000Z",
            "updatedAt":"2021-05-20T21:36:19.000Z",
            "deletedAt":null
            },
            {
            "id":520,
            "cityName":"Muzaffarabad",
            "countryId":1,
            "createdAt":"2021-05-20T21:36:47.000Z",
            "updatedAt":"2021-05-20T21:36:47.000Z",
            "deletedAt":null
            },
            {
            "id":499,
            "cityName":"Muzaffargarh",
            "countryId":1,
            "createdAt":"2021-05-20T21:35:19.000Z",
            "updatedAt":"2021-05-20T21:35:19.000Z",
            "deletedAt":null
            },
            {
            "id":554,
            "cityName":"Narowal",
            "countryId":1,
            "createdAt":"2021-05-20T21:39:39.000Z",
            "updatedAt":"2021-05-20T21:39:39.000Z",
            "deletedAt":null
            },
            {
            "id":486,
            "cityName":"Nawabshah",
            "countryId":1,
            "createdAt":"2021-05-20T21:34:07.000Z",
            "updatedAt":"2021-05-20T21:34:07.000Z",
            "deletedAt":null
            },
            {
            "id":538,
            "cityName":"Nowshera",
            "countryId":1,
            "createdAt":"2021-05-20T21:38:13.000Z",
            "updatedAt":"2021-05-20T21:38:13.000Z",
            "deletedAt":null
            },
            {
            "id":484,
            "cityName":"Okara",
            "countryId":1,
            "createdAt":"2021-05-20T21:34:00.000Z",
            "updatedAt":"2021-05-20T21:34:00.000Z",
            "deletedAt":null
            },
            {
            "id":508,
            "cityName":"Pakpattan",
            "countryId":1,
            "createdAt":"2021-05-20T21:35:59.000Z",
            "updatedAt":"2021-05-20T21:35:59.000Z",
            "deletedAt":null
            },
            
            
            {
            "id":476,
            "cityName":"Rahim Yar Khan",
            "countryId":1,
            "createdAt":"2021-05-20T21:33:18.000Z",
            "updatedAt":"2021-05-20T21:33:18.000Z",
            "deletedAt":null
            },
            
            {
            "id":491,
            "cityName":"Sadiqabad",
            "countryId":1,
            "createdAt":"2021-05-20T21:34:34.000Z",
            "updatedAt":"2021-05-20T21:34:34.000Z",
            "deletedAt":null
            },
            {
            "id":480,
            "cityName":"Sahiwal",
            "countryId":1,
            "createdAt":"2021-05-20T21:33:41.000Z",
            "updatedAt":"2021-05-20T21:33:41.000Z",
            "deletedAt":null
            },
            {
            "id":550,
            "cityName":"Sambrial",
            "countryId":1,
            "createdAt":"2021-05-20T21:39:22.000Z",
            "updatedAt":"2021-05-20T21:39:22.000Z",
            "deletedAt":null
            },
            {
            "id":515,
            "cityName":"Samundri",
            "countryId":1,
            "createdAt":"2021-05-20T21:36:26.000Z",
            "updatedAt":"2021-05-20T21:36:26.000Z",
            "deletedAt":null
            },
            {
            "id":470,
            "cityName":"Sargodha",
            "countryId":1,
            "createdAt":"2021-05-20T21:32:22.000Z",
            "updatedAt":"2021-05-20T21:32:22.000Z",
            "deletedAt":null
            },
            {
            "id":540,
            "cityName":"Shahdadkot",
            "countryId":1,
            "createdAt":"2021-05-20T21:38:21.000Z",
            "updatedAt":"2021-05-20T21:38:21.000Z",
            "deletedAt":null
            },
            {
            "id":475,
            "cityName":"Sheikhupura",
            "countryId":1,
            "createdAt":"2021-05-20T21:33:12.000Z",
            "updatedAt":"2021-05-20T21:33:12.000Z",
            "deletedAt":null
            },
            {
            "id":502,
            "cityName":"Shikarpur",
            "countryId":1,
            "createdAt":"2021-05-20T21:35:35.000Z",
            "updatedAt":"2021-05-20T21:35:35.000Z",
            "deletedAt":null
            },
            {
            "id":471,
            "cityName":"Sialkot",
            "countryId":1,
            "createdAt":"2021-05-20T21:32:26.000Z",
            "updatedAt":"2021-05-20T21:32:26.000Z",
            "deletedAt":null
            },
            {
            "id":472,
            "cityName":"Sukkur",
            "countryId":1,
            "createdAt":"2021-05-20T21:32:30.000Z",
            "updatedAt":"2021-05-20T21:32:30.000Z",
            "deletedAt":null
            },
            {
            "id":535,
            "cityName":"Swabi",
            "countryId":1,
            "createdAt":"2021-05-20T21:38:00.000Z",
            "updatedAt":"2021-05-20T21:38:00.000Z",
            "deletedAt":null
            },
            {
            "id":517,
            "cityName":"Tando Adam",
            "countryId":1,
            "createdAt":"2021-05-20T21:36:36.000Z",
            "updatedAt":"2021-05-20T21:36:36.000Z",
            "deletedAt":null
            },
            {
            "id":516,
            "cityName":"Tando Allahyar",
            "countryId":1,
            "createdAt":"2021-05-20T21:36:32.000Z",
            "updatedAt":"2021-05-20T21:36:32.000Z",
            "deletedAt":null
            },
            {
            "id":555,
            "cityName":"Tando Muhammad Khan",
            "countryId":1,
            "createdAt":"2021-05-20T21:39:44.000Z",
            "updatedAt":"2021-05-20T21:39:44.000Z",
            "deletedAt":null
            },
            {
            "id":537,
            "cityName":"Taxila",
            "countryId":1,
            "createdAt":"2021-05-20T21:38:08.000Z",
            "updatedAt":"2021-05-20T21:38:08.000Z",
            "deletedAt":null
            },
            {
            "id":498,
            "cityName":"Turbat",
            "countryId":1,
            "createdAt":"2021-05-20T21:35:14.000Z",
            "updatedAt":"2021-05-20T21:35:14.000Z",
            "deletedAt":null
            },
            {
            "id":528,
            "cityName":"Umerkot",
            "countryId":1,
            "createdAt":"2021-05-20T21:37:23.000Z",
            "updatedAt":"2021-05-20T21:37:23.000Z",
            "deletedAt":null
            },
            {
            "id":522,
            "cityName":"Vehari",
            "countryId":1,
            "createdAt":"2021-05-20T21:36:56.000Z",
            "updatedAt":"2021-05-20T21:36:56.000Z",
            "deletedAt":null
            },
            {
            "id":481,
            "cityName":"Wah Cantonment",
            "countryId":1,
            "createdAt":"2021-05-20T21:33:46.000Z",
            "updatedAt":"2021-05-20T21:33:46.000Z",
            "deletedAt":null
            },
            {
            "id":531,
            "cityName":"Wazirabad",
            "countryId":1,
            "createdAt":"2021-05-20T21:37:35.000Z",
            "updatedAt":"2021-05-20T21:37:35.000Z",
            "deletedAt":null
            }
          ],
          yeardata:[
            {"name":"2021","id":"2021"},
            {"name":"2020","id":"2020"},
            {"name":"2019","id":"2019"},
            {"name":"2018","id":"2018"},
            {"name":"2017","id":"2017"},
            {"name":"2016","id":"2016"},
            {"name":"2015","id":"2015"},
            {"name":"2014","id":"2014"},
            {"name":"2013","id":"2013"},
            {"name":"2012","id":"2012"},
            {"name":"2011","id":"2011"},
            {"name":"2010","id":"2010"},
            {"name":"2009","id":"2009"},
            {"name":"2008","id":"2008"},
            {"name":"2007","id":"2007"},
            {"name":"2006","id":"2006"},
            {"name":"2005","id":"2005"},
            {"name":"2004","id":"2004"},
            {"name":"2003","id":"2003"},
            {"name":"2002","id":"2002"},
            {"name":"2001","id":"2001"},
            {"name":"2000","id":"2000"},
            {"name":"1999","id":"1999"},
            {"name":"1998","id":"1998"},
            {"name":"1997","id":"1997"},
            {"name":"1996","id":"1996"},
            {"name":"1995","id":"1995"},
            {"name":"1994","id":"1994"},
            {"name":"1993","id":"1993"},
            {"name":"1992","id":"1992"},
            {"name":"1991","id":"1991"},
            {"name":"1990","id":"1990"},
            {"name":"1989","id":"1989"},
            {"name":"1988","id":"1988"},
            {"name":"1987","id":"1987"},
            {"name":"1986","id":"1986"},
            {"name":"1985","id":"1985"},
            {"name":"1984","id":"1984"},
            {"name":"1983","id":"1983"},
            {"name":"1982","id":"1982"},
            {"name":"1981","id":"1981"},
            {"name":"1980","id":"1980"},
            {"name":"1979","id":"1979"},
            {"name":"1978","id":"1978"},
            {"name":"1977","id":"1977"},
            {"name":"1976","id":"1976"},
            {"name":"1975","id":"1975"},
            {"name":"1974","id":"1974"},
            {"name":"1973","id":"1973"},
            {"name":"1972","id":"1972"},
            {"name":"1971","id":"1971"},
            {"name":"1970","id":"1970"},
            {"name":"1969","id":"1969"},
            {"name":"1968","id":"1968"},
            {"name":"1967","id":"1967"},
            {"name":"1966","id":"1966"},
            {"name":"1965","id":"1965"},
            {"name":"1964","id":"1964"},
            {"name":"1963","id":"1963"},
            {"name":"1962","id":"1962"},
            {"name":"1961","id":"1961"},
            {"name":"1960","id":"1960"}
          ],
          searchText:'',
          type:'',
          category:'',
          brand:'',
          model:'',
          year:'',
          typeid:'',
          categoryid:'',
          brandid:'',
          modelid:'',
          yearid:'',
          long:'',
          lat:'',
          offset:0
        };
      }
      reload=()=>{
        this.getLocation();
      }
      componentDidMount(){
        
        this.getcategory();
        this.getbrand();
        this.getLocation();
      }
      getLocation=()=>{
        GetLocation.getCurrentPosition({
          enableHighAccuracy: true,
          timeout: 15000,
        }).then(location => {
            console.log(location);
            this.setState({long:location.longitude,lat:location.latitude})
            setTimeout(() => {
              this.getProducts();
            }, 100);
        }).catch(error => {
            const { code, message } = error;
            console.warn(code, message);
            this.setState({long:'',lat:''})
            setTimeout(() => {
              this.getProducts();
            }, 100);
            alert('Turn on your location to see services nearby');
        })
      }
      getProducts=()=>{
        this.setState({offset:0});
        this.setState({refreshing:true})
        var header={
          "Content-Type":"application/json",
          "Authorization":global.usertoken
        }
        var branch= this.state.brandid!=''?'&brandId='+this.state.brandid:'';
        var category= this.state.categoryid!=''?'&categoryId='+this.state.categoryid:'';
        var model= this.state.modelid!=''?'&modelId='+this.state.modelid:'';
        var year= this.state.yearid!=''?'&yearId='+this.state.yearid:'';
        var type= this.state.typeid!=''?'&categoryId='+this.state.typeid:'';
        var city= this.state.cityid!=''?'&cityId='+this.state.cityid:'';
        var name= this.state.searchText!=''?'&name='+this.state.searchText:'';
        var long=this.state.long!=''?'&longitude='+this.state.long:'';
        var lat =this.state.lat!=''?'&lattitude='+this.state.lat:'';
        var url= global.url + '/api/service?offset=0&limit=10'+branch+category+model+year+type+city+name+long+lat;
        console.log('====================================');
        console.log(url);
        console.log('====================================');
        axios.get( url ,{headers:header}).then(response => {
          console.log(response.data.result.data);
          this.setState({
            productdata:response.data.result.data
          })
          this.setState({refreshing:false})
        }).catch(error => {
          this.setState({refreshing:false})
          console.log(error.response);
        });
      }
      loadMoreResults=(offset)=>{
        var header={
          "Content-Type":"application/json",
          "Authorization":global.usertoken
        }
        var branch= this.state.brandid!=''?'&brandId='+this.state.brandid:'';
        var category= this.state.categoryid!=''?'&categoryId='+this.state.categoryid:'';
        var model= this.state.modelid!=''?'&modelId='+this.state.modelid:'';
        var year= this.state.yearid!=''?'&yearId='+this.state.yearid:'';
        var type= this.state.typeid!=''?'&categoryId='+this.state.typeid:'';
        var city= this.state.cityid!=''?'&cityId='+this.state.cityid:'';
        var name= this.state.searchText!=''?'&name='+this.state.searchText:'';
        var long=this.state.long!=''?'&longitude='+this.state.long:'';
        var lat =this.state.lat!=''?'&lattitude='+this.state.lat:'';
        var url= global.url + '/api/service?offset='+offset+'&limit=10'+branch+category+model+year+type+city+name+long+lat;
        console.log('====================================');
        console.log(url);
        console.log('====================================');
        axios.get( url ,{headers:header}).then(response => {
          console.log(response.data.result.data);
          if(response.data.result.data.length>0){
            var temp=this.state.productdata;
            response.data.result.data.map((item)=>{
              temp.push(item);
            })
            setTimeout(() => {
              this.setState({productdata:temp})
            }, 100);
          }
        }).catch(error => {
          console.log(error.response);
        });
      };
      //get data
      getcategory=()=>{
        const url = global.url+'/api/service-category?offset=0&limit=100';
        axios.get(url).then(response => {
            console.log('====================================');
            console.log(response.data.result.data);
            console.log('====================================');
            this.setState({categorydata:response.data.result.data})
        }).catch(err=>{
        console.log(err)
        });
      }
      getbrand=()=>{
        const url = global.url+'/api/brand?offset=0&limit=100';
        axios.get(url).then(response => {
            console.log('====================================');
            console.log(response.data.result.data);
            console.log('====================================');
            this.setState({branddata:response.data.result.data})
        }).catch(err=>{
        console.log(err)
        });
      }
      getmodel=()=>{
        const url = global.url+'/api/model?offset=0&limit=100&id='+this.state.brandid;
        axios.get(url).then(response => {
            console.log('====================================');
            console.log(response.data.result.data);
            console.log('====================================');
            this.setState({modeldata:response.data.result.data})
        }).catch(err=>{
        console.log(err)
        });
      }
      getyear=()=>{
        this.setState({ editname:'',name:''})
        const url = global.url+'/api/year?offset=0&limit=100&id='+this.state.modelid;
        axios.get(url).then(response => {
            console.log('====================================');
            console.log(response.data.result.data);
            console.log('====================================');
            this.setState({yeardata:response.data.result.data})
        }).catch(err=>{
        console.log(err)
        });
      }
      //get data
      render() {
        return (
          <SafeAreaView style={{ flex: 1 }}>
            <View style={{ backgroundColor:'#fff',paddingVertical: 15, borderBottomWidth: 1, borderBottomColor: 'rgb(230,230,230)', paddingHorizontal: 10}} >
              <View style={{ width:'100%',paddingHorizontal:10,backgroundColor: 'rgb(242,242,242)', flexDirection: "row", alignItems: "center", justifyContent: "space-between", borderRadius: 10 }} >
                <TextInput
                    style={{ paddingVertical: 13,width:'90%' }}
                    placeholder={'Petrol Pump, Suspension, …'}
                    placeholderTextColor={'#b3b3b3'}
                    value={this.state.searchText}
                    onChangeText={text => {
                      this.setState({searchText:text});
                      setTimeout(() => {
                        this.getProducts();                    
                      }, 100);
                    }}
                />
                <Image source={Search} style={{ width: 24, height: 24 }} />
              </View>
              <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} alwaysBounceHorizontal={true}>
                <TouchableOpacity style={[styles.homefilterview,{backgroundColor:'#000'}]} onPress={()=>{
                  this.setState({
                    type:'',
                    category:'',
                    brand:'',
                    model:'',
                    year:'',
                    typeid:'',
                    categoryid:'',
                    brandid:'',
                    modelid:'',
                    yearid:'',
                    city:'',
                    cityId:''
                  })
                  setTimeout(() => {
                    this.getProducts();
                  }, 200);
                }}>
                  <Text style={[styles.hometxtfilter,{color:'#fff',}]}>{global.lang=='en'?'Clear Filter':'فلٹر صاف کریں'}</Text>
                </TouchableOpacity>
                {/* <TouchableOpacity style={[styles.homefilterview,{borderColor:this.state.city!=''?'#f9af16':'#333333',backgroundColor:this.state.city!=''?'#f3ce4529':'#fff'}]} onPress={()=>{this.CitySheet.open()}}>
                  <Text style={[styles.hometxtfilter,{color:this.state.city!=''?'#f9af16':"#1a1a1a"}]}>{this.state.city==''?'City':this.state.city}</Text>
                  {(this.state.city!='') && (
                    <TouchableOpacity onPress={()=>{
                      this.setState({city:'',cityid:''})
                      setTimeout(() => {
                        this.getProducts();                    
                      }, 100);
                    }}>
                      <Image source={require('../../assets/images/close.png')} style={[styles.homeimg,{ width: 20, height: 20,marginRight:4 }]} resizeMode={'contain'}/>
                    </TouchableOpacity>
                  )}
                </TouchableOpacity> */}
                <TouchableOpacity style={[styles.homefilterview,{borderColor:this.state.type!=''?'#f9af16':'#333333',backgroundColor:this.state.type!=''?'#f3ce4529':'#fff'}]} onPress={()=>{this.TypeSheet.open()}}>
                  <Text style={[styles.hometxtfilter,{color:this.state.type!=''?'#f9af16':"#1a1a1a"}]}>{this.state.type==''?'Type':this.state.type}</Text>
                  {(this.state.type!='') && (
                    <TouchableOpacity onPress={()=>{
                      this.setState({type:'',typeid:''})
                      setTimeout(() => {
                        this.getProducts();                    
                      }, 100);
                    }}>
                      <Image source={require('../../assets/images/close.png')} style={[styles.homeimg,{ width: 20, height: 20,marginRight:4 }]} resizeMode={'contain'}/>
                    </TouchableOpacity>
                  )}
                </TouchableOpacity>
                <TouchableOpacity style={[styles.homefilterview,{borderColor:this.state.brand!=''?'#f9af16':'#333333',backgroundColor:this.state.brand!=''?'#f3ce4529':'#fff'}]} onPress={()=>{this.BrandSheet.open()}}>
                  <Text style={[styles.hometxtfilter,{color:this.state.brand!=''?'#f9af16':"#1a1a1a"}]}>{this.state.brand==''?'Brand':this.state.brand}</Text>
                  {(this.state.brand!='') && (
                    <TouchableOpacity onPress={()=>{
                      this.setState({brand:'',model:'',year:'',brandid:'',modelid:'',yearid:''})
                      setTimeout(() => {
                        this.getProducts();                    
                      }, 100);
                    }}>
                      <Image source={require('../../assets/images/close.png')} style={[styles.homeimg,{ width: 20, height: 20,marginRight:4 }]} resizeMode={'contain'}/>
                    </TouchableOpacity>
                  )}
                </TouchableOpacity>
                <TouchableOpacity style={[styles.homefilterview,{borderColor:this.state.model!=''?'#f9af16':'#333333',backgroundColor:this.state.model!=''?'#f3ce4529':'#fff'}]} onPress={()=>{this.ModelSheet.open()}}>
                  <Text style={[styles.hometxtfilter,{color:this.state.model!=''?'#f9af16':"#1a1a1a"}]}>{this.state.model==''?'Model':this.state.model}</Text>
                  {(this.state.model!='') && (
                    <TouchableOpacity onPress={()=>{
                      this.setState({model:'',year:'',modelid:'',yearid:''})
                      setTimeout(() => {
                        this.getProducts();                    
                      }, 100);
                    }}>
                      <Image source={require('../../assets/images/close.png')} style={[styles.homeimg,{ width: 20, height: 20,marginRight:4 }]} resizeMode={'contain'}/>
                    </TouchableOpacity>
                  )}
                </TouchableOpacity>
                {/* <TouchableOpacity style={[styles.homefilterview,{borderColor:this.state.year!=''?'#f9af16':'#333333',backgroundColor:this.state.year!=''?'#f3ce4529':'#fff'}]} onPress={()=>{this.YearSheet.open()}}>
                  <Text style={[styles.hometxtfilter,{color:this.state.year!=''?'#f9af16':"#1a1a1a"}]}>{this.state.year==''?'Year':this.state.year}</Text>
                  {(this.state.year!='') && (
                    <TouchableOpacity onPress={()=>{
                      this.setState({year:'',yearid:''})
                      setTimeout(() => {
                        this.getProducts();                    
                      }, 100);
                    }}>
                      <Image source={require('../../assets/images/close.png')} style={[styles.homeimg,{ width: 20, height: 20,marginRight:4 }]} resizeMode={'contain'}/>
                    </TouchableOpacity>
                  )}
                </TouchableOpacity> */}
              </ScrollView>
            </View>
            
              <FlatList
                refreshControl={
                  <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={()=>{this.reload()}}
                    colors={["#9Bd35A", "#689F38"]}
                  />
                }
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                numColumns={2}
                onEndReachedThreshold={0.01}
                onEndReached={() => {
                  this.loadMoreResults(this.state.offset+1);
                  this.setState({offset:this.state.offset+1});
                }}
                // inverted={true}
                contentContainerStyle={{padding:10}}
                keyExtractor={(item,index) => index + ''}
                data={this.state.productdata}
                renderItem={({ item, index }) => {
                  return (
                    <TouchableOpacity key={index} style={[styles.homeview,{marginRight:10}]} onPress={()=>{this.props.navigation.navigate('ServiceDetail',{data:item})}}>
                      <FastImage
                          style={[styles.homeimg,{ width: '100%', height: 202 }]}
                          source={{ uri:item.resources[0].awsUrl }}
                          resizeMode={FastImage.resizeMode.cover}
                      />
                      <View style={{marginLeft:10,marginRight:10}}>
                        <Text style={[styles.hometxt,styles.coloryellow,styles.fs12,styles.ffm,{marginBottom:0}]}>{item.Shop.name}</Text>
                        <View style={{flexDirection:'row',flexWrap:'wrap'}}>
                          <Image source={{uri:item.Brand.awsUrl}} style={[styles.homeimg,{ width: 20, height: 20,marginRight:5,alignSelf:'center',justifyContent:'center' }]} resizeMode={'contain'}/>
                          <Text style={[styles.hometxt2,styles.colorlightblack,{flex: 1, flexWrap: 'wrap'}]}>{item.Brand.name} - {item.Model.name}</Text>
                        </View>
                        <Text style={[styles.hometxt2,styles.fs16,styles.ffb,styles.pb10]}>{item.ServiceCategory?.name}</Text>
                      </View>
                    </TouchableOpacity>
                  );
                }}
              />
            
            <EmergencyBtn />
            <RBSheet ref={ref => { this.CitySheet = ref; }} height={350} openDuration={250}  >
              <View>
                <View style={{ borderBottomColor:'#d9d9d9',borderBottomWidth:1,position: 'relative', }}>
                  <Text style={[styles.ffb,styles.fs16,styles.colorblack,styles.p20,{textAlign:'center'}]}>{global.lang=='en'?'Select City':'قسم منتخب کریں'}</Text>
                  <View style={{position: 'absolute',right:0}}>
                  </View>
                </View>
                <ScrollView contentContainerStyle={{padding:20,paddingBottom:100}}>
                  {this.state.citiesdata.map((item, index) => {
                    return (
                      <TouchableOpacity key={index} 
                      style={{flexDirection:'row',justifyContent:'space-between',alignContent:'center',alignItems:'center'}}
                      onPress={() => {
                        this.setState({city:item.cityName,cityid:item.id});
                        this.CitySheet.close();
                        setTimeout(() => {
                          this.getProducts();                    
                        }, 100);
                      }}>
                        <Text style={[styles.ffm,styles.fs14,styles.colorblack,styles.p10]}>{item.cityName}</Text>
                        {(this.state.city==item.cityName) && (
                          <Image source={require('../../assets/images/tick.png')} style={[styles.homeimg,{ width: 20, height: 20,marginRight:4 }]} resizeMode={'contain'}/>
                        )}
                      </TouchableOpacity>
                    )
                  })}
                </ScrollView>
              </View>
            </RBSheet>
            <RBSheet ref={ref => { this.TypeSheet = ref; }} height={400} openDuration={250}  >
              <View>
                <View style={{ borderBottomColor:'#d9d9d9',borderBottomWidth:1,position: 'relative', }}>
                  <Text style={[styles.ffb,styles.fs16,styles.colorblack,styles.p20,{textAlign:'center'}]}>{global.lang=='en'?'Select Type':'قسم منتخب کریں'}</Text>
                  <View style={{position: 'absolute',right:0}}>
                  </View>
                </View>
                <ScrollView contentContainerStyle={{padding:20,paddingBottom:100}}>
                  {this.state.categorydata.map((item, index) => {
                    return (
                      <TouchableOpacity key={index} 
                      style={{flexDirection:'row',justifyContent:'space-between',alignContent:'center',alignItems:'center'}}
                      onPress={() => {
                        this.setState({type:item.name,typeid:item.id});
                        this.TypeSheet.close();
                        setTimeout(() => {
                          this.getProducts();                    
                        }, 100);
                      }}>
                        <Text style={[styles.ffm,styles.fs14,styles.colorblack,styles.p10]}>{item.name}</Text>
                        {(this.state.type==item.name) && (
                          <Image source={require('../../assets/images/tick.png')} style={[styles.homeimg,{ width: 20, height: 20,marginRight:4 }]} resizeMode={'contain'}/>
                        )}
                      </TouchableOpacity>
                    )
                  })}
                </ScrollView>
              </View>
            </RBSheet>
            <RBSheet ref={ref => { this.CategorySheet = ref; }} height={400} openDuration={250}  >
              <View>
                <View style={{ borderBottomColor:'#d9d9d9',borderBottomWidth:1,position: 'relative', }}>
                  <Text style={[styles.ffb,styles.fs16,styles.colorblack,styles.p20,{textAlign:'center'}]}>{global.lang=='en'?'Select Category':'زمرہ منتخب کریں'}</Text>
                  <View style={{position: 'absolute',right:0}}>
                  </View>
                </View>
                <ScrollView contentContainerStyle={{padding:20,paddingBottom:100}}>
                  {this.state.categorydata.map((item, index) => {
                    return (
                      <TouchableOpacity key={index} 
                      style={{flexDirection:'row',justifyContent:'space-between',alignContent:'center',alignItems:'center'}}
                      onPress={() => {
                        this.setState({category:item.name,categoryid:item.id});
                        this.CategorySheet.close();
                        setTimeout(() => {
                          this.getProducts();                    
                        }, 100);
                      }}>
                        <Text style={[styles.ffm,styles.fs14,styles.colorblack,styles.p10]}>{item.name}</Text>
                        {(this.state.category==item.name) && (
                          <Image source={require('../../assets/images/tick.png')} style={[styles.homeimg,{ width: 20, height: 20,marginRight:4 }]} resizeMode={'contain'}/>
                        )}
                      </TouchableOpacity>
                    )
                  })}
                </ScrollView>
              </View>
            </RBSheet>
            <RBSheet ref={ref => { this.BrandSheet = ref; }} height={400} openDuration={250}  >
              <View>
                <View style={{ borderBottomColor:'#d9d9d9',borderBottomWidth:1,position: 'relative', }}>
                  <Text style={[styles.ffb,styles.fs16,styles.colorblack,styles.p20,{textAlign:'center'}]}>{global.lang=='en'?'Select Brand':'برانڈ منتخب کریں'}</Text>
                  <View style={{position: 'absolute',right:0}}>
                  </View>
                </View>
                <ScrollView contentContainerStyle={{padding:20,paddingBottom:100}}>
                  {this.state.branddata.map((item, index) => {
                    return (
                      <TouchableOpacity key={index} 
                      style={{flexDirection:'row',justifyContent:'space-between',alignContent:'center',alignItems:'center'}}
                      onPress={() => {
                        this.setState({brand:item.name,brandid:item.id,model:'',modelid:'',year:'',yearid:''});
                        this.BrandSheet.close();
                        setTimeout(() => {
                          this.getmodel();
                          this.getProducts();                    
                        }, 100);
                      }}>
                        <Text style={[styles.ffm,styles.fs14,styles.colorblack,styles.p10]}>{item.name}</Text>
                        {(this.state.brand==item.name) && (
                          <Image source={require('../../assets/images/tick.png')} style={[styles.homeimg,{ width: 20, height: 20,marginRight:4 }]} resizeMode={'contain'}/>
                        )}
                      </TouchableOpacity>
                    )
                  })}
                </ScrollView>
              </View>
            </RBSheet>
            <RBSheet ref={ref => { this.ModelSheet = ref; }} height={400} openDuration={250}  >
              <View>
                <View style={{ borderBottomColor:'#d9d9d9',borderBottomWidth:1,position: 'relative', }}>
                  <Text style={[styles.ffb,styles.fs16,styles.colorblack,styles.p20,{textAlign:'center'}]}>{global.lang=='en'?'Select Model':'ماڈل منتخب کریں'}</Text>
                  <View style={{position: 'absolute',right:0}}>
                  </View>
                </View>
                <ScrollView contentContainerStyle={{padding:20,paddingBottom:100}}>
                  {this.state.modeldata.map((item, index) => {
                    return (
                      <TouchableOpacity key={index} 
                      style={{flexDirection:'row',justifyContent:'space-between',alignContent:'center',alignItems:'center'}}
                      onPress={() => {
                        this.setState({model:item.name,modelid:item.id,year:'',yearid:''});
                        this.ModelSheet.close();
                        setTimeout(() => {
                          this.getyear();
                          this.getProducts();                    
                        }, 100);
                      }}>
                        <Text style={[styles.ffm,styles.fs14,styles.colorblack,styles.p10]}>{item.name}</Text>
                        {(this.state.model==item.name) && (
                          <Image source={require('../../assets/images/tick.png')} style={[styles.homeimg,{ width: 20, height: 20,marginRight:4 }]} resizeMode={'contain'}/>
                        )}
                      </TouchableOpacity>
                    )
                  })}
                </ScrollView>
              </View>
            </RBSheet>
            <RBSheet ref={ref => { this.YearSheet = ref; }} height={400} openDuration={250}  >
              <View>
                <View style={{ borderBottomColor:'#d9d9d9',borderBottomWidth:1,position: 'relative', }}>
                  <Text style={[styles.ffb,styles.fs16,styles.colorblack,styles.p20,{textAlign:'center'}]}>{global.lang=='en'?'Select Year':'سال کو منتخب کریں'}</Text>
                  <View style={{position: 'absolute',right:0}}>
                  </View>
                </View>
                <ScrollView contentContainerStyle={{padding:20,paddingBottom:100}}>
                  {this.state.yeardata.map((item, index) => {
                    return (
                      <TouchableOpacity key={index} 
                      style={{flexDirection:'row',justifyContent:'space-between',alignContent:'center',alignItems:'center'}}
                      onPress={() => {
                        this.setState({year:item.name,yearid:item.id});
                        this.YearSheet.close();
                        setTimeout(() => {
                          this.getProducts();                    
                        }, 100);
                      }}>
                        <Text style={[styles.ffm,styles.fs14,styles.colorblack,styles.p10]}>{item.name}</Text>
                        {(this.state.year==item.name) && (
                          <Image source={require('../../assets/images/tick.png')} style={[styles.homeimg,{ width: 20, height: 20,marginRight:4 }]} resizeMode={'contain'}/>
                        )}
                      </TouchableOpacity>
                    )
                  })}
                </ScrollView>
              </View>
            </RBSheet>
    
          </SafeAreaView>
        );
      }
}
