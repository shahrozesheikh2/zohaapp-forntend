import React from 'react'
import { SafeAreaView, TouchableOpacity, Text, Image, View, ScrollView } from 'react-native';
import styles from '../../assets/css/styles';
import axios from 'axios';
import { Overlay } from 'react-native-elements';

export default class ShopServiceItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          fullname:'',
          shopname:'',
          shopphone:'',
          shopaddress:'',
          contactnumber:'',
          typeofselling:'',
          about:'',
          rating:'',
          productdata:[],
          shopid:'',
          typeid:'',
          showOverlay:false,
        };
        this.getProducts();
      }
      toggleOverlay = () => {
        this.setState({showOverlay:!this.state.showOverlay})
      }
      getProducts=()=>{
        var header={
          "Content-Type":"application/json",
          "Authorization":global.usertoken
        }
        axios.get( global.url + '/api/service?offset=0&limit=900&self=true',{headers:header}).then(response => {
          console.log(response.data.result.data);
          this.setState({
            productdata:response.data.result.data
          })
        }).catch(error => {
          console.log(error.response);
        });
      }
      
      render() {
        return (
          <SafeAreaView style={{ flex: 1 }}>
            <View>
                <Text style={[styles.menutxt,styles.fs20,styles.p20]}>{global.lang=='en'?"Services":'خدمات'}</Text>
            </View>
            <ScrollView style={{backgroundColor: '#fff',}}>
              <View style={{flexDirection:'row',justifyContent:'space-between',flexWrap:'wrap',padding: 20,}}>
                <TouchableOpacity style={styles.homeview2} onPress={()=>{this.props.navigation.navigate('AddServiceItem',{typeid:this.state.typeid})}}>
                    <Image source={require('../../assets/images/plus.png')} style={[styles.homeimg,{ width: 40, height: 40 }]} resizeMode={'cover'}/>
                    <View style={{marginLeft:10,marginRight:10}}>
                      <Text style={styles.hometxt3}>{global.lang=='en'?"Add New Item":'نیا آئٹم شامل کریں'}</Text>
                    </View>
                </TouchableOpacity>
                {this.state.productdata.map((item,index)=>{
                  return(
                    <TouchableOpacity style={styles.homeview} key={index} onPress={()=>{this.props.navigation.navigate('EditServiceItem',{product:item,typeid:this.state.typeid})}}>
                      <Image source={{uri:item.resources[0].awsUrl}} style={[styles.homeimg,{ width: '100%', height: 150 }]} resizeMode={'cover'}/>
                      <View style={{marginLeft:10,marginRight:10}}>
                        <Text style={[styles.hometxt,styles.ffm,styles.coloryellow,{fontSize:10,marginTop:5}]}>{item.ServiceCategory.name}</Text>
                        <Text style={styles.hometxt2}>{item.Brand.name} - {item.Model.name}</Text>
                        <Text style={styles.hometxt3} numberOfLines={1}>{item.description}</Text>
                      </View>
                    </TouchableOpacity>
                  )
                })}
                
              </View>
            </ScrollView>
            <Overlay overlayStyle={{padding:0,borderRadius:8,}} isVisible={this.state.showOverlay} onBackdropPress={this.toggleOverlay}>
              <View style={{width:300,overflow: 'hidden',}}>
                <Image source={{uri:this.state.shopimage}} style={[{ width: 300, height: 300,borderRadius:8,}]} resizeMode={'cover'}/>
              </View>
            </Overlay>
            
          </SafeAreaView>
        
        );
      }
}
