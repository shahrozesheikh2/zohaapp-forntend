import React from 'react'
import { SafeAreaView, TextInput, Alert,TouchableOpacity, Text, Image, View, ScrollView,Dimensions, Platform } from 'react-native'
import styles from '../../assets/css/styles';
import Share from '../../assets/images/share.png';
import BackButtonComponent from '../../components/BackButtonComponent';
import axios from 'axios';
import DropDownPicker from 'react-native-dropdown-picker';
import ImagePicker from 'react-native-customized-image-picker';
import ImageResizer from 'react-native-image-resizer';
const options = {
  maxWidth: 500,
  maxHeight: 500,
  quality: 1
};
export default class EditItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          productname:this.props.route.params.product.name,
          productprice:this.props.route.params.product.price,
          category:'',
          categoryid:'',
          desc:this.props.route.params.product.description,
          brandname:'',
          brandid:'',
          modelname:'',
          yearname:'',
          modelid:'',
          yearid:'',
          //data
          categorydata:[],
          branddata:[],
          modeldata:[],
          yeardata:[
            {"name":"2021","id":"2021"},
            {"name":"2020","id":"2020"},
            {"name":"2019","id":"2019"},
            {"name":"2018","id":"2018"},
            {"name":"2017","id":"2017"},
            {"name":"2016","id":"2016"},
            {"name":"2015","id":"2015"},
            {"name":"2014","id":"2014"},
            {"name":"2013","id":"2013"},
            {"name":"2012","id":"2012"},
            {"name":"2011","id":"2011"},
            {"name":"2010","id":"2010"},
            {"name":"2009","id":"2009"},
            {"name":"2008","id":"2008"},
            {"name":"2007","id":"2007"},
            {"name":"2006","id":"2006"},
            {"name":"2005","id":"2005"},
            {"name":"2004","id":"2004"},
            {"name":"2003","id":"2003"},
            {"name":"2002","id":"2002"},
            {"name":"2001","id":"2001"},
            {"name":"2000","id":"2000"},
            {"name":"1999","id":"1999"},
            {"name":"1998","id":"1998"},
            {"name":"1997","id":"1997"},
            {"name":"1996","id":"1996"},
            {"name":"1995","id":"1995"},
            {"name":"1994","id":"1994"},
            {"name":"1993","id":"1993"},
            {"name":"1992","id":"1992"},
            {"name":"1991","id":"1991"},
            {"name":"1990","id":"1990"},
            {"name":"1989","id":"1989"},
            {"name":"1988","id":"1988"},
            {"name":"1987","id":"1987"},
            {"name":"1986","id":"1986"},
            {"name":"1985","id":"1985"},
            {"name":"1984","id":"1984"},
            {"name":"1983","id":"1983"},
            {"name":"1982","id":"1982"},
            {"name":"1981","id":"1981"},
            {"name":"1980","id":"1980"},
            {"name":"1979","id":"1979"},
            {"name":"1978","id":"1978"},
            {"name":"1977","id":"1977"},
            {"name":"1976","id":"1976"},
            {"name":"1975","id":"1975"},
            {"name":"1974","id":"1974"},
            {"name":"1973","id":"1973"},
            {"name":"1972","id":"1972"},
            {"name":"1971","id":"1971"},
            {"name":"1970","id":"1970"},
            {"name":"1969","id":"1969"},
            {"name":"1968","id":"1968"},
            {"name":"1967","id":"1967"},
            {"name":"1966","id":"1966"},
            {"name":"1965","id":"1965"},
            {"name":"1964","id":"1964"},
            {"name":"1963","id":"1963"},
            {"name":"1962","id":"1962"},
            {"name":"1961","id":"1961"},
            {"name":"1960","id":"1960"}
          ],
          imageassets:this.props.route.params.product.resources,
          imageassetstosend:[]
          
        };
        this.getcategory();
        this.getbrand();
        console.log('====================================');
        console.log(this.props.route.params.product);
        console.log('====================================');
        var i;
        for(i=0;i<this.props.route.params.product.resources.length;i++){
          this.state.imageassets[i]={
            "path":this.props.route.params.product.resources[i].awsUrl,
            "awsId": this.props.route.params.product.resources[i].awsKey
          }
          this.state.imageassetstosend[i]={
            "awsUrl":this.props.route.params.product.resources[i].path,
            "awsKey": this.props.route.params.product.resources[i].awsId
          }
          this.setState({extra:true});
        }
    }
    opengallery = () => {
      if(this.state.imageassetstosend.length<5){
        ImagePicker.openPicker({imageLoader: 'UNIVERSAL',isCamera:true,multiple: true,maxSize:5-this.state.imageassetstosend.length}).then(response => {
          console.log(response);
          response.map((item)=>{
            ImageResizer.createResizedImage(item.path, 500, 500,'JPEG', 100).then(res => {
              this.updatepic(res.uri);
            })
            .catch(err => {
              console.log(err);
              alert('this image is not allowed')
            });
          }) 
        });
      }
      else{
        alert('You can only upload 5 images')
      }
    };
    updatepic = (image) => {
      const config = {
        headers: {
          "Content-Type": "multipart/form-data"
        },
      };
      var formdata = new FormData();
      formdata.append("file", {
        uri: image,
        type: "image/jpg",
        name: new Date().getTime()+"image.jpg"
      });
      const url = global.url + "/api/upload-resource";
      axios.post(url, formdata, config ).then((response) => {
        console.log(response.data);
        this.state.imageassets.push(response.data.result.data[0]);
        this.setState({extra:true})
        var i;
        for(i=0;i<this.state.imageassets.length;i++){
          this.state.imageassetstosend[i]={
            "awsKey":this.state.imageassets[i].awsId,
            "awsUrl": this.state.imageassets[i].path
          }
        }
        
      }).catch((err) => {
        console.log(err.response);
      });
    };
    cancelimage=(index)=>{
      this.state.imageassets.splice(index, 1);
      this.state.imageassetstosend.splice(index, 1);
      this.setState({extra:true})
    }
    //get data
    getcategory=()=>{
      const url = global.url+'/api/category?offset=0&limit=100';
      axios.get(url).then(response => {
          this.setState({categorydata:response.data.result.data});
          this.setState({
              categoryid: this.props.route.params.product.Category.id
          })
      }).catch(err=>{
      console.log(err)
      });
    }
    getbrand=()=>{
      const url = global.url+'/api/brand?offset=0&limit=100';
      axios.get(url).then(response => {
          this.setState({branddata:response.data.result.data})
          this.setState({ brandid : this.props.route.params.product.Brand.id})
          setTimeout(() => {
              this.getmodel();
          }, 100);
      }).catch(err=>{
      console.log(err)
      });
    }
    getmodel=()=>{
      const url = global.url+'/api/model?offset=0&limit=100&id='+this.state.brandid;
      axios.get(url).then(response => {
          this.setState({modeldata:response.data.result.data});
          this.setState({ modelid : this.props.route.params.product.Model.id ,yearid:this.props.route.params?.product?.year.toString()});
      }).catch(err=>{
      console.log(err)
      });
    }
    //get data
    addProduct=()=>{
      console.log('==================imageassetstosend==================');
      console.log(this.state.imageassetstosend);
      console.log('==================imageassetstosend==================');
      var header={
        "Content-Type":"application/json",
        "Authorization":global.usertoken
      }
      var body={
        "id":this.props.route.params.product.id,
        "name":this.state.productname,
        "price":this.state.productprice,
        "categoryId":this.state.categoryid,
        "typeId":this.props.route.params.typeid,
        "description":this.state.desc,
        "resources": this.state.imageassetstosend,
        "brandId":this.state.brandid,
        "modelId":this.state.modelid,
        "year":this.state.yearid==''?1:this.state.yearid
      }
      console.log(body);
      if(this.state.productname==''){
        alert('Enter Product Name')
      }
      else if(this.state.productprice==''){
        alert('Enter Product Price')
      }
      else if(this.state.categoryid==''){
        alert('Enter Category')
      }
      else if(this.state.desc==''){
        alert('Enter Description')
      }
      else if(this.state.brandid==''){
        alert('Enter Brand')
      }
      else if(this.state.modelid==''){
        alert('Enter Model')
      }
      else if(this.state.yearid==''){
        alert('Enter Year')
      }
      else if(this.state.imageassetstosend.length==0){
        alert('Please add product image')
      }
      else if(this.state.imageassetstosend.length>5){
        alert('Please upload only 5 images')
      }
      else{
        axios.post( global.url + '/api/product/edit',body,{headers:header} ).then(response => {
          console.log(response.data);
          alert('Product Edit Successfully');
          this.props.navigation.replace('Seller')
        }).catch(error => {
          alert(error.response.data.message)
          console.log(error.response);
        });
      }
    }
    deleteProduct=()=>{
      var header={
          "Content-Type":"application/json",
          "Authorization":global.usertoken
      }
      console.log('====================================');
      console.log(global.usertoken);
      console.log('====================================');
      axios.delete( global.url + '/api/product/'+this.props.route.params.product.id,{headers:header} ).then(response => {
          console.log(response.data);
          alert('Product Deleted Successfully');
          this.props.navigation.replace('Seller')
      }).catch(error => {
      alert(error.response.data.message)
      console.log(error.response);
      });
    }
    render() {
      return (
        <SafeAreaView style={{ flex: 1 }}>
          <View style={{flexDirection:"row",justifyContent:'space-between',borderBottomColor:'#d9d9d9',borderBottomWidth:1,paddingBottom:15}}>
            <BackButtonComponent navigation={this.props.navigation}/>
            
            <View  style={{flexDirection:"row" ,justifyContent:'center'}}>
              <Text style={[styles.menutxt,styles.mt20, styles.fs16]}>{global.lang=='en'?"Edit Product":'ترمیم کریں'}</Text>
            </View>

            <View style={{flexDirection:'row',paddingRight:10,paddingLeft:10}}>
              <TouchableOpacity onPress={()=>{this.deleteProduct()}} style={{marginRight:15}}>
                <Text style={[styles.menutxt,styles.mt25,styles.fs14,styles.colorred,styles.ffb ]}>{global.lang=='en'?"Delete":'حذف کریں'}</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=>{this.addProduct()}}>
                <Text style={[styles.menutxt,styles.mt25,styles.fs14,styles.coloryellow,styles.ffb ]}>{global.lang=='en'?"Save":'محفوظ کریں'}</Text>
              </TouchableOpacity>
            </View>
          </View>
          
          
          <ScrollView contentContainerStyle={{padding: 20,paddingBottom:170}}>
            <View>
              <Text style={[styles.menutxt,styles.fs10,styles.ffr,{padding:8}]}>{global.lang=='en'?"Product Name":'پروڈکٹ کا نام'}</Text>
              <TextInput style={{ width:'100%',height:56, backgroundColor:'#f2f2f2',
                borderRadius: 10,paddingHorizontal: 10, fontSize: 13,color:'black'
                }}
                value={this.state.productname}
                onChangeText={(val)=>this.setState({productname:val})}
                
                placeholder={'Ex: Mehran Wheel'}
                placeholderTextColor={'#7c7c7c'}
              />
            </View>
            <View>
              <Text style={[styles.menutxt,styles.fs10,styles.ffr,{padding:8}]}>{global.lang=='en'?"Product Price (Rs)":'مصنوع کی قیمت (روپے)'}</Text>
              <TextInput style={{ width:'100%',height:56, backgroundColor:'#f2f2f2',
                borderRadius: 10,paddingHorizontal: 10, fontSize: 13,color:'black'
                }}
                onChangeText={(val)=>this.setState({productprice:val})}
                value={this.state.productprice.toString()}
                placeholder={'Ex: 2000'}
                placeholderTextColor={'#7c7c7c'}
                keyboardType={'number-pad'}
              />
            </View>
            <View>
              <Text style={[styles.menutxt,styles.fs10,styles.ffr,{padding:8}]}>{global.lang=='en'?"Product Description":'Product Description'}</Text>
              <TextInput style={{ width:'100%',height:100, backgroundColor:'#f2f2f2',paddingTop:10,
                borderRadius: 10,paddingHorizontal: 10, fontSize: 13,color:'black',textAlignVertical: 'top'
                }}
                value={this.state.desc}
                onChangeText={(val)=>this.setState({desc:val})}
                multiline={true}
                numberOfLines={5}
                placeholder={'Write here….'}
                placeholderTextColor={'#7c7c7c'}
              />
            </View>
            <View>
              <Text style={[styles.menutxt,styles.fs10,styles.ffr,{padding:8}]}>{global.lang=='en'?"Product Previews":'پروڈکٹ کا مشاہدہ'}</Text>
              <View style={{flexDirection:'row',flexWrap:'wrap'}}>
                <TouchableOpacity  onPress={()=>{this.opengallery()}} style={[styles.homeview2,{height:100,width:100,marginRight:10}]}>
                    <Image source={require('../../assets/images/plus.png')} style={[styles.homeimg,{ width: 20, height: 20 }]} resizeMode={'cover'}/>
                </TouchableOpacity>
                {this.state.imageassets.map((item,index)=>{
                  return(
                    <View key={index} style={[styles.homeview2,{height:100,width:100,marginRight:10,
                      position: 'relative',}]}>
                      <Image source={{uri:item.path}} style={[styles.homeimg,{ width: 100, height: 100 }]} resizeMode={'cover'}/>
                      <TouchableOpacity  onPress={()=>{this.cancelimage(index)}} style={{position: 'absolute',bottom:0,right: 0,}}>
                        <Image source={require('../../assets/images/close.png')} style={[styles.homeimg,{ width: 30, height: 30 ,
                          }]} resizeMode={'contain'}/>
                      </TouchableOpacity>
                    </View>
                  )
                })}
              </View>
              
              
            </View>
            <Text style={[styles.menutxt,styles.fs10,styles.ffr,{padding:8}]}>{global.lang=='en'?"Product Category":'مصنوعات کی قسم'}</Text>
            <DropDownPicker
              items={[
                ...(() => {
                  return this.state.categorydata.length > 0
                    ? [{
                      label: "Select Product Category",
                      value: ""
                    },...this.state.categorydata.map((item) => {
                        return {
                          label: item.name,
                          value: item.id,
                        };
                      })]
                    : [
                        {
                          label: "No Product Category",
                          value: "",
                        },
                      ];
                })(),
              ]}
              placeholder="Select Product Category"
              placeholderStyle={{
                color:"#333"
              }}
              style={{backgroundColor: '#fff'}}
              selectedLabelStyle={[{color:'#363636'},styles.fs12,styles.ffm]}
              itemStyle={{
                  justifyContent: 'center'
              }}
              defaultValue={this.state.categoryid}
              arrowSize={22}
              arrowColor="#333"
              style={[{width:Dimensions.get('screen').width-40}]}
              dropDownStyle={[{widhth:Dimensions.get('screen').width-40,}]}
              onChangeItem={item => {
                  console.log(item);
                  this.setState({categoryid:item.value})
              }}
              // value={this.state.productname}
              zIndex={9999999}
            />
            <Text style={[styles.menutxt,styles.fs10,styles.ffr,{padding:8}]}>{global.lang=='en'?"Brand":'برانڈ'}</Text>
            <DropDownPicker
              items={[
                ...(() => {
                  return this.state.branddata.length > 0
                    ? [{
                      label: "Select Brand",
                      value: ""
                    },...this.state.branddata.map((item) => {
                        return {
                          label: item.name,
                          value: item.id,
                        };
                      })]
                    : [
                        {
                          label: "No Brand",
                          value: "",
                        },
                      ];
                })(),
              ]}
              placeholder="Select Brand"
              placeholderStyle={{
                color:"#333"
              }}
              style={{backgroundColor: '#fff'}}
              selectedLabelStyle={[{color:'#363636'},styles.fs12,styles.ffm]}
              itemStyle={{
                  justifyContent: 'center'
              }}
              defaultValue={this.state.brandid}
              arrowSize={22}
              arrowColor="#333"
              style={[{width:Dimensions.get('screen').width-40}]}
              dropDownStyle={[{widhth:Dimensions.get('screen').width-40,}]}
              onChangeItem={item => {
                  console.log(item);
                  this.setState({brandid:item.value});
                  setTimeout(() => {
                    this.getmodel()
                  }, 100);
                  
              }}
              zIndex={899999}
            />
            <Text style={[styles.menutxt,styles.fs10,styles.ffr,{padding:8}]}>{global.lang=='en'?"Model":'ماڈل'}</Text>
            <DropDownPicker
              items={[
                ...(() => {
                  return this.state.modeldata.length > 0
                    ? [{
                      label: "Select Model",
                      value: ""
                    },...this.state.modeldata.map((item) => {
                        return {
                          label: item.name,
                          value: item.id,
                        };
                      })]
                    : [
                        {
                          label: "No Model",
                          value: "",
                        },
                      ];
                })(),
              ]}
              placeholder="Select Model"
              placeholderStyle={{
                color:"#333"
              }}
              defaultValue={this.state.modelid}
              style={{backgroundColor: '#fff'}}
              selectedLabelStyle={[{color:'#363636'},styles.fs12,styles.ffm]}
              itemStyle={{
                  justifyContent: 'center'
              }}
              arrowSize={22}
              arrowColor="#333"
              style={[{width:Dimensions.get('screen').width-40}]}
              dropDownStyle={[{widhth:Dimensions.get('screen').width-40,}]}
              onChangeItem={item => {
                  console.log(item);
                  this.setState({modelid:item.value});
              }}
              zIndex={79999}
            />
            <Text style={[styles.menutxt,styles.fs10,styles.ffr,{padding:8}]}>{global.lang=='en'?"Year":'سال'}</Text>
            <DropDownPicker
              items={[
                ...(() => {
                  return this.state.yeardata.length > 0
                    ? [{
                      label: "Select Year",
                      value: ""
                    },...this.state.yeardata.map((item) => {
                        return {
                          label: item.name,
                          value: item.id,
                        };
                      })]
                    : [
                        {
                          label: "No Year",
                          value: "",
                        },
                      ];
                })(),
              ]}
              placeholder="Select Year"
              placeholderStyle={{
                color:"#333"
              }}
              defaultValue={this.state.yearid}
              style={{backgroundColor: '#fff'}}
              selectedLabelStyle={[{color:'#363636'},styles.fs12,styles.ffm]}
              itemStyle={{
                  justifyContent: 'center'
              }}
              arrowSize={22}
              arrowColor="#333"
              style={[{width:Dimensions.get('screen').width-40}]}
              dropDownStyle={[{widhth:Dimensions.get('screen').width-40,}]}
              onChangeItem={item => {
                  console.log(item);
                  this.setState({yearid:item.value})
              }}
              zIndex={6999}
            />
            
          </ScrollView>  
        </SafeAreaView>
      );
    }
}
