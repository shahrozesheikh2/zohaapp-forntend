import React from 'react'
import { SafeAreaView, Alert,TextInput, KeyboardAvoidingView,TouchableOpacity, Text, Image, View, ScrollView,Dimensions,Platform } from 'react-native'
import styles from '../../assets/css/styles';
import Share from '../../assets/images/share.png';
import BackButtonComponent from '../../components/BackButtonComponent';
import axios from 'axios';
import DropDownPicker from 'react-native-dropdown-picker';
import ImagePicker from 'react-native-customized-image-picker';
import ImageResizer from 'react-native-image-resizer';

export default class AddServiceItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          productname:'',
          productprice:'',
          category:'',
          categoryid:'',
          desc:'',
          brandname:'',
          brandid:'',
          modelname:'',
          yearname:'',
          modelid:'',
          yearid:'',
          //data
          categorydata:[],
          branddata:[],
          modeldata:[],
          yeardata:[
            {"name":"2021","id":"2021"},
            {"name":"2020","id":"2020"},
            {"name":"2019","id":"2019"},
            {"name":"2018","id":"2018"},
            {"name":"2017","id":"2017"},
            {"name":"2016","id":"2016"},
            {"name":"2015","id":"2015"},
            {"name":"2014","id":"2014"},
            {"name":"2013","id":"2013"},
            {"name":"2012","id":"2012"},
            {"name":"2011","id":"2011"},
            {"name":"2010","id":"2010"},
            {"name":"2009","id":"2009"},
            {"name":"2008","id":"2008"},
            {"name":"2007","id":"2007"},
            {"name":"2006","id":"2006"},
            {"name":"2005","id":"2005"},
            {"name":"2004","id":"2004"},
            {"name":"2003","id":"2003"},
            {"name":"2002","id":"2002"},
            {"name":"2001","id":"2001"},
            {"name":"2000","id":"2000"},
            {"name":"1999","id":"1999"},
            {"name":"1998","id":"1998"},
            {"name":"1997","id":"1997"},
            {"name":"1996","id":"1996"},
            {"name":"1995","id":"1995"},
            {"name":"1994","id":"1994"},
            {"name":"1993","id":"1993"},
            {"name":"1992","id":"1992"},
            {"name":"1991","id":"1991"},
            {"name":"1990","id":"1990"},
            {"name":"1989","id":"1989"},
            {"name":"1988","id":"1988"},
            {"name":"1987","id":"1987"},
            {"name":"1986","id":"1986"},
            {"name":"1985","id":"1985"},
            {"name":"1984","id":"1984"},
            {"name":"1983","id":"1983"},
            {"name":"1982","id":"1982"},
            {"name":"1981","id":"1981"},
            {"name":"1980","id":"1980"},
            {"name":"1979","id":"1979"},
            {"name":"1978","id":"1978"},
            {"name":"1977","id":"1977"},
            {"name":"1976","id":"1976"},
            {"name":"1975","id":"1975"},
            {"name":"1974","id":"1974"},
            {"name":"1973","id":"1973"},
            {"name":"1972","id":"1972"},
            {"name":"1971","id":"1971"},
            {"name":"1970","id":"1970"},
            {"name":"1969","id":"1969"},
            {"name":"1968","id":"1968"},
            {"name":"1967","id":"1967"},
            {"name":"1966","id":"1966"},
            {"name":"1965","id":"1965"},
            {"name":"1964","id":"1964"},
            {"name":"1963","id":"1963"},
            {"name":"1962","id":"1962"},
            {"name":"1961","id":"1961"},
            {"name":"1960","id":"1960"}
          ],
          imageassets:[],
          imageassetstosend:[]
        };
        this.getcategory();
        this.getbrand()
      }
      opengallery = async () => {
        if(this.state.imageassetstosend.length<5){
          ImagePicker.openPicker({imageLoader: 'UNIVERSAL',isCamera:true,multiple: true,maxSize:5-this.state.imageassetstosend.length}).then(response => {
            console.log(response);
            response.map((item)=>{
              ImageResizer.createResizedImage(item.path, 500, 500,'JPEG', 100).then(res => {
                this.updatepic(res.uri);
              })
              .catch(err => {
                console.log(err);
                alert('this image is not allowed')
              });
            }) 
          });
        }
        else{
          alert('You can only upload 5 images')
        }
        
      };
      updatepic = (image) => {
        const config = {
          headers: {
            "Content-Type": "multipart/form-data"
          },
        };
        var formdata = new FormData();
        formdata.append("file", {
          uri: image,
          type: "image/jpg",
          name: new Date().getTime()+"image.jpg"
        });
        const url = global.url + "/api/upload-resource";
        axios.post(url, formdata, config ).then((response) => {
          console.log(response.data);
          this.state.imageassets.push(response.data.result.data[0])
          console.log(this.state.imageassets);
          console.log(this.state.imageassets.length);
          this.setState({extra:true})
          var i;
          for(i=0;i<this.state.imageassets.length;i++){
            this.state.imageassetstosend[i]={
              "awsKey":this.state.imageassets[i].awsId,
              "awsUrl": this.state.imageassets[i].path
            }
            console.log('=============imageassetstosend=======================');
            console.log(this.state.imageassetstosend);
            console.log('===============imageassetstosend=====================');
          }
          
        }).catch((err) => {
          console.log(err.response);
        });
      };
      cancelimage=(index)=>{
        this.state.imageassets.splice(index, 1);
        this.state.imageassetstosend.splice(index, 1);
        this.setState({extra:true})
      }
      //get data
      getcategory=()=>{
        const url = global.url+'/api/service-category?offset=0&limit=100';
        axios.get(url).then(response => {
            console.log('====================================');
            console.log(response.data.result.data);
            console.log('====================================');
            this.setState({categorydata:response.data.result.data})
        }).catch(err=>{
        console.log(err)
        });
      }
      getbrand=()=>{
        const url = global.url+'/api/brand?offset=0&limit=100';
        axios.get(url).then(response => {
            console.log('====================================');
            console.log(response.data.result.data);
            console.log('====================================');
            this.setState({branddata:response.data.result.data})
        }).catch(err=>{
        console.log(err)
        });
      }
      getmodel=()=>{
        const url = global.url+'/api/model?offset=0&limit=100&id='+this.state.brandid;
        axios.get(url).then(response => {
            console.log('====================================');
            console.log(response.data.result.data);
            console.log('====================================');
            this.setState({modeldata:response.data.result.data})
        }).catch(err=>{
        console.log(err)
        });
      }
      getyear=()=>{
        this.setState({ editname:'',name:''})
        const url = global.url+'/api/year?offset=0&limit=100&id='+this.state.modelid;
        axios.get(url).then(response => {
            console.log('====================================');
            console.log(response.data.result.data);
            console.log('====================================');
            this.setState({yeardata:response.data.result.data})
        }).catch(err=>{
        console.log(err)
        });
      }
      //get data
      addProduct=()=>{
        var header={
          "Content-Type":"application/json",
          "Authorization":global.usertoken
        }
        var body={
          "categoryId":this.state.categoryid,
          "description":this.state.desc,
          "resources": this.state.imageassetstosend,
          "brandId":this.state.brandid,
          "modelId":this.state.modelid
        }
        console.log(body);
        if(this.state.desc==''){
            alert('Enter Description')
        }
        else if(this.state.categoryid==''){
          alert('Enter Category')
        }
        else if(this.state.brandid==''){
          alert('Enter Brand')
        }
        else if(this.state.modelid==''){
          alert('Enter Model')
        }
        else if(this.state.imageassetstosend.length==0){
          alert('Please add Service image')
        }
        else if(this.state.imageassetstosend.length>5){
          alert('Please upload only 5 images')
        }
        else{
          axios.post( global.url + '/api/service/',body,{headers:header} ).then(response => {
            console.log(response.data);
            alert('Service Added Successfully');
            this.props.navigation.replace('Seller');
          }).catch(error => {
            alert(error.response.data.message);
            console.log(error.response);
          });
        }
      }
      render() {
        return (
          <KeyboardAvoidingView style={{flex:1}} enabled={Platform.OS == "ios"} behavior={Platform.OS == "ios" ? "padding" : null} 
            keyboardVerticalOffset={40}>
          <SafeAreaView style={{ flex: 1 }}>
            <View style={{flexDirection:"row",borderBottomColor:'#d9d9d9',borderBottomWidth:1,paddingBottom:15}}>
              <BackButtonComponent navigation={this.props.navigation}/>
              <View  style={{flexDirection:"row" ,width:Dimensions.get('screen').width-80,justifyContent:'center'}}>
                <Text style={[styles.menutxt,styles.mt20, styles.fs16]}>{global.lang=='en'?"Add New Service":'نئی سروس شامل کریں'}</Text>
              </View>
              <View style={{position: 'absolute',right: 10,}}>
                <TouchableOpacity onPress={()=>{this.addProduct()}}>
                  <Text style={[styles.menutxt,styles.mt25,styles.fs14,styles.coloryellow,styles.ffb ]}>{global.lang=='en'?"Publish":'شائع کریں'}</Text>
                </TouchableOpacity>
              </View>
            </View>
            
            
            <ScrollView contentContainerStyle={{padding: 20,paddingBottom:170}}>
             
              <View>
                <Text style={[styles.menutxt,styles.fs10,styles.ffr,{padding:8}]}>{global.lang=='en'?"Service Description":'خدمت کی تفصیل'}</Text>
                <TextInput style={{ width:'100%',height:100, backgroundColor:'#f2f2f2',paddingTop:10,
                  borderRadius: 10,paddingHorizontal: 10, fontSize: 13,color:'black',textAlignVertical: 'top'
                  }}
                  value={this.state.desc}
                  onChangeText={(val)=>this.setState({desc:val})}
                  multiline={true}
                  numberOfLines={5}
                  placeholder={'Write here….'}
                  placeholderTextColor={'#7c7c7c'}
                />
              </View>
              <View>
                <Text style={[styles.menutxt,styles.fs10,styles.ffr,{padding:8}]}>{global.lang=='en'?"Services Previews":'خدمات کا پیش نظارہ'}</Text>
    
                <View style={{flexDirection:'row',flexWrap:'wrap'}}>
                  <TouchableOpacity  onPress={()=>{this.opengallery()}} style={[styles.homeview2,{height:100,width:100,marginRight:10}]}>
                      <Image source={require('../../assets/images/plus.png')} style={[styles.homeimg,{ width: 20, height: 20 }]} resizeMode={'cover'}/>
                  </TouchableOpacity>
                  {this.state.imageassets.map((item,index)=>{
                    return(
                      <View key={index} style={[styles.homeview2,{height:100,width:100,marginRight:10,
                        position: 'relative',}]}>
                        <Image source={{uri:item.path}} style={[styles.homeimg,{ width: 100, height: 100 }]} resizeMode={'cover'}/>
                        <TouchableOpacity  onPress={()=>{this.cancelimage(index)}} style={{position: 'absolute',bottom:0,right: 0,}}>
                          <Image source={require('../../assets/images/close.png')} style={[styles.homeimg,{ width: 30, height: 30 ,
                            }]} resizeMode={'contain'}/>
                        </TouchableOpacity>
                      </View>
                    )
                  })}
                </View>
                
                
              </View>
              <View>
                <Text style={[styles.menutxt,styles.fs10,styles.ffr,{padding:8}]}>{global.lang=='en'?"Service Category":'سروس کیٹیگری'}</Text>
    
                <DropDownPicker
                    items={[
                      ...(() => {
                        return this.state.categorydata.length > 0
                          ? [{
                            label: "Select Service Category",
                            value: ""
                          },...this.state.categorydata.map((item) => {
                              return {
                                label: item.name,
                                value: item.id,
                              };
                            })]
                          : [
                              {
                                label: "No Service Category",
                                value: "",
                              },
                            ];
                      })(),
                    ]}
                    placeholder="Select Service Category"
                    placeholderStyle={{
                      color:"#333"
                    }}
                    style={{backgroundColor: '#fff'}}
                    selectedLabelStyle={[{color:'#363636'},styles.fs12,styles.ffm]}
                    itemStyle={{
                        justifyContent: 'center'
                    }}
                    arrowSize={22}
                    arrowColor="#333"
                    style={[{width:Dimensions.get('screen').width-40}]}
                    dropDownStyle={[{widhth:Dimensions.get('screen').width-40,}]}
                    onChangeItem={item => {
                        console.log(item);
                        this.setState({categoryid:item.value})
                    }}
                    zIndex={9999999}
                />

              <Text style={[styles.menutxt,styles.fs10,styles.ffr,{padding:8}]}>{global.lang=='en'?"Brand":'برانڈ'}</Text>
    
              <DropDownPicker
                items={[
                  ...(() => {
                    return this.state.branddata.length > 0
                      ? [{
                        label: "Select Brand",
                        value: ""
                      },...this.state.branddata.map((item) => {
                          return {
                            label: item.name,
                            value: item.id,
                          };
                        })]
                      : [
                          {
                            label: "No Brand",
                            value: "",
                          },
                        ];
                  })(),
                ]}
                placeholder="Select Brand"
                placeholderStyle={{
                  color:"#333"
                }}
                style={{backgroundColor: '#fff'}}
                selectedLabelStyle={[{color:'#363636'},styles.fs12,styles.ffm]}
                itemStyle={{
                    justifyContent: 'center'
                }}
                arrowSize={22}
                arrowColor="#333"
                style={[{width:Dimensions.get('screen').width-40}]}
                dropDownStyle={[{widhth:Dimensions.get('screen').width-40,}]}
                onChangeItem={item => {
                    console.log(item);
                    this.setState({brandid:item.value});
                    setTimeout(() => {
                      this.getmodel()
                    }, 100);
                    
                }}
                zIndex={899999}
              />
              <Text style={[styles.menutxt,styles.fs10,styles.ffr,{padding:8}]}>{global.lang=='en'?"Model":'ماڈل'}</Text>
    
              <DropDownPicker
                items={[
                  ...(() => {
                    return this.state.modeldata.length > 0
                      ? [{
                        label: "Select Model",
                        value: ""
                      },...this.state.modeldata.map((item) => {
                          return {
                            label: item.name,
                            value: item.id,
                          };
                        })]
                      : [
                          {
                            label: "No Model",
                            value: "",
                          },
                        ];
                  })(),
                ]}
                placeholder="Select Model"
                placeholderStyle={{
                  color:"#333"
                }}
                style={{backgroundColor: '#fff'}}
                selectedLabelStyle={[{color:'#363636'},styles.fs12,styles.ffm]}
                itemStyle={{
                    justifyContent: 'center'
                }}
                arrowSize={22}
                arrowColor="#333"
                style={[{width:Dimensions.get('screen').width-40}]}
                dropDownStyle={[{widhth:Dimensions.get('screen').width-40,}]}
                onChangeItem={item => {
                    console.log(item);
                    this.setState({modelid:item.value});
                }}
                zIndex={79999}
              />
             
            </View>
            </ScrollView>  
            
          </SafeAreaView>
          </KeyboardAvoidingView>
        );
      }
}
