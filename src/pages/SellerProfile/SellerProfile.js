import React from 'react'
import { SafeAreaView, Alert,TextInput, TouchableOpacity, Text, Image, View, ScrollView, Dimensions,Platform } from 'react-native'
import styles from '../../assets/css/styles';
import Share from '../../assets/images/share.png';
import DropDownPicker from 'react-native-dropdown-picker';
import BackButtonComponent from '../../components/BackButtonComponent';
import axios from 'axios';
import ImagePicker from 'react-native-customized-image-picker';
import ImageResizer from 'react-native-image-resizer';
import GetLocation from 'react-native-get-location';
const options = {
  maxWidth: 500,
  maxHeight: 500,
  quality: 1
};
export default class SellerProfile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fullname:'',
      shopname:'',
      shopaddress:'',
      contactnumber:'',
      type:1,
      typeofselling:'',
      about:'',
      profileimage:'',
      awskey:'',
      awsurl:'',
      long:'',
      lat:''
    };
  }
  getLocation=()=>{
    GetLocation.getCurrentPosition({
      enableHighAccuracy: true,
      timeout: 15000,
    }).then(location => {
        console.log(location);
        this.setState({long:location.longitude,lat:location.latitude})
        
    }).catch(error => {
        const { code, message } = error;
        console.warn(code, message);
        this.setState({long:'',lat:''})
        alert('Turn on your location so nearby buyer can see your shop.');
    })
  }
  addShop=()=>{
    this.getLocation();
    setTimeout(() => {
      var header={
        "Content-Type":"application/json",
        "Authorization":global.usertoken
      }
      var body={
        "name" : this.state.shopname,
        "awsKey": this.state.awskey,
        "awsUrl": this.state.awsurl,
        "address": this.state.shopaddress,
        "phoneNumber": this.props.route.params.phone,
        "description": this.state.about,
        "typeId": this.state.type,
        "dealsWith": {
            "manufacturer": false,
            "wholeSeller": false,
            "importer": false
        },
        "location": {
          "coordinates": [this.state.lat,this.state.long]
        }
      }
      console.log(body);
      if(this.state.shopname==''){
        alert('Enter Shop Name')
      }
      else if(this.state.shopaddress==''){
        alert('Enter Shop Address')
      }
      else if(this.state.about==''){
        alert('Enter Little about your business')
      }
      else if(this.state.profileimage==''){
        alert('Upload profile image')
      }
      else if(this.state.lat=='' || this.state.long==''){
        alert('Turn on your location so nearby buyer can see your shop.')
      }
      else{
        axios.post( global.url + '/api/shop/add',body,{headers:header} ).then(response => {
          console.log(response.data);
          this.props.navigation.replace('Seller')
        }).catch(error => {
          alert(error.response.data.message)
          console.log(error.response);
        });
      }
    }, 100);
    
  }
  opengallery = () => {
    ImagePicker.openPicker({imageLoader: 'UNIVERSAL',isCamera:true,multiple: false}).then(response => {
      console.log(response);
      ImageResizer.createResizedImage(response[0].path, 500, 500,'JPEG', 100).then(res => {
        this.updatepic(res.uri);
      })
      .catch(err => {
        console.log(err);
        alert('this image is not allowed')
      });
    });
  };
  updatepic = (image) => {
    const config = {
      headers: {
        "Content-Type": "multipart/form-data"
      },
    };
    var formdata = new FormData();
    formdata.append("file", {
      uri: image,
      type: "image/jpg",
      name: new Date().getTime()+"image.jpg"
    });
    const url = global.url + "/api/upload-resource";
    axios.post(url, formdata, config ).then((response) => {
      console.log(response.data);
      this.setState({profileimage:response.data.result.data[0].path});
      this.setState({awskey:response.data.result.data[0].awsId});
      this.setState({awsurl:response.data.result.data[0].path}); 
    }).catch((err) => {
      console.log(err.response);
    });
  };
  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={{flexDirection:"row",justifyContent:'space-between',paddingRight:20,paddingBottom:10}}>
          <BackButtonComponent navigation={this.props.navigation}/>
        </View>

        <View >
          <Text style={[styles.menutxt,styles.fs25,{textAlign:'center'}]}>{global.lang=='en'?"Your First Selling Shop":'فروخت کنندہ کی پہلی دکان'}</Text>
          <Text style={[styles.menutxt,styles.mt10, styles.fs18,styles.ffm,styles.colorblack,{textAlign:'center'}]}>{global.lang=='en'?"Personalizing your initial shop profile":'اپنے ابتدائی دکان پروفائل کو ذاتی بنانا'}</Text>
        </View>
        <TouchableOpacity onPress={()=>{this.opengallery()}} style={{alignSelf:'center',paddingTop:40,paddingBottom:10,position: 'relative',}}>
          {(this.state.profileimage=='') && (
            <Image source={require('../../assets/images/profilepic.jpeg')} style={{ width: 100, height: 100,borderRadius:10 }} resizeMode={'cover'}/>
          )}
          {(this.state.profileimage!='') && (
            <Image source={{uri:this.state.profileimage}} style={{ width: 100, height: 100,borderRadius:10 }} resizeMode={'cover'}/>
          )}
          <Image source={require('../../assets/images/camera.png')} style={{ width: 40, height: 40,position:'absolute',right: 0,bottom:10 }} resizeMode={'contain'}/>
        </TouchableOpacity>
        
        <ScrollView contentContainerStyle={{padding: 20,}}>
          <View>
            <Text style={[styles.menutxt,styles.fs10,styles.ffr,{padding:8}]}>{global.lang=='en'?"Shop Name":'دکان کا نام'}</Text>

            <TextInput style={{ width:'100%',height:56, backgroundColor:'#ddd',
              borderRadius: 10,paddingHorizontal: 10, fontSize: 13,color:'black'
              }}
              onChangeText={(val)=>this.setState({shopname:val})}
              placeholder={'Ex: Federal Auto Workshop'}
              placeholderTextColor={'#7c7c7c'}
            />
          </View>
          <View>
            <Text style={[styles.menutxt,styles.fs10,styles.ffr,{padding:8}]}>{global.lang=='en'?"Shop Address":'دکان کا پتہ'}</Text>

            <TextInput style={{ width:'100%',height:56, backgroundColor:'#ddd',
              borderRadius: 10,paddingHorizontal: 10, fontSize: 13,color:'black'
              }}
              onChangeText={(val)=>this.setState({shopaddress:val})}
              placeholder={'Ex: F-10 Markaz Islamabad'}
              placeholderTextColor={'#7c7c7c'}
            />
          </View>
          <View>
            <Text style={[styles.menutxt,styles.fs10,styles.ffr,{padding:8}]}>{global.lang=='en'?"Contact Number":'رابطہ نمبر'}</Text>

            <TextInput style={{ width:'100%',height:56, backgroundColor:'#ddd',
              borderRadius: 10,paddingHorizontal: 10, fontSize: 13,color:'black'
              }}
              // onChangeText={(val)=>this.setState({contactnumber:val})}
              value={this.props.route.params.phone}
              placeholder={'Ex: +92 3345728849'}
              placeholderTextColor={'#7c7c7c'}
              keyboardType={"number-pad"}
              editable={false}
            />
          </View>
          {(Platform.OS=='ios') && (
            <View style={{zIndex:5000}} >
              <Text style={[styles.menutxt,styles.fs10,styles.ffr,{padding:8}]}>{global.lang=='en'?"Auto Parts Type":'آٹو پارٹس کی قسم'}</Text>

              <DropDownPicker
                  items={[
                      {label: 'Old Auto Parts', value: 1},
                      {label: 'New Auto Parts', value: 2}
                  ]}
                  defaultValue={this.state.type}
                  style={{backgroundColor: '#fff'}}
                  selectedLabelStyle={[{color:'#363636'},styles.fs12,styles.ffm]}
                  itemStyle={{
                      justifyContent: 'center'
                  }}
                  arrowSize={22}
                  arrowColor="#333"
                  style={[{width:Dimensions.get('screen').width-40}]}
                  dropDownStyle={[{widhth:Dimensions.get('screen').width-40,}]}
                  onChangeItem={item => {
                      console.log(item);
                      this.setState({type:item.value})
                  }}
                  zIndex={5000}
              />
            </View>
          )}
          
          {(Platform.OS=='android') && (
            <View>
              <Text style={[styles.menutxt,styles.fs10,styles.ffr,{padding:8}]}>{global.lang=='en'?"Auto Parts Type":'آٹو پارٹس کی قسم'}</Text>
              <DropDownPicker
                  items={[
                      {label: 'Old Auto Parts', value: 1},
                      {label: 'New Auto Parts', value: 2}
                  ]}
                  defaultValue={this.state.type}
                  style={{backgroundColor: '#fff'}}
                  selectedLabelStyle={[{color:'#363636'},styles.fs12,styles.ffm]}
                  itemStyle={{
                      justifyContent: 'center'
                  }}
                  arrowSize={22}
                  arrowColor="#333"
                  style={[{width:Dimensions.get('screen').width-40}]}
                  dropDownStyle={[{widhth:Dimensions.get('screen').width-40,}]}
                  onChangeItem={item => {
                      console.log(item);
                      this.setState({type:item.value})
                  }}
                  zIndex={5000}
              />
            </View>
          )}
          <View>
            <Text style={[styles.menutxt,styles.fs10,styles.ffr,{padding:8}]}>{global.lang=='en'?"Little about your business":'اپنے کاروبار کے بارے میں لکھیں'}</Text>
            <TextInput style={{ width:'100%',height:56, backgroundColor:'#ddd',
              borderRadius: 10,paddingHorizontal: 10, fontSize: 13,color:'black'
              }}
              onChangeText={(val)=>this.setState({about:val})}
              placeholder={'Write here….'}
              placeholderTextColor={'#7c7c7c'}
            />
          </View>
          <TouchableOpacity onPress={()=>{this.addShop()}}>
            <View style={[styles.bgcoloryellow,styles.br10,styles.p20,styles.mt10,{position:'relative'}]}>
              <Text style={[styles.colorblack,styles.ffm,styles.fs16,{alignSelf:'center'}]}>{global.lang=='en'?"Create my Shop":'اپنی دکان بنائیں'}</Text>
            </View>
          </TouchableOpacity>
        </ScrollView>
      
      </SafeAreaView>
    );
  }
}
