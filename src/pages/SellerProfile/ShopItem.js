import React from 'react'
import { SafeAreaView, TouchableOpacity, Text, Image, View, ScrollView } from 'react-native';
import styles from '../../assets/css/styles';
import axios from 'axios';
import { Overlay } from 'react-native-elements';
export default class ShopItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fullname:'',
      shopname:'',
      shopphone:'',
      shopaddress:'',
      contactnumber:'',
      typeofselling:'',
      about:'',
      rating:'',
      productdata:[],
      shopid:'',
      typeid:'',
      showOverlay:false,
    };
    this.getShop();
  }
  toggleOverlay = () => {
    this.setState({showOverlay:!this.state.showOverlay})
  }
  getShop=()=>{
    var header={
      "Content-Type":"application/json",
      "Authorization":global.usertoken
    }
    axios.get( global.url + '/api/shop/self',{headers:header}).then(response => {
      console.log(response.data.result.data);
      this.setState({
        shopname:response.data.result.data.Shop.name,
        about:response.data.result.data.Shop.description,
        rating:response.data.result.data.Shop.rating,
        shopimage:response.data.result.data.Shop.awsUrl,
        shopid:response.data.result.data.Shop.id,
        typeid:response.data.result.data.Shop.typeId,
        shopphone:response.data.result.data.Shop.phoneNumber
      })
      setTimeout(() => {
        this.getProducts();  
      }, 100);
    }).catch(error => {
      console.log(error.response);
    });
  }
  getProducts=()=>{
    var header={
      "Content-Type":"application/json",
      "Authorization":global.usertoken
    }
    axios.get( global.url + '/api/product/self?offset=0&limit=99',{headers:header}).then(response => {
      console.log(response.data.result.data);
      this.setState({
        productdata:response.data.result.data
      })
    }).catch(error => {
      console.log(error.response);
    });
  }
  
  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={{ backgroundColor:'#fff',paddingVertical: 20, borderBottomWidth: 1, borderBottomColor: 'rgb(230,230,230)', paddingHorizontal: 20}} >
          <View style={{flexDirection:'row',justifyContent:'space-between'}}>
            <View style={{flexDirection:'row'}}> 
              <TouchableOpacity onPress={()=>{this.setState({showOverlay:true});}}>
                <Image source={{uri:this.state.shopimage}} style={[{ width: 56, height: 56,borderRadius:10 }]} resizeMode={'cover'}/>
              </TouchableOpacity>
              <View>
                <Text style={[styles.menutxt,styles.fs25,styles.pl20,styles.pr20,{maxWidth:200}]}>{this.state.shopname}</Text>
                <Text style={[styles.menutxt,styles.fs16,styles.pl20,styles.pr20,styles.colorlightblack,{maxWidth:200}]}>+{this.state.shopphone}</Text>
                
              </View>
            </View>
    
            <TouchableOpacity onPress={()=>{this.props.navigation.navigate('EditProfile');}}>
              <Image source={require('../../assets/images/edit.png')} style={[{ width: 40, height: 40,borderRadius:10 }]} resizeMode={'cover'}/>
            </TouchableOpacity>
          </View>
          <View>
            <Text style={[styles.menutxt,styles.fs18,styles.mt10]}>{global.lang=='en'?"About Us":'ہمارے بارے میں معلومات'}</Text>
            <ScrollView style={[styles.mt10,{maxHeight:70}]}>
              <Text style={[styles.menutxt, styles.fs14,styles.ffr,styles.colorblack]}>{this.state.about}</Text>
            </ScrollView>
          </View>
          
        
        </View>
        
        <ScrollView>
          <View style={{flexDirection:'row',justifyContent:'space-between',flexWrap:'wrap',padding: 20,backgroundColor: '#f2f2f2',}}>
            <TouchableOpacity style={styles.homeview2} onPress={()=>{this.props.navigation.navigate('AddItem',{typeid:this.state.typeid})}}>
                <Image source={require('../../assets/images/plus.png')} style={[styles.homeimg,{ width: 40, height: 40 }]} resizeMode={'cover'}/>
                <View style={{marginLeft:10,marginRight:10}}>
                  <Text style={styles.hometxt3}>{global.lang=='en'?"Add New Item":'نیا آئٹم شامل کریں'}</Text>
                </View>
            </TouchableOpacity>
            {this.state.productdata.map((item,index)=>{
              return(
                <TouchableOpacity style={styles.homeview} key={index} onPress={()=>{this.props.navigation.navigate('EditItem',{product:item,typeid:this.state.typeid})}}>
                  <Image source={{uri:item.resources[0].awsUrl}} style={[styles.homeimg,{ width: '100%', height: 150 }]} resizeMode={'cover'}/>
                  <View style={{marginLeft:10,marginRight:10}}>
                    <Text style={[styles.hometxt,styles.ffm,styles.coloryellow,{fontSize:10,marginTop:5}]}>{item.Category.name}</Text>
                    <Text style={styles.hometxt2}>{item.name}</Text>
                    <Text style={styles.hometxt3}>Rs {item.price}/-</Text>
                  </View>
                </TouchableOpacity>
              )
            })}
            
          </View>
        </ScrollView>
        <Overlay overlayStyle={{padding:0,borderRadius:8,}} isVisible={this.state.showOverlay} onBackdropPress={this.toggleOverlay}>
          <View style={{width:300,overflow: 'hidden',}}>
            <Image source={{uri:this.state.shopimage}} style={[{ width: 300, height: 300,borderRadius:8,}]} resizeMode={'cover'}/>
          </View>
        </Overlay>
        
      </SafeAreaView>
    
    );
  }
}
