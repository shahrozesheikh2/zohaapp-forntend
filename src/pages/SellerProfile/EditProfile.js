import React from 'react'
import { SafeAreaView, TextInput, Alert,TouchableOpacity, Text, Image, View, ScrollView,Dimensions,Platform } from 'react-native'
import styles from '../../assets/css/styles';
import Share from '../../assets/images/share.png';
import BackButtonComponent from '../../components/BackButtonComponent';
import axios from 'axios';
import DropDownPicker from 'react-native-dropdown-picker';
import ImagePicker from 'react-native-customized-image-picker';
import ImageResizer from 'react-native-image-resizer';
import GetLocation from 'react-native-get-location';
export default class EditProfile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fullname:'',
      shopname:'',
      shopaddress:'',
      contactnumber:'',
      typeofselling:'',
      about:'',
      shopimage:'',
      type:'',
      awskey:'',
      awsurl:'',
      long:'',
      lat:''
    };
    this.getShop();
  }
  getLocation=()=>{
    GetLocation.getCurrentPosition({
      enableHighAccuracy: true,
      timeout: 15000,
    }).then(location => {
        console.log(location);
        this.setState({long:location.longitude,lat:location.latitude})
        
    }).catch(error => {
        const { code, message } = error;
        console.warn(code, message);
        this.setState({long:'',lat:''})
        alert('Turn on your location so nearby buyer can see your shop.');
    })
  }
  getShop=()=>{
    var header={
      "Content-Type":"application/json",
      "Authorization":global.usertoken
    }
    axios.get( global.url + '/api/shop/self',{headers:header}).then(response => {
      console.log(response.data);
      this.setState({
        shopname:response.data.result.data.Shop.name,
        shopaddress:response.data.result.data.Shop.address,
        contactnumber:response.data.result.data.Shop.phoneNumber,
        about:response.data.result.data.Shop.description,
        shopimage:response.data.result.data.Shop.awsUrl,
        type:response.data.result.data.Shop.typeId,
        awskey:response.data.result.data.Shop.awsKey,
        awsurl:response.data.result.data.Shop.awsUrl,
        
      })
    }).catch(error => {
      alert(error.response.data.message)
      console.log(error.response);
    });
  }
  editShop=()=>{
    this.getLocation();
    setTimeout(() => {
      var header={
        "Content-Type":"application/json",
        "Authorization":global.usertoken
      }
      var body={
        "name" : this.state.shopname,
        "awsKey": this.state.awskey,
        "awsUrl": this.state.awsurl,
        "address": this.state.shopaddress,
        "phoneNumber": this.state.contactnumber,
        "description": this.state.about,
        "typeId": this.state.type,
        "dealsWith": {
            "manufacturer": false,
            "wholeSeller": false,
            "importer": false
        },
        "location": {
          "coordinates": [this.state.lat,this.state.long]
        },
      }
      console.log(body);
  
      if(this.state.shopname==''){
        alert('Enter Shop Name')
      }
      else if(this.state.shopaddress==''){
        alert('Enter Shop Address')
      }
      else if(this.state.contactnumber==''){
        alert('Enter Contact Number')
      }
      else if(this.state.about==''){
        alert('Enter Little about your business')
      }
      else if(this.state.lat=='' || this.state.long==''){
        alert('Turn on your location so nearby buyer can see your shop.')
      }
      else{
        axios.post( global.url + '/api/shop/edit',body,{headers:header} ).then(response => {
          console.log(response.data);
          alert('Shop Edit Successfully');
          this.props.navigation.replace('Seller')
        }).catch(error => {
          console.log(error.response);
        });
      }
    }, 100);
  }
  opengallery = () => {
    ImagePicker.openPicker({imageLoader: 'UNIVERSAL',isCamera:true,multiple: false}).then(response => {
      console.log(response);
      ImageResizer.createResizedImage(response[0].path, 500, 500,'JPEG', 100).then(res => {
        this.updatepic(res.uri);
      })
      .catch(err => {
        console.log(err);
        alert('this image is not allowed')
      });
    });
  };
  updatepic = (image) => {
    const config = {
      headers: {
        "Content-Type": "multipart/form-data"
      },
    };
    var formdata = new FormData();
    formdata.append("file", {
      uri: image,
      type: "image/jpg",
      name: new Date().getTime()+"image.jpg"
    });
    const url = global.url + "/api/upload-resource";
    axios.post(url, formdata, config ).then((response) => {
      console.log(response.data);
      this.setState({shopimage:response.data.result.data[0].path});
      this.setState({awskey:response.data.result.data[0].awsId});
      this.setState({awsurl:response.data.result.data[0].path}); 
    }).catch((err) => {
      console.log(err.response);
    });
  };
  chooseImage=()=>{
    if (Platform.OS=='ios'){
      Alert.alert(
        "Choose Profile Image",
        "Select Image Upload Option",
        [
          {
            text: "Camera",
            onPress: () => this.launchcamera()
          },
          {
            text: "Gallery",
            onPress: () => this.opengallery()
          },
          {
            text: "Cancel",
            onPress: () => console.log("Cancel Pressed"),
            style: "destructive"
          }
        ],
        {cancelable: true}
      );
    }
    else{
      Alert.alert(
        "Choose Profile Image",
        "Select Image Upload Option",
        [
          {
            text: "Camera",
            onPress: () => this.launchcamera()
          },
          {
            text: "Gallery",
            onPress: () => this.opengallery()
          }
        ],
        {cancelable: true}
      );
    }
  }
  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={{flexDirection:"row",borderBottomColor:'#d9d9d9',borderBottomWidth:1,paddingBottom:15}}>
          <BackButtonComponent navigation={this.props.navigation}/>
          <View  style={{flexDirection:"row" ,width:Dimensions.get('screen').width-80,justifyContent:'center'}}>
            <Text style={[styles.menutxt,styles.mt20, styles.fs16]}>{global.lang=='en'?"Edit  Profile":'پروفائل میں ترمیم کریں'}</Text>
          </View>

          <View style={{position: 'absolute',right: 10,}}>
            <TouchableOpacity onPress={()=>{this.editShop()}}>
              <Text style={[styles.menutxt,styles.mt25,styles.fs14,styles.coloryellow,styles.ffb ]}>{global.lang=='en'?"Save Changes":'تبدیلیاں محفوظ کرو'}</Text>
            </TouchableOpacity>
          </View>
        </View>
        
        <View  style={{padding: 20,paddingBottom:0,position: 'relative',flexDirection:'row',justifyContent:'flex-start'}} >
          <TouchableOpacity  style={{position: 'relative',flexDirection:'row',justifyContent:'flex-start'}} onPress={()=>{this.opengallery()}}>
            <Image source={{uri:this.state.shopimage}} style={{ width: 100, height: 100,borderRadius:10 }} resizeMode={'cover'}/>
            <Image source={require('../../assets/images/camera.png')} style={{ width: 40, height: 40,position:'absolute',right: 0,bottom:0 }} resizeMode={'contain'}/>
          </TouchableOpacity>
        </View>
        
        
        <ScrollView contentContainerStyle={{padding: 20,}}>
          <View>
            <Text style={[styles.menutxt,styles.fs10,styles.ffr,{padding:8}]}>{global.lang=='en'?"Shop Name":'دکان کا نام'}</Text>

            <TextInput style={{ width:'100%',height:56, backgroundColor:'#ddd',
              borderRadius: 10,paddingHorizontal: 10, fontSize: 13,color:'black'
              }}
              value={this.state.shopname}
              onChangeText={(val)=>this.setState({shopname:val})}
              placeholder={'Ex: Federal Auto Workshop'}
              placeholderTextColor={'#7c7c7c'}
            />
          </View>
          <View>
            <Text style={[styles.menutxt,styles.fs10,styles.ffr,{padding:8}]}>{global.lang=='en'?"Shop Address":'دکان کا پتہ'}</Text>

            <TextInput style={{ width:'100%',height:56, backgroundColor:'#ddd',
              borderRadius: 10,paddingHorizontal: 10, fontSize: 13,color:'black'
              }}
              value={this.state.shopaddress}
              onChangeText={(val)=>this.setState({shopaddress:val})}
              placeholder={'Ex: F-10 Markaz Islamabad'}
              placeholderTextColor={'#7c7c7c'}
            />
          </View>
          <View>
            <Text style={[styles.menutxt,styles.fs10,styles.ffr,{padding:8}]}>{global.lang=='en'?"Contact Number":'رابطہ نمبر'}</Text>

            <TextInput style={{ width:'100%',height:56, backgroundColor:'#ddd',
              borderRadius: 10,paddingHorizontal: 10, fontSize: 13,color:'black'
              }}
              value={this.state.contactnumber}
              onChangeText={(val)=>this.setState({contactnumber:val})}
              placeholder={'Ex: +92 3345728849'}
              placeholderTextColor={'#7c7c7c'}
              keyboardType={"number-pad"}
              editable={false}
            />
          </View>
          {(Platform.OS=='ios') && (
            <View style={{zIndex:9999}} >
              <Text style={[styles.menutxt,styles.fs10,styles.ffr,{padding:8}]}>{global.lang=='en'?"Auto Parts Type":'آٹو پارٹس کی قسم'}</Text>
              <DropDownPicker
                  items={[
                      {label: 'Used Auto Parts', value: 1},
                      {label: 'New Auto Parts', value: 2}
                  ]}
                  placeholder="Select Type"
                  placeholderStyle={{
                    color:"#333"
                  }}
                  defaultValue={this.state.type}
                  style={{backgroundColor: '#fff'}}
                  selectedLabelStyle={[{color:'#363636'},styles.fs12,styles.ffm]}
                  itemStyle={{
                      justifyContent: 'center'
                  }}
                  arrowSize={22}
                  arrowColor="#333"
                  style={[{width:Dimensions.get('screen').width-40}]}
                  dropDownStyle={[{widhth:Dimensions.get('screen').width-40,}]}
                  onChangeItem={item => {
                      this.setState({type:item.value})
                  }}
                  zIndex={5000}
              />
            </View>
          )}

          {(Platform.OS=='android') && (
            <View>
              <Text style={[styles.menutxt,styles.fs10,styles.ffr,{padding:8}]}>{global.lang=='en'?"Auto Parts Type":'آٹو پارٹس کی قسم'}</Text>
              <DropDownPicker
                  items={[
                      {label: 'Used Auto Parts', value: 1},
                      {label: 'New Auto Parts', value: 2}
                  ]}
                  placeholder="Select Type"
                  placeholderStyle={{
                    color:"#333"
                  }}
                  defaultValue={this.state.type}
                  style={{backgroundColor: '#fff'}}
                  selectedLabelStyle={[{color:'#363636'},styles.fs12,styles.ffm]}
                  itemStyle={{
                      justifyContent: 'center'
                  }}
                  arrowSize={22}
                  arrowColor="#333"
                  style={[{width:Dimensions.get('screen').width-40}]}
                  dropDownStyle={[{widhth:Dimensions.get('screen').width-40,}]}
                  onChangeItem={item => {
                    this.setState({type:item.value})
                  }}
                  zIndex={5000}
              />
            </View>
          
          )}

          <View>
            <Text style={[styles.menutxt,styles.fs10,styles.ffr,{padding:8}]}>{global.lang=='en'?"Little about your business":'اپنے کاروبار کے بارے میں لکھیں'}</Text>

            <TextInput style={{ width:'100%',height:56, backgroundColor:'#ddd',
              borderRadius: 10,paddingHorizontal: 10, fontSize: 13,color:'black'
              }}
              value={this.state.about}
              onChangeText={(val)=>this.setState({about:val})}
              placeholder={'Write here….'}
              placeholderTextColor={'#7c7c7c'}
            />
          </View>
        </ScrollView>  
      </SafeAreaView>
    );
  }
}
