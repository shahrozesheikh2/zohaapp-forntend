import React, { Component } from 'react';
import { View, Text, ScrollView, Image,TouchableOpacity,Platform } from 'react-native';
import styles from '../../assets/css/styles';
import DropDownPicker from 'react-native-dropdown-picker';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default class RoleSelection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      role:'',
      lang:global.lang==null?'en':global.lang
    };
    console.log(global.lang);
    
  }
  changeLang=async()=>{
    console.log(this.state.lang);
    if(this.state.lang=='ud'){
      this.setState({lang:'en'});
      await AsyncStorage.setItem('lang', 'en');
      global.lang='en';
    }
    else{
      this.setState({lang:'ud'})
      await AsyncStorage.setItem('lang', 'ud');
      global.lang='ud';
    }
  }
  render() {
    return (
      <View style={{padding:15,flex:1}}>
          <ScrollView style={{ flex:1}}>
              {(Platform.OS=='ios') && (
                <View style={{justifyContent:'flex-end',flexDirection:'row',zIndex:9999}}>
                  <TouchableOpacity onPress={this.changeLang}>
                    <Text>{this.state.lang=='en'?"اردو":"English"}</Text>
                  </TouchableOpacity>
              </View>
              )}
              {(Platform.OS=='android') && (
              <View style={{justifyContent:'flex-end',flexDirection:'row'}}>
                <TouchableOpacity onPress={this.changeLang} style={{backgroundColor:'#f0ad34',padding:10,borderRadius:10}}>
                  <Text>{this.state.lang=='en'?"اردو":"English"}</Text>
                </TouchableOpacity>
              </View>
              )}
              
              <View style={{justifyContent:"center",alignContent:"center",flexDirection:'column',alignItems:"center",marginTop:40,marginBottom:40}}>
                <Text style={[styles.ffb,styles.fs25,styles.colorblack,styles.mb5]}>{this.state.lang=='en'?'Select Your Role':'اپنا کردار منتخب کریں'}</Text>
                <Text style={[styles.ffm,styles.fs18,styles.colorblack]}>{this.state.lang=='en'?'Either you are a buyer or seller':'آپ خرید کندہ ہیں یا فروخت کنندہ  ہیں'}</Text>
              </View>
              <View style={[styles.mt45,{flexDirection:'row',justifyContent:'space-evenly'}]}>
                  <TouchableOpacity onPress={()=>{this.setState({role:'buyer'})}}>
                    <Image source={require("../../assets/images/buyer.png")} resizeMode={"contain"} style={[{ width: this.state.role=='buyer'?95:75, height: this.state.role=='buyer'?165:165 }]} />
                    {this.state.role=='buyer'?
                      <View style={[styles.bgcoloryellow,styles.br10,styles.pl20,styles.pr20,styles.pt5,styles.pb5,styles.mt20]}>
                        <Text style={[styles.ffb,styles.fs14,styles.colorblack,{textAlign:'center'}]}>{this.state.lang=='en'?'Buyer':'خرید  کنندہ'}</Text>
                      </View>
                      :
                      <View>
                        <Text style={[styles.ffb,styles.fs14,styles.colorlightblack,styles.mt20,{textAlign:'center'}]}>{this.state.lang=='en'?'Buyer':'خرید  کنندہ'}</Text>
                      </View>
                    }
                  </TouchableOpacity>
                  <TouchableOpacity onPress={()=>{this.setState({role:'seller'})}}>
                    <Image source={require("../../assets/images/seller.png")} resizeMode={"contain"} style={[{ width: this.state.role=='seller'?95:75, height: this.state.role=='seller'?165:165 }]} />
                    {this.state.role=='seller'?
                      <View style={[styles.bgcoloryellow,styles.br10,styles.pl20,styles.pr20,styles.pt5,styles.pb5,styles.mt20]}>
                        <Text style={[styles.ffb,styles.fs14,styles.colorblack,{textAlign:'center'}]}>{this.state.lang=='en'?'Seller':'فروخت کنندہ'}</Text>
                      </View>
                      :
                      <View>
                        <Text style={[styles.ffb,styles.fs14,styles.colorlightblack,styles.mt20,{textAlign:'center'}]}>{this.state.lang=='en'?'Seller':'فروخت کنندہ'}</Text>
                      </View>
                    }
                  </TouchableOpacity>
              </View>
              {/* <Text>{global.fbtoken}</Text> */}
          </ScrollView>
          {(this.state.role!='') && (
            <View style={[styles.mb20,{justifyContent:'flex-end',padding:20}]}>
              <TouchableOpacity onPress={()=>{this.props.navigation.replace('Signin',{role:this.state.role})}}>
                  <View style={[styles.bgcoloryellow,styles.br10,styles.p20,styles.mt10,{position:'relative'}]}>
                      <Text style={[styles.colorblack,styles.ffm,styles.fs16,{alignSelf:'center'}]}>{this.state.lang=='en'?'Get Started':'شروع کریں'}</Text>
                      <Image source={require("../../assets/images/arrowright.png")} resizeMode={"contain"} style={[{ width: 16, height: 15,marginTop:3,position:'absolute',right: 20,top:20 }]} />
                  </View>
              </TouchableOpacity>
            </View>
          )}
          
      </View>
    );
  }
}
