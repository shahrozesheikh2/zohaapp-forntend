import React from 'react'
import { SafeAreaView, TextInput, Alert,TouchableOpacity, Text, Image, View, ScrollView,Dimensions,Platform } from 'react-native'
import styles from '../../assets/css/styles';
import Share from '../../assets/images/share.png';
import BackButtonComponent from '../../components/BackButtonComponent';
import axios from 'axios';
import RBSheet from "react-native-raw-bottom-sheet";
export default class ChangeCityAndName extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          fullname:this.props.route.params.data.fullName,
          city:'',
          cityid:this.props.route.params.data.cityId,
          citiesdata:[
            {
              "id":462,
              "cityName":"Rawalpindi",
              "countryId":1,
              "createdAt":"2021-05-20T21:31:34.000Z",
              "updatedAt":"2021-05-20T21:31:34.000Z",
              "deletedAt":null
            },
            {
              "id":467,
              "cityName":"Islamabad",
              "countryId":1,
              "createdAt":"2021-05-20T21:32:04.000Z",
              "updatedAt":"2021-05-20T21:32:04.000Z",
              "deletedAt":null
            },
            {
              "id":460,
              "cityName":"Lahore",
              "countryId":1,
              "createdAt":"2021-05-20T21:31:22.000Z",
              "updatedAt":"2021-05-20T21:31:22.000Z",
              "deletedAt":null
            },
            {
              "id":464,
              "cityName":"Peshawar",
              "countryId":1,
              "createdAt":"2021-05-20T21:31:47.000Z",
              "updatedAt":"2021-05-20T21:31:47.000Z",
              "deletedAt":null
            },
            {
              "id":468,
              "cityName":"Quetta",
              "countryId":1,
              "createdAt":"2021-05-20T21:32:10.000Z",
              "updatedAt":"2021-05-20T21:32:10.000Z",
              "deletedAt":null
            },
            {
              "id":459,
              "cityName":"Karachi",
              "countryId":1,
              "createdAt":"2021-05-20T21:31:14.000Z",
              "updatedAt":"2021-05-20T21:31:14.000Z",
              "deletedAt":null
            },
            {
            "id":500,
            "cityName":"Abbotabad",
            "countryId":1,
            "createdAt":"2021-05-20T21:35:25.000Z",
            "updatedAt":"2021-05-20T21:35:25.000Z",
            "deletedAt":null
            },
            {
            "id":529,
            "cityName":"Ahmedpur East",
            "countryId":1,
            "createdAt":"2021-05-20T21:37:27.000Z",
            "updatedAt":"2021-05-20T21:37:27.000Z",
            "deletedAt":null
            },
            {
            "id":548,
            "cityName":"Arif Wala",
            "countryId":1,
            "createdAt":"2021-05-20T21:39:02.000Z",
            "updatedAt":"2021-05-20T21:39:02.000Z",
            "deletedAt":null
            },
            {
            "id":521,
            "cityName":"Attock",
            "countryId":1,
            "createdAt":"2021-05-20T21:36:52.000Z",
            "updatedAt":"2021-05-20T21:36:52.000Z",
            "deletedAt":null
            },
            {
            "id":547,
            "cityName":"Badin",
            "countryId":1,
            "createdAt":"2021-05-20T21:38:58.000Z",
            "updatedAt":"2021-05-20T21:38:58.000Z",
            "deletedAt":null
            },
            {
            "id":514,
            "cityName":"Bahawalnagar",
            "countryId":1,
            "createdAt":"2021-05-20T21:36:23.000Z",
            "updatedAt":"2021-05-20T21:36:23.000Z",
            "deletedAt":null
            },
            {
            "id":469,
            "cityName":"Bahawalpur",
            "countryId":1,
            "createdAt":"2021-05-20T21:32:18.000Z",
            "updatedAt":"2021-05-20T21:32:18.000Z",
            "deletedAt":null
            },
            {
            "id":546,
            "cityName":"Bhakkar",
            "countryId":1,
            "createdAt":"2021-05-20T21:38:55.000Z",
            "updatedAt":"2021-05-20T21:38:55.000Z",
            "deletedAt":null
            },
            {
            "id":559,
            "cityName":"Bhalwal",
            "countryId":1,
            "createdAt":"2021-05-20T21:40:02.000Z",
            "updatedAt":"2021-05-20T21:40:02.000Z",
            "deletedAt":null
            },
            {
            "id":493,
            "cityName":"Burewala",
            "countryId":1,
            "createdAt":"2021-05-20T21:34:45.000Z",
            "updatedAt":"2021-05-20T21:34:45.000Z",
            "deletedAt":null
            },
            {
            "id":525,
            "cityName":"Chakwal",
            "countryId":1,
            "createdAt":"2021-05-20T21:37:08.000Z",
            "updatedAt":"2021-05-20T21:37:08.000Z",
            "deletedAt":null
            },
            {
            "id":536,
            "cityName":"Chaman",
            "countryId":1,
            "createdAt":"2021-05-20T21:38:04.000Z",
            "updatedAt":"2021-05-20T21:38:04.000Z",
            "deletedAt":null
            },
            {
            "id":545,
            "cityName":"Charsadda",
            "countryId":1,
            "createdAt":"2021-05-20T21:38:49.000Z",
            "updatedAt":"2021-05-20T21:38:49.000Z",
            "deletedAt":null
            },
            {
            "id":487,
            "cityName":"Chiniot",
            "countryId":1,
            "createdAt":"2021-05-20T21:34:11.000Z",
            "updatedAt":"2021-05-20T21:34:11.000Z",
            "deletedAt":null
            },
            {
            "id":519,
            "cityName":"Chishtian",
            "countryId":1,
            "createdAt":"2021-05-20T21:36:43.000Z",
            "updatedAt":"2021-05-20T21:36:43.000Z",
            "deletedAt":null
            },
            {
            "id":512,
            "cityName":"Dadu",
            "countryId":1,
            "createdAt":"2021-05-20T21:36:16.000Z",
            "updatedAt":"2021-05-20T21:36:16.000Z",
            "deletedAt":null
            },
            {
            "id":553,
            "cityName":"Daharki",
            "countryId":1,
            "createdAt":"2021-05-20T21:39:35.000Z",
            "updatedAt":"2021-05-20T21:39:35.000Z",
            "deletedAt":null
            },
            {
            "id":510,
            "cityName":"Daska",
            "countryId":1,
            "createdAt":"2021-05-20T21:36:07.000Z",
            "updatedAt":"2021-05-20T21:36:07.000Z",
            "deletedAt":null
            },
            {
            "id":478,
            "cityName":"Dera Ghazi Khan",
            "countryId":1,
            "createdAt":"2021-05-20T21:33:31.000Z",
            "updatedAt":"2021-05-20T21:33:31.000Z",
            "deletedAt":null
            },
            {
            "id":497,
            "cityName":"Dera Ismail Khan",
            "countryId":1,
            "createdAt":"2021-05-20T21:35:09.000Z",
            "updatedAt":"2021-05-20T21:35:09.000Z",
            "deletedAt":null
            },
            {
            "id":461,
            "cityName":"Faisalabad",
            "countryId":1,
            "createdAt":"2021-05-20T21:31:29.000Z",
            "updatedAt":"2021-05-20T21:31:29.000Z",
            "deletedAt":null
            },
            {
            "id":524,
            "cityName":"Ferozwala",
            "countryId":1,
            "createdAt":"2021-05-20T21:37:04.000Z",
            "updatedAt":"2021-05-20T21:37:04.000Z",
            "deletedAt":null
            },
            {
            "id":549,
            "cityName":"Ghotki",
            "countryId":1,
            "createdAt":"2021-05-20T21:39:08.000Z",
            "updatedAt":"2021-05-20T21:39:08.000Z",
            "deletedAt":null
            },
            {
            "id":511,
            "cityName":"Gojra",
            "countryId":1,
            "createdAt":"2021-05-20T21:36:11.000Z",
            "updatedAt":"2021-05-20T21:36:11.000Z",
            "deletedAt":null
            },
            {
            "id":463,
            "cityName":"Gujranwala",
            "countryId":1,
            "createdAt":"2021-05-20T21:31:42.000Z",
            "updatedAt":"2021-05-20T21:31:42.000Z",
            "deletedAt":null
            },
            {
            "id":479,
            "cityName":"Gujrat",
            "countryId":1,
            "createdAt":"2021-05-20T21:33:35.000Z",
            "updatedAt":"2021-05-20T21:33:35.000Z",
            "deletedAt":null
            },
            {
            "id":490,
            "cityName":"Hafizabad",
            "countryId":1,
            "createdAt":"2021-05-20T21:34:22.000Z",
            "updatedAt":"2021-05-20T21:34:22.000Z",
            "deletedAt":null
            },
            {
            "id":552,
            "cityName":"Haroonabad",
            "countryId":1,
            "createdAt":"2021-05-20T21:39:31.000Z",
            "updatedAt":"2021-05-20T21:39:31.000Z",
            "deletedAt":null
            },
            {
            "id":544,
            "cityName":"Hasilpur",
            "countryId":1,
            "createdAt":"2021-05-20T21:38:39.000Z",
            "updatedAt":"2021-05-20T21:38:39.000Z",
            "deletedAt":null
            },
            {
            "id":509,
            "cityName":"Hub",
            "countryId":1,
            "createdAt":"2021-05-20T21:36:03.000Z",
            "updatedAt":"2021-05-20T21:36:03.000Z",
            "deletedAt":null
            },
            {
            "id":466,
            "cityName":"Hyderabad",
            "countryId":1,
            "createdAt":"2021-05-20T21:32:00.000Z",
            "updatedAt":"2021-05-20T21:32:00.000Z",
            "deletedAt":null
            },
            
            {
            "id":503,
            "cityName":"Jacobabad",
            "countryId":1,
            "createdAt":"2021-05-20T21:35:39.000Z",
            "updatedAt":"2021-05-20T21:35:39.000Z",
            "deletedAt":null
            },
            {
            "id":518,
            "cityName":"Jaranwala",
            "countryId":1,
            "createdAt":"2021-05-20T21:36:40.000Z",
            "updatedAt":"2021-05-20T21:36:40.000Z",
            "deletedAt":null
            },
            {
            "id":551,
            "cityName":"Jatoi",
            "countryId":1,
            "createdAt":"2021-05-20T21:39:27.000Z",
            "updatedAt":"2021-05-20T21:39:27.000Z",
            "deletedAt":null
            },
            {
            "id":477,
            "cityName":"Jhang",
            "countryId":1,
            "createdAt":"2021-05-20T21:33:23.000Z",
            "updatedAt":"2021-05-20T21:33:23.000Z",
            "deletedAt":null
            },
            {
            "id":504,
            "cityName":"Jhelum",
            "countryId":1,
            "createdAt":"2021-05-20T21:35:43.000Z",
            "updatedAt":"2021-05-20T21:35:43.000Z",
            "deletedAt":null
            },
            {
            "id":542,
            "cityName":"Kabal",
            "countryId":1,
            "createdAt":"2021-05-20T21:38:30.000Z",
            "updatedAt":"2021-05-20T21:38:30.000Z",
            "deletedAt":null
            },
            {
            "id":527,
            "cityName":"Kamalia",
            "countryId":1,
            "createdAt":"2021-05-20T21:37:19.000Z",
            "updatedAt":"2021-05-20T21:37:19.000Z",
            "deletedAt":null
            },
            {
            "id":556,
            "cityName":"Kamber Ali Khan",
            "countryId":1,
            "createdAt":"2021-05-20T21:39:49.000Z",
            "updatedAt":"2021-05-20T21:39:49.000Z",
            "deletedAt":null
            },
            {
            "id":489,
            "cityName":"Kāmoke",
            "countryId":1,
            "createdAt":"2021-05-20T21:34:18.000Z",
            "updatedAt":"2021-05-20T21:34:18.000Z",
            "deletedAt":null
            },
            {
            "id":558,
            "cityName":"Kandhkot",
            "countryId":1,
            "createdAt":"2021-05-20T21:39:57.000Z",
            "updatedAt":"2021-05-20T21:39:57.000Z",
            "deletedAt":null
            },
            
            {
            "id":483,
            "cityName":"Kasur",
            "countryId":1,
            "createdAt":"2021-05-20T21:33:56.000Z",
            "updatedAt":"2021-05-20T21:33:56.000Z",
            "deletedAt":null
            },
            {
            "id":506,
            "cityName":"Khairpur",
            "countryId":1,
            "createdAt":"2021-05-20T21:35:51.000Z",
            "updatedAt":"2021-05-20T21:35:51.000Z",
            "deletedAt":null
            },
            {
            "id":495,
            "cityName":"Khanewal",
            "countryId":1,
            "createdAt":"2021-05-20T21:34:54.000Z",
            "updatedAt":"2021-05-20T21:34:54.000Z",
            "deletedAt":null
            },
            {
            "id":505,
            "cityName":"Khanpur",
            "countryId":1,
            "createdAt":"2021-05-20T21:35:47.000Z",
            "updatedAt":"2021-05-20T21:35:47.000Z",
            "deletedAt":null
            },
            {
            "id":539,
            "cityName":"Khushab",
            "countryId":1,
            "createdAt":"2021-05-20T21:38:17.000Z",
            "updatedAt":"2021-05-20T21:38:17.000Z",
            "deletedAt":null
            },
            {
            "id":507,
            "cityName":"Khuzdar",
            "countryId":1,
            "createdAt":"2021-05-20T21:35:55.000Z",
            "updatedAt":"2021-05-20T21:35:55.000Z",
            "deletedAt":null
            },
            {
            "id":494,
            "cityName":"Kohat",
            "countryId":1,
            "createdAt":"2021-05-20T21:34:49.000Z",
            "updatedAt":"2021-05-20T21:34:49.000Z",
            "deletedAt":null
            },
            {
            "id":523,
            "cityName":"Kot Abdul Malik",
            "countryId":1,
            "createdAt":"2021-05-20T21:37:00.000Z",
            "updatedAt":"2021-05-20T21:37:00.000Z",
            "deletedAt":null
            },
            {
            "id":530,
            "cityName":"Kot Addu",
            "countryId":1,
            "createdAt":"2021-05-20T21:37:30.000Z",
            "updatedAt":"2021-05-20T21:37:30.000Z",
            "deletedAt":null
            },
            {
            "id":488,
            "cityName":"Kotri",
            "countryId":1,
            "createdAt":"2021-05-20T21:34:15.000Z",
            "updatedAt":"2021-05-20T21:34:15.000Z",
            "deletedAt":null
            },
            
            {
            "id":474,
            "cityName":"Larkana",
            "countryId":1,
            "createdAt":"2021-05-20T21:33:06.000Z",
            "updatedAt":"2021-05-20T21:33:06.000Z",
            "deletedAt":null
            },
            {
            "id":533,
            "cityName":"Layyah",
            "countryId":1,
            "createdAt":"2021-05-20T21:37:49.000Z",
            "updatedAt":"2021-05-20T21:37:49.000Z",
            "deletedAt":null
            },
            {
            "id":543,
            "cityName":"Lodhran",
            "countryId":1,
            "createdAt":"2021-05-20T21:38:34.000Z",
            "updatedAt":"2021-05-20T21:38:34.000Z",
            "deletedAt":null
            },
            {
            "id":501,
            "cityName":"Mandi Bahauddin",
            "countryId":1,
            "createdAt":"2021-05-20T21:35:30.000Z",
            "updatedAt":"2021-05-20T21:35:30.000Z",
            "deletedAt":null
            },
            {
            "id":532,
            "cityName":"Mansehra",
            "countryId":1,
            "createdAt":"2021-05-20T21:37:44.000Z",
            "updatedAt":"2021-05-20T21:37:44.000Z",
            "deletedAt":null
            },
            {
            "id":482,
            "cityName":"Mardan",
            "countryId":1,
            "createdAt":"2021-05-20T21:33:52.000Z",
            "updatedAt":"2021-05-20T21:33:52.000Z",
            "deletedAt":null
            },
            {
            "id":541,
            "cityName":"Mianwali",
            "countryId":1,
            "createdAt":"2021-05-20T21:38:26.000Z",
            "updatedAt":"2021-05-20T21:38:26.000Z",
            "deletedAt":null
            },
            {
            "id":485,
            "cityName":"Mingora",
            "countryId":1,
            "createdAt":"2021-05-20T21:34:04.000Z",
            "updatedAt":"2021-05-20T21:34:04.000Z",
            "deletedAt":null
            },
            {
            "id":534,
            "cityName":"Mirpur",
            "countryId":1,
            "createdAt":"2021-05-20T21:37:54.000Z",
            "updatedAt":"2021-05-20T21:37:54.000Z",
            "deletedAt":null
            },
            {
            "id":492,
            "cityName":"Mirpur Khas",
            "countryId":1,
            "createdAt":"2021-05-20T21:34:40.000Z",
            "updatedAt":"2021-05-20T21:34:40.000Z",
            "deletedAt":null
            },
            {
            "id":557,
            "cityName":"Mirpur Mathelo",
            "countryId":1,
            "createdAt":"2021-05-20T21:39:53.000Z",
            "updatedAt":"2021-05-20T21:39:53.000Z",
            "deletedAt":null
            },
            {
            "id":465,
            "cityName":"Multan",
            "countryId":1,
            "createdAt":"2021-05-20T21:31:55.000Z",
            "updatedAt":"2021-05-20T21:31:55.000Z",
            "deletedAt":null
            },
            {
            "id":513,
            "cityName":"Muridke",
            "countryId":1,
            "createdAt":"2021-05-20T21:36:19.000Z",
            "updatedAt":"2021-05-20T21:36:19.000Z",
            "deletedAt":null
            },
            {
            "id":520,
            "cityName":"Muzaffarabad",
            "countryId":1,
            "createdAt":"2021-05-20T21:36:47.000Z",
            "updatedAt":"2021-05-20T21:36:47.000Z",
            "deletedAt":null
            },
            {
            "id":499,
            "cityName":"Muzaffargarh",
            "countryId":1,
            "createdAt":"2021-05-20T21:35:19.000Z",
            "updatedAt":"2021-05-20T21:35:19.000Z",
            "deletedAt":null
            },
            {
            "id":554,
            "cityName":"Narowal",
            "countryId":1,
            "createdAt":"2021-05-20T21:39:39.000Z",
            "updatedAt":"2021-05-20T21:39:39.000Z",
            "deletedAt":null
            },
            {
            "id":486,
            "cityName":"Nawabshah",
            "countryId":1,
            "createdAt":"2021-05-20T21:34:07.000Z",
            "updatedAt":"2021-05-20T21:34:07.000Z",
            "deletedAt":null
            },
            {
            "id":538,
            "cityName":"Nowshera",
            "countryId":1,
            "createdAt":"2021-05-20T21:38:13.000Z",
            "updatedAt":"2021-05-20T21:38:13.000Z",
            "deletedAt":null
            },
            {
            "id":484,
            "cityName":"Okara",
            "countryId":1,
            "createdAt":"2021-05-20T21:34:00.000Z",
            "updatedAt":"2021-05-20T21:34:00.000Z",
            "deletedAt":null
            },
            {
            "id":508,
            "cityName":"Pakpattan",
            "countryId":1,
            "createdAt":"2021-05-20T21:35:59.000Z",
            "updatedAt":"2021-05-20T21:35:59.000Z",
            "deletedAt":null
            },
            
            
            {
            "id":476,
            "cityName":"Rahim Yar Khan",
            "countryId":1,
            "createdAt":"2021-05-20T21:33:18.000Z",
            "updatedAt":"2021-05-20T21:33:18.000Z",
            "deletedAt":null
            },
            
            {
            "id":491,
            "cityName":"Sadiqabad",
            "countryId":1,
            "createdAt":"2021-05-20T21:34:34.000Z",
            "updatedAt":"2021-05-20T21:34:34.000Z",
            "deletedAt":null
            },
            {
            "id":480,
            "cityName":"Sahiwal",
            "countryId":1,
            "createdAt":"2021-05-20T21:33:41.000Z",
            "updatedAt":"2021-05-20T21:33:41.000Z",
            "deletedAt":null
            },
            {
            "id":550,
            "cityName":"Sambrial",
            "countryId":1,
            "createdAt":"2021-05-20T21:39:22.000Z",
            "updatedAt":"2021-05-20T21:39:22.000Z",
            "deletedAt":null
            },
            {
            "id":515,
            "cityName":"Samundri",
            "countryId":1,
            "createdAt":"2021-05-20T21:36:26.000Z",
            "updatedAt":"2021-05-20T21:36:26.000Z",
            "deletedAt":null
            },
            {
            "id":470,
            "cityName":"Sargodha",
            "countryId":1,
            "createdAt":"2021-05-20T21:32:22.000Z",
            "updatedAt":"2021-05-20T21:32:22.000Z",
            "deletedAt":null
            },
            {
            "id":540,
            "cityName":"Shahdadkot",
            "countryId":1,
            "createdAt":"2021-05-20T21:38:21.000Z",
            "updatedAt":"2021-05-20T21:38:21.000Z",
            "deletedAt":null
            },
            {
            "id":475,
            "cityName":"Sheikhupura",
            "countryId":1,
            "createdAt":"2021-05-20T21:33:12.000Z",
            "updatedAt":"2021-05-20T21:33:12.000Z",
            "deletedAt":null
            },
            {
            "id":502,
            "cityName":"Shikarpur",
            "countryId":1,
            "createdAt":"2021-05-20T21:35:35.000Z",
            "updatedAt":"2021-05-20T21:35:35.000Z",
            "deletedAt":null
            },
            {
            "id":471,
            "cityName":"Sialkot",
            "countryId":1,
            "createdAt":"2021-05-20T21:32:26.000Z",
            "updatedAt":"2021-05-20T21:32:26.000Z",
            "deletedAt":null
            },
            {
            "id":472,
            "cityName":"Sukkur",
            "countryId":1,
            "createdAt":"2021-05-20T21:32:30.000Z",
            "updatedAt":"2021-05-20T21:32:30.000Z",
            "deletedAt":null
            },
            {
            "id":535,
            "cityName":"Swabi",
            "countryId":1,
            "createdAt":"2021-05-20T21:38:00.000Z",
            "updatedAt":"2021-05-20T21:38:00.000Z",
            "deletedAt":null
            },
            {
            "id":517,
            "cityName":"Tando Adam",
            "countryId":1,
            "createdAt":"2021-05-20T21:36:36.000Z",
            "updatedAt":"2021-05-20T21:36:36.000Z",
            "deletedAt":null
            },
            {
            "id":516,
            "cityName":"Tando Allahyar",
            "countryId":1,
            "createdAt":"2021-05-20T21:36:32.000Z",
            "updatedAt":"2021-05-20T21:36:32.000Z",
            "deletedAt":null
            },
            {
            "id":555,
            "cityName":"Tando Muhammad Khan",
            "countryId":1,
            "createdAt":"2021-05-20T21:39:44.000Z",
            "updatedAt":"2021-05-20T21:39:44.000Z",
            "deletedAt":null
            },
            {
            "id":537,
            "cityName":"Taxila",
            "countryId":1,
            "createdAt":"2021-05-20T21:38:08.000Z",
            "updatedAt":"2021-05-20T21:38:08.000Z",
            "deletedAt":null
            },
            {
            "id":498,
            "cityName":"Turbat",
            "countryId":1,
            "createdAt":"2021-05-20T21:35:14.000Z",
            "updatedAt":"2021-05-20T21:35:14.000Z",
            "deletedAt":null
            },
            {
            "id":528,
            "cityName":"Umerkot",
            "countryId":1,
            "createdAt":"2021-05-20T21:37:23.000Z",
            "updatedAt":"2021-05-20T21:37:23.000Z",
            "deletedAt":null
            },
            {
            "id":522,
            "cityName":"Vehari",
            "countryId":1,
            "createdAt":"2021-05-20T21:36:56.000Z",
            "updatedAt":"2021-05-20T21:36:56.000Z",
            "deletedAt":null
            },
            {
            "id":481,
            "cityName":"Wah Cantonment",
            "countryId":1,
            "createdAt":"2021-05-20T21:33:46.000Z",
            "updatedAt":"2021-05-20T21:33:46.000Z",
            "deletedAt":null
            },
            {
            "id":531,
            "cityName":"Wazirabad",
            "countryId":1,
            "createdAt":"2021-05-20T21:37:35.000Z",
            "updatedAt":"2021-05-20T21:37:35.000Z",
            "deletedAt":null
            }
          ],
          profileimage:this.props.route.params.data.awsUrl,
          awskey:this.props.route.params.data.awsKey,
          awsurl:this.props.route.params.data.awsUrl
        };
    }
    editNameCity=()=>{
    var header={
        "Content-Type":"application/json",
        "Authorization":global.usertoken
    }
    var body={
        "fullName":this.state.fullname,
        "countryId":1,
        "cityId": this.state.cityid,
        "awsKey": this.state.awskey,
        "awsUrl": this.state.awsurl
    }
    console.log(body);
    if(this.state.fullname==''){
        alert('Enter Name')
    }
    else{
        axios.post( global.url + '/api/user/edit-profile',body,{headers:header} ).then(response => {
        console.log(response.data);
        alert('Profile Edit Successfully');
        this.props.navigation.goBack();
        }).catch(error => {
        console.log(error.response);
        });
    }
    }
    render() {
    return (
        <SafeAreaView style={{ flex: 1 }}>
        <View style={{flexDirection:"row",borderBottomColor:'#d9d9d9',borderBottomWidth:1,paddingBottom:15}}>
            <BackButtonComponent navigation={this.props.navigation}/>
            <View style={{position: 'absolute',right: 10,}}>
            <TouchableOpacity onPress={()=>{this.editNameCity()}}>
                <Text style={[styles.menutxt,styles.mt25,styles.fs14,styles.coloryellow,styles.ffb ]}>{global.lang=='en'?'Save Changes':'تبدیلیاں محفوظ کرو'}</Text>
            </TouchableOpacity>
            </View>
        </View>
        
        {/* <View  style={{padding: 20,paddingBottom:0,position: 'relative',flexDirection:'row',justifyContent:'flex-start'}} onPress={()=>{this.chooseImage()}}>
            <TouchableOpacity  style={{position: 'relative',flexDirection:'row',justifyContent:'flex-start'}} onPress={()=>{this.chooseImage()}}>
            {(this.state.profileimage=='') && (
            <Image source={require('../../assets/images/profilepic.jpeg')} style={{ width: 100, height: 100,borderRadius:10 }} resizeMode={'cover'}/>
            )}
            {(this.state.profileimage!='') && (
            <Image source={{uri:this.state.profileimage}} style={{ width: 100, height: 100,borderRadius:10 }} resizeMode={'cover'}/>
            )}
            <Image source={require('../../assets/images/camera.png')} style={{ width: 40, height: 40,position:'absolute',right: 0,bottom:0 }} resizeMode={'contain'}/>
            </TouchableOpacity>
        </View> */}
        
        
        <ScrollView contentContainerStyle={{padding: 20,}}>
            <View>
                <Text style={[styles.menutxt,styles.fs10,styles.ffr,{padding:8}]}>{global.lang=='en'?'Name':'نام'}</Text>
                <TextInput style={{ width:'100%',height:56, backgroundColor:'#ddd', borderRadius: 10,paddingHorizontal: 10, fontSize: 13,color:'black' }}
                    value={this.state.fullname}
                    onChangeText={(val)=>this.setState({fullname:val})}
                    placeholder={'Ex: User Name'}
                    placeholderTextColor={'#7c7c7c'}
                />
            </View>
            <TouchableOpacity onPress={() => this.RBSheet.open()}>
            <View style={{borderColor:'#cccccc',borderWidth:1,padding:20,marginTop:20,borderRadius:8,flexDirection:'row',justifyContent:'space-between'}}>
                {global.lang!='en' && (
                    <Text style={{ color: '#1a1a1a', fontSize: 13, }}>{this.state.city!=''?this.state.city:'اپنے شہر کا انتخاب کریں'}</Text>
                )}
                {global.lang=='en' && (
                    <Text style={{ color: '#1a1a1a', fontSize: 13, }}>{this.state.city!=''?this.state.city:'Select your city'}</Text>
                )}
                <Image source={require('../../assets/images/dropdownicon.png')} style={[styles.homeimg,{ width: 10, height: 6 ,marginTop:6 }]} resizeMode={'contain'}/>
            </View>
            </TouchableOpacity>
        </ScrollView>  
        <RBSheet ref={ref => { this.RBSheet = ref; }} height={400} openDuration={250}  >
            <View>
            <View style={{ borderBottomColor:'#d9d9d9',borderBottomWidth:1,position: 'relative', }}>
                <Text style={[styles.ffb,styles.fs16,styles.colorblack,styles.p20,{textAlign:'center'}]}>{global.lang=='en'?'Select Your City':'اپنا شہر منتخب کریں'}</Text>
                <View style={{position: 'absolute',right:0}}>
                <TouchableOpacity onPress={() => this.RBSheet.close()}>
                    <Text style={[styles.ffb,styles.fs16,styles.coloryellow,styles.p20,{textAlign:'center'}]}>{global.lang=='en'?'Done':'ہو گیا'}Done</Text>
                </TouchableOpacity>
                </View>
            </View>
            <ScrollView contentContainerStyle={{padding:20,paddingBottom:100}}>
                {this.state.citiesdata.map((item, index) => {
                return (
                    <TouchableOpacity key={index} onPress={() => {this.setState({city:item.cityName,cityid:item.id});this.RBSheet.close();}}>
                    <Text style={[styles.ffm,styles.fs14,styles.colorblack,styles.p10]}>{item.cityName}</Text>
                    </TouchableOpacity>
                )
                })}
            </ScrollView>
            </View>
        </RBSheet>
        
        </SafeAreaView>
    );
    }
}
