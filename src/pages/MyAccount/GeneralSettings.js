import React, { Component } from "react";
import {
  Text,
  View,
  SafeAreaView,
  Image,
  TouchableOpacity,
  ScrollView,
  Dimensions
} from "react-native";
import styles from "../../assets/css/styles";
import BackButtonComponent from '../../components/BackButtonComponent';
import axios from 'axios';
import RBSheet from "react-native-raw-bottom-sheet";
import RNRestart from 'react-native-restart';
import AsyncStorage from '@react-native-async-storage/async-storage';
export default class GeneralSettings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userdata:''
    };
    this.getuserdata();
  }
  componentDidMount() {
    this._unsubscribe = this.props.navigation.addListener('focus', () => {
      this.getuserdata();
    });
  }

  componentWillUnmount() {
    this._unsubscribe();
  }
  getuserdata=()=>{
    var header={
      "Authorization":global.usertoken
    }
    axios.get( global.url + '/api/user/self',{headers:header} ).then(response => {
      console.log(response.data);
      this.setState({userdata:response.data.result.data})
    }).catch(error => {
      console.log(error.response);
    });
  }
  changeLangEng=async()=>{
    await AsyncStorage.setItem('lang', 'en');
    global.lang='en';
    this.setState({lang:'English'});
    this.RBSheet.close();
    RNRestart.Restart();
  }
  changeLangUrdu=async()=>{
    await AsyncStorage.setItem('lang', 'ud');
    global.lang='ud';
    this.setState({lang:'اردو'});
    this.RBSheet.close();
    RNRestart.Restart();
  }
  render() {
    return (
      <SafeAreaView style={{ flex: 1,}}>
        <View style={{flexDirection:"row"}}>
          <BackButtonComponent navigation={this.props.navigation}/>
          <View  style={{flexDirection:"row" ,width:Dimensions.get('screen').width-100,justifyContent:'center'}}>
            <Text style={[styles.menutxt,styles.mt20, styles.fs16]}>{global.lang=='en'?'General Settings':'عام ترتیبات'}</Text>
          </View>
        </View>
        <View style={{ padding:16 }}>
          <ScrollView >
            <TouchableOpacity onPress={()=>{this.props.navigation.push('ChangePassword',{phone:this.state.userdata.phoneNumber})}}>
              <View style={styles.menuview}>
                <View>
                  <Text style={styles.menutxt}>{global.lang=='en'?'Change Phone Number':'فون نمبر تبدیل کریں'}</Text>
                  <Text style={styles.menutxt2}>{global.lang=='en'?'Current Number':'موجودہ نمبر'} {this.state.userdata.phoneNumber}</Text>
                </View>
                <Image source={require('../../assets/images/menuarrow.png')} style={{ width: 7, height: 12 }} resizeMode={'contain'}/>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>{this.props.navigation.push('ChangeCityAndName',{data:this.state.userdata})}}>
              <View style={styles.menuview}>
                <View>
                  <Text style={styles.menutxt}>{global.lang=='en'?'Change Name and City':'نام اور شہر تبدیل کریں'}</Text>
                  <Text style={styles.menutxt2}>{this.state.userdata.fullName}</Text>
                </View>
                <Image source={require('../../assets/images/menuarrow.png')} style={{ width: 7, height: 12 }} resizeMode={'contain'}/>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.RBSheet.open()}>
              <View style={styles.menuview}>
                <View>
                  <Text style={styles.menutxt}>{global.lang=='en'?'Change Language':'زبان تبدیل کریں'}</Text>
                  <Text style={styles.menutxt2}>{global.lang=='en'?'English':'اردو'}</Text>
                </View>
                <Image source={require('../../assets/images/menuarrow.png')} style={{ width: 7, height: 12 }} resizeMode={'contain'}/>
              </View>
            </TouchableOpacity>
          </ScrollView>
        </View>
        <RBSheet ref={ref => { this.RBSheet = ref; }} height={180} openDuration={250}  >
          <View>
            <View style={{ borderBottomColor:'#d9d9d9',borderBottomWidth:1,position: 'relative', }}>
              <Text style={[styles.ffb,styles.fs16,styles.colorblack,styles.p20,{textAlign:'center'}]}>{global.lang=='en'?'Select Language':'زبان منتخب کریں'}</Text>
            </View>
            <ScrollView contentContainerStyle={{padding:20,paddingBottom:100}}>
              <TouchableOpacity onPress={() => {this.changeLangEng()}}>
                <Text style={[styles.ffm,styles.fs14,styles.colorblack,styles.p10]}>English</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => {this.changeLangUrdu()}}>
                <Text style={[styles.ffm,styles.fs14,styles.colorblack,styles.p10]}>اردو</Text>
              </TouchableOpacity>
            </ScrollView>
          </View>
        </RBSheet>
            
        </SafeAreaView>
    );
  }
}
