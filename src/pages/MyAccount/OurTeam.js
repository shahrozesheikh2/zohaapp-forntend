import React, { Component } from 'react'
import { Text, View, SafeAreaView, TouchableOpacity,ScrollView, Dimensions,Image,Linking} from 'react-native'
import styles from '../../assets/css/styles';
import BackButtonComponent from '../../components/BackButtonComponent';
export default class OurTeam extends Component {
    onCall=(phone)=>{
        Linking.openURL(`tel:${phone}`)
      }
    onLinkedin=(url)=>{
        console.log(url);
        Linking.openURL(url)
    }
    render() {
        return (
            <SafeAreaView>
                <View style={{flexDirection:"row"}}>
                    <BackButtonComponent navigation={this.props.navigation}/>
                    <View  style={{flexDirection:"row" ,width:Dimensions.get('screen').width-100,justifyContent:'center'}}>
                        <Text style={[styles.menutxt,styles.mt20, styles.fs16]}>{global.lang=='en'?'Core Team':'ہماری ٹیم'}</Text>
                    </View>
                </View>
                <ScrollView contentContainerStyle={{paddingHorizontal:24,paddingTop:20}}>
                        <View style={{flexDirection:'row',marginBottom:20}}>
                            <Image source={require('../../assets/images/team/naveed.jpeg')} style={[{ width: 100, height: 100,borderRadius:50,borderColor:'#f9af16',borderWidth:5}]} resizeMode={'cover'}/>
                            <View style={{paddingLeft:15,alignSelf:'center'}}>
                                <Text style={[styles.menutxt, styles.fs18]}>Naveed Hussain</Text>
                                <Text style={[styles.menutxt, styles.fs14,styles.mb5]}>CEO of Zoha</Text>
                                <TouchableOpacity onPress={()=>{
                                    this.onCall('03102500426')
                                }}>
                                    <Text style={[styles.menutxtteam,styles.fs12,styles.mb5]}>0310 2500426</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{flexDirection:'row',marginBottom:20}}>
                            <Image source={require('../../assets/images/team/wajeeh.jpeg')} style={{ width: 100, height: 100,borderRadius:50,borderColor:'#f9af16',borderWidth:5}} resizeMode={'cover'}/>
                            <View style={{paddingLeft:15,alignSelf:'center'}}>
                                <Text style={[styles.menutxt, styles.fs18]}>Wajeeh Sikandar</Text>
                                <Text numberOfLines={1} style={[styles.menutxt, styles.fs14,styles.mb5]}>Bussiness Manager</Text>
                                <TouchableOpacity onPress={()=>{
                                    this.onCall('03359222467')
                                }}>
                                    <Text style={[styles.menutxtteam,styles.fs12,styles.mb5]}>0335 9222467</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={{flexDirection:'row',alignItems:'center'}} onPress={()=>{this.onLinkedin('https://www.linkedin.com/in/wajeeh-sikandar-94b0b71b0')}}>
                                    <Image source={require('../../assets/images/team/linkedin.png')} style={{ width: 20, height: 20}} resizeMode={'contain'}/>
                                    <Text style={[styles.menutxtteam,styles.fs12,{marginLeft:5}]}>linkedin</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{flexDirection:'row',marginBottom:20}}>
                            <Image source={require('../../assets/images/team/sharoze.jpg')} style={{ width: 100, height: 100,borderRadius:50,borderColor:'#f9af16',borderWidth:5}} resizeMode={'cover'}/>
                            <View style={{paddingLeft:15,alignSelf:'center'}}>
                                <Text style={[styles.menutxt, styles.fs18]}>Shahroze Sheikh</Text>
                                <Text style={[styles.menutxt, styles.fs14,styles.mb5]}>Software Engineer</Text>
                                <TouchableOpacity onPress={()=>{
                                    this.onCall('03331478581')
                                }}>
                                    <Text style={[styles.menutxtteam,styles.fs12,styles.mb5]}>0333 1478581</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={{flexDirection:'row',alignItems:'center'}} onPress={()=>{this.onLinkedin('https://www.linkedin.com/in/shahroze-sheikh-a34288180/')}}>
                                    <Image source={require('../../assets/images/team/linkedin.png')} style={{ width: 20, height: 20}} resizeMode={'contain'}/>
                                    <Text style={[styles.menutxtteam,styles.fs12,{marginLeft:5}]}>linkedin</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        
                        <View style={{flexDirection:'row',marginBottom:20}}>
                            <Image source={require('../../assets/images/team/hamza.jpg')} style={{ width: 100, height: 100,borderRadius:50,borderColor:'#f9af16',borderWidth:5}} resizeMode={'cover'}/>
                            <View style={{paddingLeft:15,alignSelf:'center'}}>
                                <Text style={[styles.menutxt, styles.fs18]}>Hamza Kayani</Text>
                                <Text style={[styles.menutxt, styles.fs14,styles.mb5]}>Mobile Application Developer</Text>
                                <TouchableOpacity onPress={()=>{
                                    this.onCall('03045959785')
                                }}>
                                    <Text style={[styles.menutxtteam,styles.fs12,styles.mb5]}>0304 5959785</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={{flexDirection:'row',alignItems:'center'}} onPress={()=>{this.onLinkedin('https://www.linkedin.com/in/usama-7-ali/')}}>
                                    <Image source={require('../../assets/images/team/linkedin.png')} style={{ width: 20, height: 20}} resizeMode={'contain'}/>
                                    <Text style={[styles.menutxtteam,styles.fs12,{marginLeft:5}]}>linkedin</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{flexDirection:'row',marginBottom:20}}>
                            <Image source={require('../../assets/images/team/usama.jpeg')} style={{ width: 100, height: 100,borderRadius:50,borderColor:'#f9af16',borderWidth:5}} resizeMode={'cover'}/>
                            <View style={{paddingLeft:15,alignSelf:'center'}}>
                                <Text style={[styles.menutxt, styles.fs18]}>Usama Ali</Text>
                                <Text style={[styles.menutxt, styles.fs14,styles.mb5]}>Backend Developer</Text>
                                <TouchableOpacity onPress={()=>{
                                    this.onCall('03037777081')
                                }}>
                                    <Text style={[styles.menutxtteam,styles.fs12,styles.mb5]}>0303 7777081</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={{flexDirection:'row',alignItems:'center'}} onPress={()=>{this.onLinkedin('https://www.linkedin.com/in/usama-7-ali/')}}>
                                    <Image source={require('../../assets/images/team/linkedin.png')} style={{ width: 20, height: 20}} resizeMode={'contain'}/>
                                    <Text style={[styles.menutxtteam,styles.fs12,{marginLeft:5}]}>linkedin</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        
                </ScrollView>
                
            </SafeAreaView>
        )
    }
}
