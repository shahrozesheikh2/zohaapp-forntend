import React, { Component } from "react";
import {
  Text,
  View,
  SafeAreaView,
  Image,
  TouchableOpacity,
  ScrollView,
  Dimensions,
  Linking
} from "react-native";
import styles from "../../assets/css/styles";
import BackButtonComponent from '../../components/BackButtonComponent'
export default class FaqsDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  onMail=()=>{
    Linking.openURL(`mailto:support@zoha.com`)
  }
  render() {
    return (
      <SafeAreaView style={{ flex: 1,backgroundColor:'#fff'}}>
        <View style={{flexDirection:"row",borderBottomColor:'#f2f2f2',borderBottomWidth:1,paddingBottom:20,paddingRight:10,justifyContent:'space-between'}}>
          <BackButtonComponent navigation={this.props.navigation}/>
          <View  style={{flexDirection:"row"}}>
            <Text style={[styles.menutxt,styles.mt20, styles.fs16]}>{global.lang=='en'?'FAQs':'عمومی سوالنامہ'}</Text>
          </View>
          <TouchableOpacity onPress={()=>{this.onMail()}}>
          <Text style={[styles.menutxt,styles.mt25,styles.fs14,styles.coloryellow,styles.ffb ]}>{global.lang=='en'?'Email Us':'مصنوعات کی وضاحت'}</Text>
          </TouchableOpacity>
          
        </View>
        <View style={{ padding:32 }}>
          <Text style={styles.faqsdetailtxt2}>Q: {this.props.route.params.title}</Text>
          <ScrollView >
            <View style={styles.faqsdetailview}>
              <Text style={styles.faqsdetailtxt3}>{this.props.route.params.desc}</Text>
            </View>
          </ScrollView>
        </View>
      </SafeAreaView>
    
    );
  }
}
