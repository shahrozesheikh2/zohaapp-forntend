import React, { Component } from "react";
import {
  Text,
  View,
  SafeAreaView,
  Image,
  TouchableOpacity,
  ScrollView,
  Share,
  Linking
} from "react-native";
import styles from "../../assets/css/styles";
import { CommonActions } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
export default class MyAccount extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  logout=async()=>{
    global.usertoken = null;
    global.userrole = null;
    await AsyncStorage.clear();
    this.props.navigation.dispatch(
      CommonActions.reset({
        index: 1,
        routes: [
          { name: 'RoleSelection' }
        ],
      })
    );
  }
  deactivate=async()=>{
    var header={
      "Authorization":global.usertoken
    }
    axios.get( global.url + '/api/user/deactivate',{headers:header} ).then(async (response) => {
      console.log(response.data);
      global.usertoken = null;
      global.userrole = null;
      await AsyncStorage.clear();
      this.props.navigation.dispatch(
        CommonActions.reset({
          index: 1,
          routes: [
            { name: 'RoleSelection' }
          ],
        })
      );
    }).catch(error => {
      console.log(error.response);
    });
  }
  onShare = async () => {
    try {
      const result = await Share.share({
        message: `Please install Zoha App,\n\nZoha App Android Link :https://play.google.com/store/apps/details?id=com.zoha \n\nZoha App IOS Link :https://apps.apple.com/pk/app/zoha/id1565424423`,
        // Zoha App IOS Link :https://play.google.com/store/apps/details?id=com.zoha
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };
  onCall=()=>{
    Linking.openURL(`tel:03100040003`)
  }
  render() {
    return (
      <SafeAreaView style={{ flex: 1,backgroundColor:'#fff'}}>
        <View style={{ padding:24,paddingTop:0}}>
          <Text style={[styles.title,styles.mb40]}>{global.lang=='en'?'My Account':'میرا اکاونٹ'}</Text>
          <ScrollView >
            <TouchableOpacity onPress={()=>{this.props.navigation.navigate('GeneralSettings')}}>
              <View style={styles.menuview}>
                <View>
                  <Text style={styles.menutxt}>{global.lang=='en'?'General App Settings':'عمومی ایپ کی ترتیبات'}</Text>

                  <Text style={styles.menutxt2}>{global.lang=='en'?'Change your account details':'اپنے اکاؤنٹ کی تفصیلات تبدیل کریں'}</Text>

                </View>
                <Image source={require('../../assets/images/menuarrow.png')} style={{ width: 7, height: 12 }} resizeMode={'contain'}/>
              </View>
            </TouchableOpacity>
            
            <TouchableOpacity onPress={()=>{this.onCall()}}>
              <View style={styles.menuview}>
                <Text style={styles.menutxt}>{global.lang=='en'?'Suggestions':'مشورہ'}</Text>
                <Image source={require('../../assets/images/menuarrow.png')} style={{ width: 7, height: 12 }} resizeMode={'contain'}/>
              </View>
            </TouchableOpacity>
              <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Faqs')}}>
                <View style={styles.menuview}>
                  <Text style={styles.menutxt}>{global.lang=='en'?'Client Support / FAQs':'مؤکل کی حمایت / عمومی سوالنامہ'}</Text>
                  <Image source={require('../../assets/images/menuarrow.png')} style={{ width: 7, height: 12 }} resizeMode={'contain'}/>
                </View>
              </TouchableOpacity>
              

            {(global.userrole=='buyer') && (
              <>
              <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Bookmarks')}}>
              <View style={styles.menuview}>
                <Text style={styles.menutxt}>{global.lang=='en'?'Product Bookmarks':'بُک مارکس'}</Text>
                <Image source={require('../../assets/images/menuarrow.png')} style={{ width: 7, height: 12 }} resizeMode={'contain'}/>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>{this.props.navigation.navigate('ServiceBookmarks')}}>
              <View style={styles.menuview}>
                <Text style={styles.menutxt}>{global.lang=='en'?'Service Bookmarks':'بُک مارکس'}</Text>
                <Image source={require('../../assets/images/menuarrow.png')} style={{ width: 7, height: 12 }} resizeMode={'contain'}/>
              </View>
            </TouchableOpacity>
              <TouchableOpacity onPress={()=>{this.onCall()}}>
                <View style={styles.menuview}>
                  <Text style={styles.menutxt}>{global.lang=='en'?'Become Seller':'بیچنے والے'}</Text>
                  <Image source={require('../../assets/images/menuarrow.png')} style={{ width: 7, height: 12 }} resizeMode={'contain'}/>
                </View>
              </TouchableOpacity>
              </>
            )}
            
            <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Terms')}}>
              <View style={styles.menuview}>
                <Text style={styles.menutxt}>{global.lang=='en'?'Zoha Terms and Conditions':'زوہا شرائط و ضوابط'}</Text>
                {/* <Text style={styles.menutxt}>زوہا شرائط و ضوابط</Text> */}

                <Image source={require('../../assets/images/menuarrow.png')} style={{ width: 7, height: 12 }} resizeMode={'contain'}/>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>{this.onShare()}}>
              <View style={styles.menuview}>
                <Text style={styles.menutxt}>{global.lang=='en'?'Recomend to a Friend':'کسی دوست کو مشورہ دیں'}</Text>
                <Image source={require('../../assets/images/menuarrow.png')} style={{ width: 7, height: 12 }} resizeMode={'contain'}/>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>{this.props.navigation.navigate('OurTeam')}}>
              <View style={styles.menuview}>
                <Text style={styles.menutxt}>{global.lang=='en'?'Core Team':'ہماری ٹیم'}</Text>
                <Image source={require('../../assets/images/menuarrow.png')} style={{ width: 7, height: 12 }} resizeMode={'contain'}/>
              </View>
            </TouchableOpacity>
            {/* <TouchableOpacity onPress={()=>{this.deactivate()}}>
              <View style={[styles.menuview,styles.mt30]}>
                <Text style={[styles.menutxt,styles.colorred]}>Deactivate</Text>
                
                <Image source={require('../../assets/images/menuarrow.png')} style={{ width: 7, height: 12 }} resizeMode={'contain'}/>
              </View>
            </TouchableOpacity> */}
            <TouchableOpacity onPress={()=>{this.logout()}}>
              <View style={[styles.menuview]}>
                <Text style={[styles.menutxt,styles.colorred]}>{global.lang=='en'?'Logout':'لاگ آوٹ'}</Text>
                <Image source={require('../../assets/images/menuarrow.png')} style={{ width: 7, height: 12 }} resizeMode={'contain'}/>
              </View>
            </TouchableOpacity>
          </ScrollView>
      
        </View>
        
        </SafeAreaView>
    );
  }
}
