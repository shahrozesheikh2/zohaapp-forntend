import React, { Component } from "react";
import {
  Text,
  View,
  SafeAreaView,
  Image,
  TouchableOpacity,
  ScrollView,
  Dimensions,
  Linking
} from "react-native";
import styles from "../../assets/css/styles";
import BackButtonComponent from '../../components/BackButtonComponent'
export default class Faqs extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  onMail=()=>{
    Linking.openURL(`mailto:support@zoha.com`)
  }
  render() {
    return (
      <SafeAreaView style={{ flex: 1,paddingRight:10}}>
        <View style={{flexDirection:"row",justifyContent:'space-between'}}>
          <BackButtonComponent navigation={this.props.navigation}/>
          <View  style={{flexDirection:"row"}}>
            <Text style={[styles.menutxt,styles.mt20, styles.fs16]}>{global.lang=='en'?'FAQs':'عمومی سوالنامہ'}</Text>
          </View>
          <TouchableOpacity onPress={()=>{this.onMail()}}>
          <Text style={[styles.menutxt,styles.mt25,styles.fs14,styles.coloryellow,styles.ffb ]}>{global.lang=='en'?'Email Us':'ہمیں ای میل کریں'}</Text>
          </TouchableOpacity>
          
        </View>
        <View style={{ padding:24 }}>
          <Text style={styles.faqstxt2}>{global.lang=='en'?'May be Zoha already answered your queries, explore a little bit':'ہوسکتا ہے کہ زوہا پہلے ہی آپ کے سوالات کا جواب دے سکتی ہے ، تھوڑا سا دریافت کریں'} </Text>
          <ScrollView >
            <TouchableOpacity onPress={()=>{this.props.navigation.navigate('FaqsDetail',{
              title:'What Zoha app providing you?',
              desc:'It provide you to easy way to find or but new and old auto parts on different shops on Multi places. It also interlink for buyer or seller via chat in multi feature.  '
              
            })}}>
              <View style={styles.faqsview}>
                  <Text style={styles.faqstxt}>1.	What Zoha app providing you?</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>{this.props.navigation.navigate('FaqsDetail',{
              title:'What is Refurbished Auto Part?',
              desc:'A refurbished auto part is a used auto part that has been professionally Re-manufactured back to new working condition according to industry standards. Re manufactured auto parts generally come with warranties.'
              
            })}} >
              <View style={styles.faqsview}>
                  <Text style={styles.faqstxt}>2.	What is Refurbished Auto Part?</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>{this.props.navigation.navigate('FaqsDetail',{
              title:'Where do I Buy New Auto Parts?',
              desc:'You can visit any commercial auto part store and purchase virtually any part you need, however, be prepared to pay maximum price. If you wish to find reasonably-priced then You Crown Autos (Rawalpindi). '
              
            })}} >
              <View style={styles.faqsview}>
                  <Text style={styles.faqstxt}>3.	Where do I Buy New Auto Parts?</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>{this.props.navigation.navigate('FaqsDetail',{
              title:'Where do I Buy Used Auto Parts?',
              desc:'You can purchased used auto parts from several public and private vendors, but the most reliable and budge-friendly place to purchase used auto parts at is an independently owned Crown Autos (Rawalpindi).  Not only will they have reasonable prices, a good, long-standing company may also offer warranties with their used auto parts.'
              
            })}} >
              <View style={styles.faqsview}>
                  <Text style={styles.faqstxt}>4.	Where do I Buy Used Auto Parts?</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>{this.props.navigation.navigate('FaqsDetail',{
              title:'What is the Difference between Re-manufactured, Refurbished, and Rebuilt Auto Parts?',
              desc:'“Re-manufactured” and “refurbished” are terms used synonymously with one another in the automotive industry, so they mean the same thing. Rebuilt parts, on the other hand, are non-vital parts or components taken from other vehicles for the purposes of rebuilding and repairing a defective part in your vehicle. Examples include nuts, bolts, bearings, piston rings, and rods.'
              
            })}} >
              <View style={styles.faqsview}>
                  <Text style={styles.faqstxt}>5.	What is the Difference between Re-manufactured, Refurbished, and Rebuilt Auto Parts?</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>{this.props.navigation.navigate('FaqsDetail',{
              title:'Is Buying Used Auto Parts a Smart Way to Save Money?',
              desc:'Yes! Especially if you require a very expensive auto part, such as an engine or catalytic converter, choosing to purchase used parts is a wise choice that can save you hundreds, or even thousands, of dollars.'
              })}} >
              <View style={styles.faqsview}>
                  <Text style={styles.faqstxt}>6.	Is Buying Used Auto Parts a Smart Way to Save Money?</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>{this.props.navigation.navigate('FaqsDetail',{
              title:'What Advice Do I Need When Shopping For Used Auto Parts?',
              desc:'It is in the buyer’s best interest to inquire about a used auto part’s warranty, price, and return policy. Being informed of these three categories can help you make an educated purchasing decision. So long as you buy from a reputable retailer, you should be in good hands.'
              })}} >
              <View style={styles.faqsview}>
                  <Text style={styles.faqstxt}>7.	What Advice Do I Need When Shopping For Used Auto Parts?</Text>
              </View>
            </TouchableOpacity>

          </ScrollView>
        </View>
      </SafeAreaView>
    
    );
  }
}
