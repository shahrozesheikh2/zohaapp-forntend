import React from 'react'
import { SafeAreaView, TextInput, Alert,TouchableOpacity, Text, Image, View, ScrollView,Dimensions,Platform } from 'react-native'
import styles from '../../assets/css/styles';
import BackButtonComponent from '../../components/BackButtonComponent';
import axios from 'axios';
import { CommonActions } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage'; 
export default class ChangePassword extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            phoneInput:this.props.route.params.phone,
        };
    }
    editPhone=()=>{
    var header={
        "Content-Type":"application/json",
        "Authorization":global.usertoken
    }
    var body={
        "phoneNumber": this.state.phoneInput
    }
    console.log(body);
    if(this.state.phoneInput==''){
        alert('Enter Phone Number')
    }
    else{
        axios.post( global.url + '/api/user/change-phone',body,{headers:header} ).then(response => {
            console.log(response.data);
            alert('Phone number changed successfully nw you need login again.');
            this.logout();
        }).catch(error => {
            console.log(error.response);
        });
    }
    }
    logout=async()=>{
        global.usertoken = null;
        global.userrole = null;
        await AsyncStorage.clear();
        this.props.navigation.dispatch(
            CommonActions.reset({
            index: 1,
            routes: [
                { name: 'RoleSelection' }
            ],
            })
        );
    }
    render() {
    return (
        <SafeAreaView style={{ flex: 1 }}>

        <View style={{flexDirection:"row",borderBottomColor:'#d9d9d9',borderBottomWidth:1,paddingBottom:15}}>
            <BackButtonComponent navigation={this.props.navigation}/> 
            <View style={{position: 'absolute',right: 10,}}>
                <TouchableOpacity onPress={()=>{this.editPhone()}}>
                    <Text style={[styles.menutxt,styles.mt25,styles.fs14,styles.coloryellow,styles.ffb ]}>{global.lang=='en'?'Save Changes':'تبدیلیاں محفوظ کرو'}</Text>
                </TouchableOpacity>
            </View>
        </View>
        
        <ScrollView contentContainerStyle={{padding: 20,}}>
            <View  style={{flexDirection:"row" ,width:Dimensions.get('screen').width-80,justifyContent:'center'}}>
                <Text style={[styles.menutxt,styles.mt10,styles.mb10, styles.fs14]}>{global.lang=='en'?'Edit Phone Number':'فون نمبر میں ترمیم کریں'}</Text>
            </View>
        <Text style={[styles.menutxt,styles.fs10,styles.ffr,{padding:8}]}>{global.lang=='en'?'Phone Number':'فون نمبر'}</Text>
            <View style={{ borderWidth: 0.5, borderColor:'#ddd', 
                borderRadius: 10, width: Dimensions.get('screen').width-40, height: 56, 
                flexDirection: "row"}}>
                <TextInput style={{ width: Dimensions.get('screen').width-112,height:56,
                    borderTopRightRadius: 10, borderBottomRightRadius: 10, paddingHorizontal: 10, fontSize: 13,color:'black'
                    }}
                    onChangeText={(val)=>this.setState({phoneInput:val})}
                    value={this.state.phoneInput}
                    placeholder={'Ex: 03001234567'}
                    placeholderTextColor={'#7c7c7c'}
                    keyboardType={"number-pad"}
                />
            </View>
        </ScrollView>  
        </SafeAreaView>
    );
    }
}
