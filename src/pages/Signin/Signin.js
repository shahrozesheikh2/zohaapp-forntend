import React , { Component } from 'react'
import { Text, Image, TextInput, View, Linking,TouchableOpacity,ScrollView,Dimensions,Alert,Platform,KeyboardAvoidingView,SafeAreaView,Animated } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import styles from '../../assets/css/styles';
import Phone from '../../assets/images/phone.png';
import { Overlay } from 'react-native-elements';
import RBSheet from "react-native-raw-bottom-sheet";
import axios from 'axios';
import Spinner from 'react-native-loading-spinner-overlay';
export default class Signin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phoneInput:'',
      showOverlay:false,
      bounceValue: new Animated.Value(100),
      buttonText: "Show Subview",
      citiesdata:[],
      city:'',
      cityid:'',
      name:'',
      countrydata:[],
      country:'',
      countryid:'',
      countrycode:'',
      profileimage:'',
      awskey:'62148438-0408-4f07-b375-7ec345fd1c2a',
      awsurl:'https://zoha-remote.s3.ap-southeast-1.amazonaws.com/62148438-0408-4f07-b375-7ec345fd1c2a',
      network:'Zong',
      spinner: false
    };
    console.log(this.props.route.params.role);
    this.getCountry();
  }
  toggleOverlay = () => {
    this.setState({showOverlay:!this.state.showOverlay})
  }
  verifyPhone=()=>{
    var regex=/^[+]?[0-9]{10}$/
    if(this.state.name==''){
      alert('Enter Name')
    }
    else if(this.state.phoneInput==''){
      alert('Enter Phone Number')
    }
    else if(regex.test(this.state.phoneInput)==false){
      alert('Phone Number Format ex: 3041234567 (Without Country Code)');
    }
    else if (this.state.country==''){
      alert('Select Country')
    }
    else if (this.state.city==''){
      alert('Select City')
    }
    else{
      this.sendOTP();
    }
  }
  sendOTP=()=>{
    this.setState({spinner: true});
    var header={
      'Content-Type':'application/json'
    }
    var body={
      "fullName":this.state.name,
      "phoneNumber":this.state.countrycode+this.state.phoneInput,
      "countryId":this.state.countryid,
      "cityId":this.state.cityid,
      "roleId":this.props.route.params.role=='buyer'?'2':'1',
      "awsKey":this.state.awskey,
      "awsUrl":this.state.awsurl,
      "network":this.state.network,
      "fbToken":global.fbtoken
    }
    console.log(body);
    axios.post( global.url + '/api/user/signup',body,{headers:header} ).then(response => {
      console.log(response.data);
      this.setState({spinner: false});
      this.props.navigation.replace('OtpVerification',{phone:this.state.countrycode+this.state.phoneInput,role:this.props.route.params.role});
    }).catch(error => {
      this.setState({spinner: false});
      if(error.response.data.message=='Cannot change role'){
        alert(`You can't change role to ${this.props.route.params.role}`);
      }
      else{
        alert(error.response.data.message);
      }
      console.log(error.response);
    });
  }
  onCall=()=>{
    Linking.openURL(`tel:03100040003`)
  }
  getCountry=()=>{
    axios.get( global.url + '/api/country?offset=0&limit=100').then(response => {
      console.log(response.data.result.data);
      this.setState({countrydata:response?.data?.result?.data})
    }).catch(error => {
      console.log(error.response);
    });
  }
  getCity=(id)=>{
    axios.get( global.url + '/api/city?offset=0&limit=999&id='+id).then(response => {
      console.log(response.data);
      this.setState({citiesdata:response?.data?.result?.data})
    }).catch(error => {
      console.log(error.response);
    });
  }
  render() {
    return (
      <KeyboardAvoidingView style={{flex:1}} enabled={Platform.OS == "ios"} behavior={Platform.OS == "ios" ? "padding" : null} 
        keyboardVerticalOffset={40}>
        <SafeAreaView style={{ flex: 1 }}>
          <Spinner
            visible={this.state.spinner}
            textContent={'Verifing...'}
            textStyle={styles.spinnerTextStyle}
          />
          <View style={{flexDirection:"row",marginRight:20}}>
            <TouchableOpacity style={{ marginTop: 25 , marginLeft:20,flexDirection:"row" }}  onPress={()=>{this.onCall()}}>
                <Image source={Phone} style={{ width: 24, height: 24,marginRight:15 }} resizeMode={'contain'}/>
                <Text style={styles.titlecare}>{global.lang=='en'?'Customer Care':'صارفین کی سہولت'}</Text>
            </TouchableOpacity>
            
          </View>
           
            <ScrollView style={{marginLeft:20,marginRight:20}}>
                <Text style={styles.title}>{global.lang=='en'?'Enter Phone Number':'اپنا نمبر درج کریں'}</Text>
                <Text style={styles.description}>{global.lang=='en'?"You'll get a verification code":'آپ کو تصدیقی کوڈ حاصل ہوگا'}</Text>
                <View style={{ borderWidth: 1, borderColor: '#ddd', 
                    borderRadius: 10, width: Dimensions.get('screen').width-40, height: 56, marginTop: 20, 
                    flexDirection: "row", alignItems: "center"}}>
                    <TextInput style={{ width:'100%', backgroundColor: '#fff',
                        paddingHorizontal: 10, fontSize: 14,color:'black'
                        }}
                        onChangeText={(val)=>this.setState({name:val})}
                        placeholder={global.lang=='en'?'Name':'نام'}
                        placeholderTextColor={'#7c7c7c'}
                    />
                </View>
                <View style={{ borderWidth: 0.5, borderColor: this.state.showPhoneError?'#ff4148':'#ddd', 
                    borderRadius: 10, width: Dimensions.get('screen').width-40, height: 56, marginTop: 20, 
                    flexDirection: "row"}}>

                    <TextInput style={{ width: Dimensions.get('screen').width-112,height:56,
                        borderTopRightRadius: 10, borderBottomRightRadius: 10, paddingHorizontal: 10, fontSize: 13,color:'black'
                        }}
                        onChangeText={(val)=>this.setState({phoneInput:val})}
                        placeholder={'Phone Number : (Without Country Code)'}
                        placeholderTextColor={'#7c7c7c'}
                        keyboardType={"number-pad"}
                    />
                </View>
                    <Text style={{ color: 'rgb(245,102,91)', fontSize: 12, marginTop: 5 }}>
                    {global.lang=='en'?'* Format (Without Country Code) : 3041234567':'* Format (Without Country Code) : 3041234567'}
                    </Text>

                <TouchableOpacity onPress={() => this.RBCountrySheet.open()}>
                  <View style={{borderColor:'#cccccc',borderWidth:1,padding:20,marginTop:20,borderRadius:8,flexDirection:'row',justifyContent:'space-between'}}>
                    <Text style={{ color: '#1a1a1a', fontSize: 13, }}>{this.state.country!=''?this.state.country:'Select your Country'}</Text>
                    <Image source={require('../../assets/images/dropdownicon.png')} style={[styles.homeimg,{ width: 10, height: 6 ,marginTop:6 }]} resizeMode={'contain'}/>
                  </View>
                </TouchableOpacity>
                
                <TouchableOpacity onPress={() => this.RBSheet.open()}>
                  <View style={{borderColor:'#cccccc',borderWidth:1,padding:20,marginTop:20,borderRadius:8,flexDirection:'row',justifyContent:'space-between'}}>
                  {global.lang=='en'?<Text style={{ color: '#1a1a1a', fontSize: 13, }}>{this.state.city!=''?this.state.city:'Select your city'}</Text>
                    :<Text style={{ color: '#1a1a1a', fontSize: 13, }}>{this.state.city!=''?this.state.city:'اپنے شہر کا انتخاب کریں'}</Text> }
                      <Image source={require('../../assets/images/dropdownicon.png')} style={[styles.homeimg,{ width: 10, height: 6 ,marginTop:6 }]} resizeMode={'contain'}/>
                  </View>
                </TouchableOpacity>

            </ScrollView>
            <View style={{ flexDirection: "row", alignItems: "center", justifyContent:"space-between",paddingRight:15,paddingBottom:20,marginLeft:20 }}>
            {global.lang=='en'?<Text style={{ color: '#666666', fontSize: 14, width:wp('60%'), fontWeight:'600' }}>
                By signing above you agree to the 
                    <Text style={{ color: '#f9af16', fontWeight: "bold" }}>
                        {' '}Terms and conditions{' '}
                    </Text>
                    of Zoha 2021
                </Text>
                :<Text style={{ color: '#666666', fontSize: 14, width:wp('60%'), fontWeight:'600' }}>
                     اوپر دستخط کرکے آپ  زوہا 2021 کے   
                    <Text style={{ color: '#f9af16', fontWeight: "bold" }}>
                        {' '}شرائط و ضوابط{' '}
                    </Text>
                    سے اتفاق کرتے ہیں
                </Text>}
                <TouchableOpacity style={{backgroundColor:'#f9af16', borderRadius:50, width:56, height:56, 
                    justifyContent:"center", alignItems:"center" }} onPress={() => {this.verifyPhone()}}>
                    <Image source={require('../../assets/images/arrowright.png')} style={{ width:16, height:15 }} resizeMode={'contain'}/>
                </TouchableOpacity>
            </View>
            
            <RBSheet ref={ref => { this.RBCountrySheet = ref; }} height={400} openDuration={250}  >
              <View>
                <View style={{ borderBottomColor:'#d9d9d9',borderBottomWidth:1,position: 'relative', }}>
                  <Text style={[styles.ffb,styles.fs16,styles.colorblack,styles.p20,{textAlign:'center'}]}>{global.lang=='en'?'Select Your Country':'Select Your Country'}</Text>
                  <View style={{position: 'absolute',right:0}}>
                    <TouchableOpacity onPress={() => this.RBCountrySheet.close()}>
                      <Text style={[styles.ffb,styles.fs16,styles.coloryellow,styles.p20,{textAlign:'center'}]}>{global.lang=='en'?'Done':'ہو گیا'}</Text>
                    </TouchableOpacity>
                  </View>
                </View>
                <ScrollView contentContainerStyle={{padding:20,paddingBottom:100}}>
                  {this.state.countrydata.map((item, index) => {
                    return (
                      <TouchableOpacity key={index} onPress={() => {this.setState({country:item.countryName,countryid:item.id,countrycode:item.phoneCode});this.getCity(item.id);this.RBCountrySheet.close();}}>
                        <Text style={[styles.ffm,styles.fs14,styles.colorblack,styles.p10]}>{item.countryName}</Text>
                      </TouchableOpacity>
                    )
                  })}
                </ScrollView>
              </View>
            </RBSheet>

            <RBSheet ref={ref => { this.RBSheet = ref; }} height={400} openDuration={250}  >
              <View>
                <View style={{ borderBottomColor:'#d9d9d9',borderBottomWidth:1,position: 'relative', }}>
                  <Text style={[styles.ffb,styles.fs16,styles.colorblack,styles.p20,{textAlign:'center'}]}>{global.lang=='en'?'Select Your City':'پنے شہر کا انتخاب کریں'}</Text>
                  <View style={{position: 'absolute',right:0}}>
                    <TouchableOpacity onPress={() => this.RBSheet.close()}>
                      <Text style={[styles.ffb,styles.fs16,styles.coloryellow,styles.p20,{textAlign:'center'}]}>{global.lang=='en'?'Done':'ہو گیا'}</Text>
                    </TouchableOpacity>
                  </View>
                </View>
                <ScrollView contentContainerStyle={{padding:20,paddingBottom:100}}>
                  {this.state.citiesdata.map((item, index) => {
                    return (
                      <TouchableOpacity key={index} onPress={() => {this.setState({city:item.cityName,cityid:item.id});this.RBSheet.close();}}>
                        <Text style={[styles.ffm,styles.fs14,styles.colorblack,styles.p10]}>{item.cityName}</Text>
                      </TouchableOpacity>
                    )
                  })}
                </ScrollView>
              </View>
            </RBSheet>
            
            <Overlay overlayStyle={{padding:0,borderRadius:8,}} isVisible={this.state.showOverlay} onBackdropPress={this.toggleOverlay}>
              <View style={{width:300,overflow: 'hidden',}}>
                <View style={{padding:30}}>
                  <Text style={[styles.ffb,styles.fs18,styles.colorblack,{textAlign:'center',}]}>{global.lang=='en'?'Verifying the Number':'نمبر کی تصدیق کی جا رہی ہے'}</Text>
                  <Text style={[styles.ffb,styles.fs18,styles.colorblack,styles.mt5,{textAlign:'center',}]}>+92 {this.state.phoneInput}</Text>
                  <Text style={[styles.ffr,styles.fs14,styles.colorblack,styles.mt20,{textAlign:'center',}]}>{global.lang=='en'?'Is it ok, or would you like to edit the given number':'کیا یہ ٹھیک ہے یا آپ نمبر تبدیل کرنا چاہیں گے؟'}</Text>
                </View>
                <View style={{flexDirection:'row'}}>
                  <View style={{width:'50%',borderTopWidth:1,borderTopColor:'#d9d9d9',borderRightWidth:1,
                      borderRightColor:'#d9d9d9',padding:17}}>
                    <TouchableOpacity  onPress={() => {this.setState({showOverlay:false})}}>
                        <Text style={[styles.ffb,styles.fs18,styles.colorblack,{textAlign:'center',}]}>{global.lang=='en'?'Edit':'ترمیم'}</Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{width:'50%',borderTopWidth:1,borderTopColor:'#d9d9d9',padding:17}}>
                    <TouchableOpacity onPress={() => {this.sendOTP()}}>
                        <Text style={[styles.ffb,styles.fs18,styles.coloryellow,{textAlign:'center',}]}>{global.lang=='en'?'Verify':'تصدیق کریں'}</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </Overlay>
        </SafeAreaView>
      </KeyboardAvoidingView>
    );
  }
}
