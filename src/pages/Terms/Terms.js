import React, { Component } from "react";
import {
  Text,
  View,
  SafeAreaView,
  Image,
  TouchableOpacity,
  ScrollView,
  Dimensions,
  Linking
} from "react-native";
import styles from "../../assets/css/styles";
import BackButtonComponent from '../../components/BackButtonComponent'
export default class Terms extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
        <SafeAreaView style={{ flex: 1,backgroundColor:'#fff'}}>
        <View style={{flexDirection:"row",borderBottomColor:'#f2f2f2',borderBottomWidth:1,paddingBottom:20}}>
          <BackButtonComponent navigation={this.props.navigation}/>
          <View  style={{flexDirection:"row" ,width:Dimensions.get('screen').width-100,justifyContent:'center'}}>
            <Text style={[styles.menutxt,styles.mt20, styles.fs16]}>Terms and Conditions </Text>
            
          </View>
        </View>
        {/* <View style={{ padding:32 }}> */}
          {/* <Text style={styles.faqsdetailtxt2}>Zoha Terms and Conditions?</Text> */}
          <ScrollView style={{padding:20}}>
            {/* <View style={styles.faqsdetailview}> */}
              <Text style={styles.faqsdetailtxt3}>
              You must not use this platform in ways that can bring <Text style={{fontFamily:'UberMoveText-Bold'}}>harm to ZOHA or anyone accessing the Site 
              (i.e. other users) </Text> in form of interruption, damaging or impairing of any kind
              You must stay clear of activities that can bring harm to the APP, its staff
              and all other stake holders and parties directly or indirectly linked with the ZOHA APP.
              {'\n'}{'\n'} You know that only you are in question for all electronic communications
              and data sent from your device to us and you have to use the APP for lawful purposes only. 
                {'\n'}{'\n'}
              <Text style={{fontFamily:'UberMoveText-Bold'}}>You are to strictly forbidden to:</Text>
                {'\n'}{'\n'}
              1. Fake Shop Name, If your Shop aur bussines name is fake then it can be deactivated from aur admin.
                {'\n'}{'\n'}
              2. Use ZOHA for Fraudulent Purposes or any other criminal activities.
                {'\n'}{'\n'}
              3. Upload, send, or use/re-use material that is not yours or can hurt or cause harm to anyone else 
                or which promotes sexually explicit content, blasphemy, or any other defamatory or abusive material.
                {'\n'}{'\n'}
              4. That harasses, degrades, intimidates or is hateful towards any individual or group of individuals
              on the basis of religion, gender, sexual orientation, race, ethnicity, age, or disability.
                {'\n'}{'\n'} 
              5. Registration with valid documents is leading to ONLINE SHOP on ZOHA.
                {'\n'}{'\n'}
              6. Any Illegal activity leads to permanent ban.
                {'\n'}{'\n'}
              7. ZOHA suggests safe money transfer using digital trading, otherwise Company or staff
                will not be responsible.
                {'\n'}{'\n'}
              8. Digital marketing is available at good Cost for any type of businessmen.
                {'\n'}{'\n'}
              9. That infringes any of the foregoing intellectual property rights of any party,
                  or is Content that you do not have a right to make available under any law,regulation,
                  contractual or fiduciary relationship(s).
                  {'\n'}{'\n'}
              10. That advertises any illegal services or the sale of any items the sale of which is prohibited
                or restricted by applicable law, including without limitation items the sale of which is prohibited 
                or regulated by applicable law of local jurisdiction.
                {'\n'}{'\n'}
              </Text>
            {/* </View> */}
          </ScrollView>
        {/* </View> */}
      </SafeAreaView>
    );
  }
}
