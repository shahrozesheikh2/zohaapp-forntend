import React from 'react'
import { SafeAreaView, TextInput, Dimensions,TouchableOpacity, Text, Image, View, ScrollView } from 'react-native'
import styles from '../../assets/css/styles';
import RBSheet from "react-native-raw-bottom-sheet";
import BackButtonComponent from '../../components/BackButtonComponent';
import axios from 'axios';
import ImageZoom from 'react-native-image-pan-zoom';
import { SliderBox } from "react-native-image-slider-box";
import FastImage from 'react-native-fast-image';
import { Modal } from 'react-native';
export default class ProductDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // productdata:this.props.route.params.data,
      rating:1,
      comment:'',
      shopdata:'',
      productlength:0,
      bookmarkcheck:0,
      showOverlay:false,
      ZoomedImages:[],
      swipeImage:[],
      selectedImage:''
    };
    
  }
  componentDidMount(){
    this.checkBookmark();
    // this.addimages();
    this.addswpieimages()
  }
  // addimages=()=>{
  //   var temp=[];
  //   this.props.route.params.data.resources.map((item)=>{
  //     temp.push({ 
  //       source:{
  //         url:item.awsUrl
  //       },
  //       width: 806,
  //       height: 720 })
  //   })
  //   this.setState({ZoomedImages:temp});
  //   console.log(temp);
  // }
  addswpieimages=()=>{
    var temp1=[];
    this.props.route.params.data.resources.map((item)=>{
      temp1.push(item.awsUrl)
    })
    this.setState({swipeImage:temp1});
  }
  toggleOverlay = () => {
    this.setState({showOverlay:!this.state.showOverlay})
  }
  addBookmark=(id)=>{
    var header={
      "Content-Type":"application/json",
      "Authorization":global.usertoken
    }
    var body ={
      "id": id
    }
    axios.post( global.url + '/api/bookmark/',body,{headers:header}).then(response => {
      console.log(response.data.message);
      this.checkBookmark();
    }).catch(error => {
      alert(error.response.data.message);
    });
  }
  deleteBookmark=(id)=>{
    var header={
      "Content-Type":"application/json",
      "Authorization":global.usertoken
    }
    axios.delete( global.url + '/api/bookmark/'+id,{headers:header}).then(response => {
      console.log(response.data);
      this.checkBookmark();
    }).catch(error => {
      alert(error.response.data.message);
    });
  }
  addReview=(id)=>{
    var header={
      "Content-Type":"application/json",
      "Authorization":global.usertoken
    }
    var body ={
      "comment": this.state.comment,
      "rating":this.state.rating,
      "productId": this.props.route.params.data.id
    }
    if(this.state.comment==''){
      alert('Please add comment');
    }
    else{
      this.RBSheet2.close();
      axios.post( global.url + '/api/review/',body,{headers:header}).then(response => {
        console.log(response.data.message);
        this.props.navigation.goBack();
        alert(response.data.message);
      }).catch(error => {
        alert(error.response.data.message);
      });
    }
  }
  // getProduct=()=>{
  //   var header={
  //     "Content-Type":"application/json",
  //     "Authorization":global.usertoken
  //   }
  //   axios.post( global.url + '/api/product/',body,{headers:header}).then(response => {
  //     console.log(response.data.message);
  //     this.props.navigation.goBack();
  //     alert(response.data.message);
  //   }).catch(error => {
  //     alert(error.response.data.message);
  //   });
  // }
  checkBookmark=()=>{
      var header={
      "Content-Type":"application/json",
      "Authorization":global.usertoken
    }
    axios.get( global.url + '/api/product/check-bookmark/'+this.props.route.params.data.id,{headers:header}).then(response => {
      console.log(response.data);
      this.setState({bookmarkcheck:response.data.result.data.count})

    }).catch(error => {
      alert(error.response.data.message);
    });
  }
  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={{flexDirection:"row",justifyContent:'space-between',paddingRight:20,borderBottomWidth: 1, borderBottomColor: 'rgb(230,230,230)',paddingBottom:10}}>
          <BackButtonComponent navigation={this.props.navigation}/>
          <View style={{flexDirection:'row'}}>
            <TouchableOpacity style={{ marginTop: 20 , marginLeft:0 }}  onPress={() => this.RBSheet1.open()}>
                <Image source={require('../../assets/images/review.png')} style={{ width: 24, height: 24 }} resizeMode={'contain'}/>
            </TouchableOpacity>
            {(this.state.bookmarkcheck==0) && (
              <TouchableOpacity style={{ marginTop: 20 , marginLeft:10 }} onPress={()=>{this.addBookmark(this.props.route.params.data.id)}}>
                <Image source={require('../../assets/images/fav.png')} style={{ width: 24, height: 24 }} resizeMode={'contain'}/>
              </TouchableOpacity>
            )}
            {(this.state.bookmarkcheck!=0) && (
              <TouchableOpacity style={{ marginTop: 20 , marginLeft:10 }} onPress={()=>{this.deleteBookmark(this.props.route.params.data.id)}}>
                <Image source={require('../../assets/images/favb.png')} style={{ width: 24, height: 24 }} resizeMode={'contain'}/>
              </TouchableOpacity>
            )}
            
          </View>
        </View>
        <View style={{marginBottom:10,backgroundColor:'#f9f9f9'}}>
          <SliderBox ImageComponentStyle={{backgroundColor:'#f9f9f9'}} ImageComponent={FastImage} images={this.state.swipeImage} 
            onCurrentImagePressed={(index) => this.setState({showOverlay:true,selectedImage:this.props.route.params.data.resources[index].awsUrl}) }
            resizeMethod={'resize'} resizeMode={'contain'}/>
        </View>

        <View style={{flexDirection:'row',paddingLeft:20,paddingBottom:15,alignItems:'center'}}>
          <Image source={{uri:this.props.route.params.data.Shop.awsUrl}} style={{ width: 24, height: 24, borderRadius:20 }} resizeMode={'cover'}/>
          <Text style={[styles.menutxt,styles.fs16,styles.pl10,styles.pr20,styles.ffb,styles.colorlightblack]}>{this.props.route.params?.data?.Shop?.name}</Text>
        </View>

        <ScrollView style={{height:200}}>
          <Text style={[styles.menutxt,styles.fs18,styles.pl20,styles.pr20,styles.mb10]}>{this.props.route.params?.data?.name} ( {(this.props.route.params?.data?.price=="1" || this.props.route.params?.data?.price=="333"|| this.props.route.params?.data?.price=="786"|| this.props.route.params?.data?.price=="555"|| this.props.route.params?.data?.price=="666"|| this.props.route.params?.data?.price=="111"|| this.props.route.params?.data?.price=="222"|| this.props.route.params?.data?.price=="123"|| this.props.route.params?.data?.price=="30" || this.props.route.params?.data?.price=="20"  || this.props.route.params?.data?.price=="10" || this.props.route.params?.data?.price=="124" || this.props.route.params?.data?.price=="1234" || this.props.route.params?.data?.price=="12345" || this.props.route.params?.data?.price=="2" || this.props.route.params?.data?.price=="20" || this.props.route.params?.data?.price=="9" || this.props.route.params?.data?.price=="99"|| this.props.route.params?.data?.price=="999"|| this.props.route.params?.data?.price=="9999"|| this.props.route.params?.data?.price=="99999")?'On Call':'Rs '+this.props.route.params?.data?.price+'/-'} )</Text>
          <Text style={[styles.menutxt,styles.fs14,styles.pl20,styles.pr20,styles.ffm]}>{global.lang=='en'?"Category":'قسم'}: {this.props.route.params?.data?.Category?.name}</Text>
          <Text style={[styles.menutxt,styles.fs14,styles.pl20,styles.pr20,styles.ffm]}>{global.lang=='en'?"Brand":'برانڈ'} : {this.props.route.params?.data?.Brand?.name}</Text>
          <Text style={[styles.menutxt,styles.fs14,styles.pl20,styles.pr20,styles.ffm]}>{global.lang=='en'?"Model":'ماڈل'}: {this.props.route.params?.data?.Model?.name}</Text>
          <Text style={[styles.menutxt,styles.fs14,styles.pl20,styles.pr20,styles.ffm]}>{global.lang=='en'?"Year":'سال'} : {this.props.route.params?.data?.year}</Text>
          <Text style={[styles.menutxt,styles.fs14,styles.pl20,styles.pr20,styles.ffm,{marginTop:10}]}>{global.lang=='en'?"Description":'تفصیل'} :</Text>
          <Text style={[styles.menutxt, styles.fs14,styles.pl20,styles.pr20,styles.ffr,styles.colorlightblack]}>{this.props.route.params.data.description}</Text>
        </ScrollView>
        
        <View style={{flexDirection:'row',justifyContent:'space-between',padding: 20}}>
          <TouchableOpacity style={{ backgroundColor:'#1a1a1a',borderRadius:8,width:'48%',padding: 20 }} onPress={()=>{this.props.navigation.push('ChatDetail',{fromChat:false,data:this.props.route.params?.data?.Shop})}}>
            <Text style={[styles.menutxt,styles.fs14,{color:'#fff',textAlign:'center'}]}>{global.lang=='en'?"Chat with Owner":'تاثرات'}</Text>

          </TouchableOpacity>
          <TouchableOpacity style={{ backgroundColor:'#1a1a1a',borderRadius:8,width:'48%',padding: 20, }} onPress={()=>{{this.props.navigation.push("ShopDetails",{shopdata:this.props.route.params?.data?.Shop})}}}>
            <Text style={[styles.menutxt,styles.fs14,{color:'#fff',textAlign:'center'}]}>{global.lang=='en'?"Shop Details":'دکان کی تفصیلات'}</Text>

          </TouchableOpacity>
        </View>
         
        <RBSheet ref={ref => { this.RBSheet1 = ref; }} height={510} openDuration={250}  >
          <View>
            <View style={{ borderBottomColor:'#d9d9d9',borderBottomWidth:1,position: 'relative', }}>
              <Text style={[styles.ffb,styles.fs16,styles.colorblack,styles.p20,{textAlign:'center'}]}>{global.lang=='en'?"Product Reviews":'پروڈکٹ جائزہ'}</Text>
            </View>
            <View style={{flexDirection:'row',paddingLeft:20,paddingBottom:15,paddingTop:10,alignItems:'center',borderBottomColor:'#ddd',borderBottomWidth:1}}>
                <Image source={{uri:this.props.route.params.data.Shop.awsUrl}} style={{ width: 88, height: 88, borderRadius:20 }} resizeMode={'cover'}/>
                <View>
                  <Text style={[styles.menutxt,styles.fs30,styles.pl20,styles.pr20,styles.ffm,styles.colorblack]}>{this.props.route.params.data.rating} / 5.0</Text>
                  <View style={[styles.pl20,{flexDirection:'row' }]}>
                  {(this.props.route.params?.data?.rating==0) && (
                    <>
                    <Image source={require('../../assets/images/star_z.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star_z.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star_z.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star_z.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star_z.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    </>      
                  )}
                  {(this.props.route.params?.data?.rating==1) && (
                    <>
                    <Image source={require('../../assets/images/star.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star_z.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star_z.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star_z.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star_z.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    </>      
                  )}
                  {(this.props.route.params?.data?.rating==2) && (
                    <>
                    <Image source={require('../../assets/images/star.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star_z.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star_z.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star_z.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    </>      
                  )}
                  {(this.props.route.params?.data?.rating==3) && (
                    <>
                    <Image source={require('../../assets/images/star.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star_z.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star_z.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    </>      
                  )}
                  {(this.props.route.params?.data?.rating==4) && (
                    <>
                    <Image source={require('../../assets/images/star.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star_z.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    </>      
                  )}
                  {(this.props.route.params?.data?.rating==5) && (
                    <>
                    <Image source={require('../../assets/images/star.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    </>      
                  )}
                </View>
                <Text style={[styles.menutxt,styles.fs14,styles.pl20,styles.ffb,styles.colorblack,{marginTop:3}]}>{this.props.route.params?.data?.Reviews.length} {global.lang=='en'?"Ratings":'درجہ بندی'}</Text>
              </View>
            </View>
           
            <ScrollView contentContainerStyle={{padding:20}} style={{height:260}}>
              {this.props.route.params?.data?.Reviews.map((item,index)=>{
              return(
              <View key={index} style={{borderBottomColor:'#ddd',borderBottomWidth:1,paddingBottom:15}}>
              <View style={{flexDirection:'row',paddingLeft:10,paddingBottom:15,alignItems:'center',paddingTop:15,justifyContent:'space-between'}}>
                <View style={{}}>
                  {/* <Image source={{uri:this.props.route.params.data.Reviews.awsUrl}} style={{ width: 27, height: 27, borderRadius:20 }} resizeMode={'cover'}/> */}
                  <Text style={[styles.menutxt,styles.fs14,styles.pr20,styles.ffb,styles.colorblack]}>{item.User.fullName}</Text>
                  <Text style={[styles.menutxt,styles.fs12,styles.pr20,styles.ffm,styles.colorblack]}>{item.User.phoneNumber}</Text>
                </View>
                <View style={[styles.pl20,{flexDirection:'row' }]}>
                  {(item.rating==0) && (
                    <>
                    <Image source={require('../../assets/images/star_z.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star_z.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star_z.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star_z.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star_z.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    </>      
                  )}
                  {(item.rating==1) && (
                    <>
                    <Image source={require('../../assets/images/star.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star_z.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star_z.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star_z.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star_z.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    </>      
                  )}
                  {(item.rating==2) && (
                    <>
                    <Image source={require('../../assets/images/star.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star_z.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star_z.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star_z.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    </>      
                  )}
                  {(item.rating==3) && (
                    <>
                    <Image source={require('../../assets/images/star.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star_z.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star_z.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    </>      
                  )}
                  {(item.rating==4) && (
                    <>
                    <Image source={require('../../assets/images/star.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star_z.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    </>      
                  )}
                  {(item.rating==5) && (
                    <>
                    <Image source={require('../../assets/images/star.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    <Image source={require('../../assets/images/star.png')} style={{ width: 16, height: 16,marginRight:4 }} resizeMode={'contain'}/>
                    </>      
                  )}
                </View>


              </View>
              <Text style={[styles.menutxt,styles.fs12,styles.pl10,styles.pr20,styles.ffl,styles.colorblack]}>{item.comment}</Text>
            </View>
              )
              })}
            </ScrollView>
            
            <View style={{paddingHorizontal:20}}>
              <TouchableOpacity onPress={()=>{this.RBSheet1.close();setTimeout(() => {
                 this.RBSheet2.open();
              }, 200);}}>
                  <View style={[styles.bgcoloryellow,styles.br10,styles.p20,{position:'relative'}]}>
                    <Text style={[styles.colorblack,styles.ffm,styles.fs16,{alignSelf:'center'}]}>{global.lang=='en'?"Add Product Review":'تاثمصنوعات کا جائزہ لیںرات'}</Text>
                  </View>
              </TouchableOpacity>
            </View>
          </View>
        </RBSheet>
      
        <RBSheet ref={ref => { this.RBSheet2 = ref; }} height={510} openDuration={250}  >
          <ScrollView>
            <View style={{ borderBottomColor:'#d9d9d9',borderBottomWidth:1,position: 'relative', }}>
              <Text style={[styles.ffb,styles.fs16,styles.colorblack,styles.p20,{textAlign:'center'}]}>{global.lang=='en'?"Feedback":'آراء'}</Text>
            </View>
            <View style={{paddingBottom:15,paddingTop:10,alignItems:'center'}}>
                <Image source={{uri:this.props.route.params.data.Shop.awsUrl}} style={{ width: 88, height: 88, borderRadius:20 }} resizeMode={'cover'}/>
                <Text style={[styles.menutxt,styles.fs20,styles.pl20,styles.pr20,styles.ffb,styles.colorblack,{marginTop:15,textAlign:'center'}]}>{global.lang=='en'?"How was your Experience?":'آپ کا تجربہ کیسا رہا؟'}</Text>
                <Text style={[styles.menutxt,styles.fs14,styles.pl20,styles.ffb,styles.colorblack,{marginTop:5,marginHorizontal:30, textAlign:'center'}]}>{global.lang=='en'?"Your feedback really mean to us and for everybody on zoha platform":'آپ کے تاثرات کا واقعی ہمارے اور زوہا پلیٹ فارم پر موجود ہر شخص کے لئے معنی ہے'}</Text>
                  
                <View style={{textAlign:'center',alignItems:'center',justifyContent:'center',paddingTop:15}}>
                  
                  <View style={[styles.pl20,{flexDirection:'row' }]}>
                  {(this.state.rating==0) && (
                    <>
                    <TouchableOpacity onPress={()=>{this.setState({rating:1})}}>
                      <Image source={require('../../assets/images/star_z.png')} style={{ width: 27, height: 27,marginRight:4 }} resizeMode={'contain'}/>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>{this.setState({rating:2})}}>
                      <Image source={require('../../assets/images/star_z.png')} style={{ width: 27, height: 27,marginRight:4 }} resizeMode={'contain'}/>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>{this.setState({rating:3})}}>
                      <Image source={require('../../assets/images/star_z.png')} style={{ width: 27, height: 27,marginRight:4 }} resizeMode={'contain'}/>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>{this.setState({rating:4})}}>
                      <Image source={require('../../assets/images/star_z.png')} style={{ width: 27, height: 27,marginRight:4 }} resizeMode={'contain'}/>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>{this.setState({rating:5})}}>
                      <Image source={require('../../assets/images/star_z.png')} style={{ width: 27, height: 27,marginRight:4 }} resizeMode={'contain'}/>
                    </TouchableOpacity>
                  </>      
                  )}
                  {(this.state.rating==1) && (
                    <>
                      <TouchableOpacity onPress={()=>{this.setState({rating:1})}}>
                        <Image source={require('../../assets/images/star.png')} style={{ width: 27, height: 27,marginRight:4 }} resizeMode={'contain'}/>
                      </TouchableOpacity>
                      <TouchableOpacity onPress={()=>{this.setState({rating:2})}}>
                        <Image source={require('../../assets/images/star_z.png')} style={{ width: 27, height: 27,marginRight:4 }} resizeMode={'contain'}/>
                      </TouchableOpacity>
                      <TouchableOpacity onPress={()=>{this.setState({rating:3})}}>
                        <Image source={require('../../assets/images/star_z.png')} style={{ width: 27, height: 27,marginRight:4 }} resizeMode={'contain'}/>
                      </TouchableOpacity>
                      <TouchableOpacity onPress={()=>{this.setState({rating:4})}}>
                        <Image source={require('../../assets/images/star_z.png')} style={{ width: 27, height: 27,marginRight:4 }} resizeMode={'contain'}/>
                      </TouchableOpacity>
                      <TouchableOpacity onPress={()=>{this.setState({rating:5})}}>
                        <Image source={require('../../assets/images/star_z.png')} style={{ width: 27, height: 27,marginRight:4 }} resizeMode={'contain'}/>
                      </TouchableOpacity>
                    </>      
                  )}
                  {(this.state.rating==2) && (
                    <>
                      <TouchableOpacity onPress={()=>{this.setState({rating:1})}}>
                        <Image source={require('../../assets/images/star.png')} style={{ width: 27, height: 27,marginRight:4 }} resizeMode={'contain'}/>
                      </TouchableOpacity>
                      <TouchableOpacity onPress={()=>{this.setState({rating:2})}}>
                        <Image source={require('../../assets/images/star.png')} style={{ width: 27, height: 27,marginRight:4 }} resizeMode={'contain'}/>
                      </TouchableOpacity>
                      <TouchableOpacity onPress={()=>{this.setState({rating:3})}}>
                        <Image source={require('../../assets/images/star_z.png')} style={{ width: 27, height: 27,marginRight:4 }} resizeMode={'contain'}/>
                      </TouchableOpacity>
                      <TouchableOpacity onPress={()=>{this.setState({rating:4})}}>
                        <Image source={require('../../assets/images/star_z.png')} style={{ width: 27, height: 27,marginRight:4 }} resizeMode={'contain'}/>
                      </TouchableOpacity>
                      <TouchableOpacity onPress={()=>{this.setState({rating:5})}}>
                        <Image source={require('../../assets/images/star_z.png')} style={{ width: 27, height: 27,marginRight:4 }} resizeMode={'contain'}/>
                      </TouchableOpacity>
                    </>      
                  )}
                  {(this.state.rating==3) && (
                    <>
                      <TouchableOpacity onPress={()=>{this.setState({rating:1})}}>
                        <Image source={require('../../assets/images/star.png')} style={{ width: 27, height: 27,marginRight:4 }} resizeMode={'contain'}/>
                      </TouchableOpacity>
                      <TouchableOpacity onPress={()=>{this.setState({rating:2})}}>
                        <Image source={require('../../assets/images/star.png')} style={{ width: 27, height: 27,marginRight:4 }} resizeMode={'contain'}/>
                      </TouchableOpacity>
                      <TouchableOpacity onPress={()=>{this.setState({rating:3})}}>
                        <Image source={require('../../assets/images/star.png')} style={{ width: 27, height: 27,marginRight:4 }} resizeMode={'contain'}/>
                      </TouchableOpacity>
                      <TouchableOpacity onPress={()=>{this.setState({rating:4})}}>
                        <Image source={require('../../assets/images/star_z.png')} style={{ width: 27, height: 27,marginRight:4 }} resizeMode={'contain'}/>
                      </TouchableOpacity>
                      <TouchableOpacity onPress={()=>{this.setState({rating:5})}}>
                        <Image source={require('../../assets/images/star_z.png')} style={{ width: 27, height: 27,marginRight:4 }} resizeMode={'contain'}/>
                      </TouchableOpacity>
                    </>      
                  )}
                  {(this.state.rating==4) && (
                    <>
                      <TouchableOpacity onPress={()=>{this.setState({rating:1})}}>
                        <Image source={require('../../assets/images/star.png')} style={{ width: 27, height: 27,marginRight:4 }} resizeMode={'contain'}/>
                      </TouchableOpacity>
                      <TouchableOpacity onPress={()=>{this.setState({rating:2})}}>
                        <Image source={require('../../assets/images/star.png')} style={{ width: 27, height: 27,marginRight:4 }} resizeMode={'contain'}/>
                      </TouchableOpacity>
                      <TouchableOpacity onPress={()=>{this.setState({rating:3})}}>
                        <Image source={require('../../assets/images/star.png')} style={{ width: 27, height: 27,marginRight:4 }} resizeMode={'contain'}/>
                      </TouchableOpacity>
                      <TouchableOpacity onPress={()=>{this.setState({rating:4})}}>
                        <Image source={require('../../assets/images/star.png')} style={{ width: 27, height: 27,marginRight:4 }} resizeMode={'contain'}/>
                      </TouchableOpacity>
                      <TouchableOpacity onPress={()=>{this.setState({rating:5})}}>
                        <Image source={require('../../assets/images/star_z.png')} style={{ width: 27, height: 27,marginRight:4 }} resizeMode={'contain'}/>
                      </TouchableOpacity>
                    </>      
                  )}
                  {(this.state.rating==5) && (
                    <>
                      <TouchableOpacity onPress={()=>{this.setState({rating:1})}}>
                        <Image source={require('../../assets/images/star.png')} style={{ width: 27, height: 27,marginRight:4 }} resizeMode={'contain'}/>
                      </TouchableOpacity>
                      <TouchableOpacity onPress={()=>{this.setState({rating:2})}}>
                        <Image source={require('../../assets/images/star.png')} style={{ width: 27, height: 27,marginRight:4 }} resizeMode={'contain'}/>
                      </TouchableOpacity>
                      <TouchableOpacity onPress={()=>{this.setState({rating:3})}}>
                        <Image source={require('../../assets/images/star.png')} style={{ width: 27, height: 27,marginRight:4 }} resizeMode={'contain'}/>
                      </TouchableOpacity>
                      <TouchableOpacity onPress={()=>{this.setState({rating:4})}}>
                        <Image source={require('../../assets/images/star.png')} style={{ width: 27, height: 27,marginRight:4 }} resizeMode={'contain'}/>
                      </TouchableOpacity>
                      <TouchableOpacity onPress={()=>{this.setState({rating:5})}}>
                        <Image source={require('../../assets/images/star.png')} style={{ width: 27, height: 27,marginRight:4 }} resizeMode={'contain'}/>
                      </TouchableOpacity>
                    </>      
                  )}
                </View>
              </View>
            </View>
            <View style={{paddingHorizontal:20,paddingBottom:10}}>
              <Text style={[styles.menutxt,styles.fs10,styles.ffr,{padding:8}]}>{global.lang=='en'?"Say something about the product":'مصنوع کے بارے میں کچھ کہنا'}</Text>
              <TextInput style={{ width:'100%',height:100, backgroundColor:'#f2f2f2',paddingTop:10,
                borderRadius: 10,paddingHorizontal: 10, fontSize: 13,color:'black',textAlignVertical: 'top'
                }}
                value={this.state.comment}
                onChangeText={(val)=>this.setState({comment:val})}
                multiline={true}
                numberOfLines={5}
                placeholder={'Write Here …'}
                placeholderTextColor={'#7c7c7c'}
              />
            </View>
            <View style={{paddingHorizontal:20}}>
              <TouchableOpacity onPress={()=>{this.addReview();}}>
                  <View style={[styles.bgcoloryellow,styles.br10,styles.p20,{position:'relative'}]}>
                    <Text style={[styles.colorblack,styles.ffm,styles.fs16,{alignSelf:'center'}]}>{global.lang=='en'?"Submit Review":'جائزہ پیش کریں'}</Text>
                  </View>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </RBSheet>

        <Modal visible={this.state.showOverlay} transparent={true} onRequestClose={() => this.setState({ showOverlay: false })}>
          <TouchableOpacity onPress={()=>{this.setState({showOverlay:false})}} style={{position:'absolute',right:0,zIndex:99999}}>
            <Image source={require('../../assets/images/close.png')} style={[{ width: 30, height: 30,margin:20}]} resizeMode={'contain'}/>
          </TouchableOpacity>
          <ImageZoom
            panToMove={true}
            pinchToZoom={true}
            onSwipeDown={()=>{this.setState({showOverlay:false})}}
            style={{backgroundColor:'black'}} 
            cropWidth={Dimensions.get('window').width}
            cropHeight={Dimensions.get('window').height}
            imageWidth={Dimensions.get('window').width}
            imageHeight={Dimensions.get('window').height}>
              <FastImage
                style={{width:Dimensions.get('window').width, height:Dimensions.get('window').height ,resizeMode:'contain'}}
                source={{uri:this.state.selectedImage}}
                resizeMode={FastImage.resizeMode.contain}
              />
          </ImageZoom>
        </Modal>
      </SafeAreaView>
    
    );
  }
}