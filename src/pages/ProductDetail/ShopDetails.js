import React from 'react'
import { SafeAreaView, TouchableOpacity, Text, Image, View, ScrollView,Linking } from 'react-native'
import styles from '../../assets/css/styles';
import Share from '../../assets/images/phone.png';
import BackButtonComponent from '../../components/BackButtonComponent';
import axios from 'axios';
import { Overlay } from 'react-native-elements';
export default class ShopDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      productdata:[],
      servicesdata:[],
      showOverlay:false,
      segment:1
    };
    console.log('====================================');
    console.log(this.props.route.params?.shopdata);
    console.log('====================================');
    this.getShopProducts();
    this.getShopServices();
  }
  toggleOverlay = () => {
    this.setState({showOverlay:!this.state.showOverlay})
  }
  onCall=()=>{
    Linking.openURL(`tel:+${this.props.route.params?.shopdata.phoneNumber}`)
  }
  getShopProducts=()=>{
    var header={
      "Content-Type":"application/json",
      "Authorization":global.usertoken
    }
    axios.get( global.url + '/api/product/'+this.props.route.params?.shopdata?.id+'?offset=0&limit=900',{headers:header}).then(response => {
      console.log(response.data);
      this.setState({productdata:response.data.result.data});
    }).catch(error => {
      alert(error.response.data.message);
    });
  }
  getShopServices=()=>{
    var header={
      "Content-Type":"application/json",
      "Authorization":global.usertoken
    }
    axios.get( global.url + '/api/service?offset=0&limit=900&&shopId='+this.props.route.params?.shopdata?.id,{headers:header}).then(response => {
      console.log(response.data);
      this.setState({servicesdata:response.data.result.data});
    }).catch(error => {
      alert(error.response.data.message);
    });
  }
  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={{flexDirection:"row",justifyContent:'space-between',paddingRight:20,borderBottomWidth: 1, borderBottomColor: 'rgb(230,230,230)',paddingBottom:10}}>
          <BackButtonComponent navigation={this.props.navigation}/>
          <View  style={{flexDirection:"row",justifyContent:'center'}}>
            <Text style={[styles.menutxt,styles.mt20, styles.fs16]}>{global.lang=='en'?"Shop Details":'دکان کی تفصیلات'}</Text>
          </View>
          <TouchableOpacity style={{ marginTop: 20 , marginLeft:0 }} onPress={()=>{this.onCall()}}>
              <Image source={Share} style={{ width: 24, height: 24 }} resizeMode={'contain'}/>
          </TouchableOpacity>
        </View>
        <View style={[styles.p20,{flexDirection:'row',justifyContent:'space-between'}]}>
          <TouchableOpacity onPress={()=>{this.setState({showOverlay:true});}}>
            <Image source={{uri:this.props.route.params?.shopdata?.awsUrl}} style={{ width: 80, height: 80, borderRadius:10 }} resizeMode={'cover'}/>
          </TouchableOpacity>
            <View style={{marginRight:5,justifyContent:'flex-end'}}>
              <Text style={[styles.menutxt,styles.fs14,styles.pl20,styles.ffb,styles.colorblack,{marginTop:3}]}>{this.props.route.params?.shopdata?.statusChar}</Text>
              <Text style={[styles.menutxt,styles.fs12,styles.pl20,styles.ffm,styles.colorlightblack,{marginTop:3}]}>{global.lang=='en'?"Status":'حالت'}</Text>
            </View>
            <View style={{marginRight:5,justifyContent:'flex-end'}}>
              <Text style={[styles.menutxt,styles.fs14,styles.pl10,styles.ffb,styles.colorblack,{marginTop:3}]}>{this.props.route.params?.shopdata?.type}</Text>
              <Text style={[styles.menutxt,styles.fs12,styles.pl10,styles.ffm,styles.colorlightblack,{marginTop:3}]}>{global.lang=='en'?"Type":'ٹائپ'}</Text>
            </View>
          <TouchableOpacity style={{marginLeft:0}} onPress={()=>{this.props.navigation.push('ChatDetail',{fromChat:false,data:this.props.route.params?.shopdata})}}>
            <Image source={require('../../assets/images/msg.png')} style={{ width: 28, height: 28 }} resizeMode={'contain'}/>
          </TouchableOpacity>
        </View>
        <View>
          <Text style={[styles.menutxt,styles.fs16,styles.pl20,styles.pr20,styles.ffm,styles.colorlightblack,{marginTop:3}]}>{global.lang=='en'?"Address":'پتہ'}</Text>
          <Text style={[styles.menutxt,styles.fs14,styles.pl20,styles.pr20,styles.ffb,styles.colorblack,{marginTop:3}]}>{this.props.route.params?.shopdata?.address}</Text>
          <Text style={[styles.mt10,styles.menutxt,styles.fs22,styles.pl20,styles.pr20]}>{this.props.route.params?.shopdata?.name}</Text>
          <ScrollView style={[styles.mt10,{maxHeight:70}]}>
            <Text style={[styles.menutxt, styles.fs12,styles.pl20,styles.pr20,styles.ffm,styles.colorlightblack]}>{this.props.route.params?.shopdata?.description}</Text>
          </ScrollView>
        </View>
        <View style={{flexDirection:'row',justifyContent:'space-evenly',marginTop:10}}>
          <TouchableOpacity onPress={()=>{this.setState({segment:1})}} style={{padding:10,
              backgroundColor:this.state.segment==1?'#f0ad34':'#cccccc'}}>
            <Text style={[styles.menutxt,styles.fs18,styles.pl20,styles.pr20]}>Products</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>{this.setState({segment:2})}} style={{padding:10,
              backgroundColor:this.state.segment==2?'#f0ad34':'#cccccc'}}>
            <Text style={[styles.menutxt,styles.fs18,styles.pl20,styles.pr20]}>Services</Text>
          </TouchableOpacity>
        </View>
        {this.state.segment==1 && (
          <ScrollView>
            <View style={{flexDirection:'row',justifyContent:'space-between',flexWrap:'wrap',padding: 20,}}>
              {this.state.productdata.length==0 && (
                <Text style={[styles.mt10,styles.menutxt,styles.fs16,styles.pl20,styles.pr20]}>No Product added Yet</Text>
              )}
              {(this.state.productdata.map((item,index)=>{
                return(
                  <TouchableOpacity style={styles.homeview} key={index} onPress={()=>{this.props.navigation.push('ProductDetail',{data:item})}}>  
                    <Image source={{uri:item.resources[0].awsUrl}} style={[styles.homeimg,{ width: '100%', height: 202 }]} resizeMode={'cover'}/>
                    <View style={{marginLeft:10,marginRight:10}}>
                      <Text style={styles.hometxt}>{item.Shop.name}</Text>
                      <Text style={styles.hometxt2}>{item.name}</Text>
                      <Text style={styles.hometxt3}>Rs {item.price}/-</Text>
                    </View>
                  </TouchableOpacity>
                )
              }))}
            </View>
          </ScrollView>
        )}
        {this.state.segment==2 && (
          <ScrollView>
            <View style={{flexDirection:'row',justifyContent:'space-between',flexWrap:'wrap',padding: 20,}}>
              {this.state.servicesdata.length==0 && (
                <Text style={[styles.mt10,styles.menutxt,styles.fs16,styles.pl20,styles.pr20]}>No Service added Yet</Text>
              )}
              {(this.state.servicesdata.map((item,index)=>{
                return(
                  <TouchableOpacity style={styles.homeview} key={index} onPress={()=>{this.props.navigation.push('ServiceDetail',{data:item})}}>  
                    <Image source={{uri:item.resources[0].awsUrl}} style={[styles.homeimg,{ width: '100%', height: 150 }]} resizeMode={'cover'}/>
                    <View style={{marginLeft:10,marginRight:10}}>
                      <Text style={styles.hometxt2}>{item.ServiceCategory.name}</Text>
                      <Text style={styles.hometxt2}>Brand : {item.Brand.name}</Text>
                      <Text style={[styles.hometxt2,styles.pb10]}>Model : {item.Model.name}</Text>
                    </View>
                  </TouchableOpacity>
                )
              }))}
            </View>
          </ScrollView>
        )}
        
        <Overlay overlayStyle={{padding:0,borderRadius:8,}} isVisible={this.state.showOverlay} onBackdropPress={this.toggleOverlay}>
          <View style={{width:300,overflow: 'hidden',}}>
            <Image source={{uri:this.props.route.params?.shopdata?.awsUrl}} style={[{ width: 300, height: 300,}]} resizeMode={'contain'}/>
          </View>
        </Overlay>
      </SafeAreaView>
    
    );
  }
}
