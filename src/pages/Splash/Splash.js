import React, { Component } from 'react';
import { View, Text,Platform ,PermissionsAndroid} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import { SafeAreaView } from 'react-native';
import { Image } from 'react-native';
import messaging from '@react-native-firebase/messaging';
import { Alert } from 'react-native';
async function requestUserPermission() {
  const authStatus = await messaging().requestPermission();
  const enabled = authStatus === messaging.AuthorizationStatus.AUTHORIZED || authStatus === messaging.AuthorizationStatus.PROVISIONAL;
  console.log(messaging.AuthorizationStatus);
  if (Platform.OS=='ios' && enabled) {
    console.log('Authorization status:', authStatus);
    global.fbtoken = await messaging().getToken();
  }
  if (Platform.OS=='android' && authStatus) {
    console.log("Permission settings:", authStatus);
    global.fbtoken = await messaging().getToken();
  }
  
}

export default class Splash extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
    requestUserPermission();
    this.handleNotification()
    this.permissionAndroid();
    AsyncStorage.getItem("lang").then((lang) => {
      console.log('lang',lang);
      if(lang==null){
        global.lang = 'en';
      }
      else{
        global.lang = lang;
      }
      
    });
    AsyncStorage.getItem("userToken").then((usertoken) => {
        console.log('========usertoken============================');
        console.log(usertoken);
        console.log('==========usertoken==========================');
        if (usertoken != null) {
            global.usertoken = usertoken;
            var header={
              "Authorization":usertoken
            }
            AsyncStorage.getItem("userRole").then((role) => {
                global.userrole = role;
                if (role == 'buyer') {
                    this.props.navigation.replace("Buyer");
                } else {
                  axios.get( global.url + '/api/user/self',{headers:header} ).then(response => {
                    if(response.data!=undefined){
                      if(response.data.result.data.Shop==null){
                        this.props.navigation.replace('SellerProfile',{phone:response.data.result.data.phoneNumber});
                      }
                      else{
                        this.props.navigation.replace('Seller');
                      }
                    }
                    else{
                      global.usertoken = null;
                      global.userrole = null;
                      this.props.navigation.replace("RoleSelection");
                    }
                  }).catch(error => {
                    global.usertoken = null;
                    global.userrole = null;
                    this.props.navigation.replace("RoleSelection");
                    console.log(error.response);
                  });
                }
            });
        } 
        else {
          global.usertoken = null;
          global.userrole = null;
          this.props.navigation.replace("RoleSelection");
        }
    });
  }
  handleNotification = () => {
    messaging().onNotificationOpenedApp((remoteMessage) => {
      console.log(remoteMessage);
      var addressComponent;
      if (remoteMessage) {
        if(remoteMessage.data.buyerName){
          Geocoder.from(remoteMessage?.data?.lattitude, remoteMessage?.data?.longitude).then(json => {
            addressComponent = json.results[0].formatted_address;
            console.log(addressComponent);
          })
          Alert.alert('Emergency',` Buyer Name: ${remoteMessage.data.buyerName} \n Category: ${remoteMessage.data.category} \n Brand: ${remoteMessage.data.brand} \n Model: ${remoteMessage.data.model} \n comment: ${remoteMessage.data.comment}`,
          [
            {
              text: "Cancel",
              onPress: ()=>{}
            },
            {
              text: 'Call',
              onPress: () => {
                Linking.openURL(`tel:+${remoteMessage.data.phoneNumber}`)
              }
            },
            {
              text: 'Locate',
              onPress: () => {
                openMap({
                  provider:'google',
                  travelType:'drive',
                  end:addressComponent,
                  navigate:true
                });
              }
            }
    
        ])
        }
      }
    });
    messaging().getInitialNotification().then((remoteMessage) => {
      var addressComponent1;
      if (remoteMessage) {
        if(remoteMessage.data.buyerName){
          Geocoder.from(remoteMessage?.data?.lattitude, remoteMessage?.data?.longitude).then(json => {
        		addressComponent1 = json.results[0].formatted_address;
			      console.log(addressComponent1);
		      }).catch(error => console.warn(error));
          Alert.alert('Emergency',` Buyer Name: ${remoteMessage.data.buyerName} \n Category: ${remoteMessage.data.category} \n Brand: ${remoteMessage.data.brand} \n Model: ${remoteMessage.data.model} \n comment: ${remoteMessage.data.comment}`,
          [
            {
              text: "Cancel",
              onPress: ()=>{}
            },
            {
              text: 'Call',
              onPress: () => {
                Linking.openURL(`tel:+${remoteMessage.data.phoneNumber}`)
              }
            },
            {
              text: 'Locate',
              onPress: () => {
                openMap({
                  provider:'google',
                  travelType:'drive',
                  end:addressComponent1,
                  navigate:true
                });
              }
            }
    
        ])
        }
      }
    });
  };
  permissionAndroid=async()=>{
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'Permissions for write access',
            message: 'Give permission to your storage to write a file',
            buttonPositive: 'ok',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log('You can use the storage');
        } else {
          console.log('permission denied');
          return;
        }
      } catch (err) {
        console.warn(err);
        return;
      }
    }
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
          {
            title: 'Permissions for write access',
            message: 'Give permission to your storage to write a file',
            buttonPositive: 'ok',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log('You can use the camera');
        } else {
          console.log('permission denied');
          return;
        }
      } catch (err) {
        console.warn(err);
        return;
      }
    }
  }
  render() {
    return (
      <SafeAreaView>
        <Image source={require('../../assets/images/splash.png')} style={[{ width: '100%', height: '100%' }]} resizeMode={'cover'}/>
      </SafeAreaView>
    );
  }
}
