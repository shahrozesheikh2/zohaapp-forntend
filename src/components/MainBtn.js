import React, { Component } from 'react';
import { View, Text , TouchableOpacity} from 'react-native';
import styles from '../assets/css/styles';

export default class MainBtn extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
        <View style={[styles.bgcolorblue,styles.br30,styles.p20,styles.mt10]}>
            <Text style={[styles.colorwhite,styles.ffm,styles.fs16,{alignSelf:'center'}]}>{this.props.text}</Text>
        </View>
            
    );
  }
}
