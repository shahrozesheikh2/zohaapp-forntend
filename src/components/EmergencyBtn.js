import React from 'react'
import { TextInput, Dimensions, TouchableOpacity, Text, Image, View, ScrollView } from 'react-native';
import axios from 'axios';
import RBSheet from "react-native-raw-bottom-sheet";
import DropDownPicker from 'react-native-dropdown-picker';
import GetLocation from 'react-native-get-location';
import styles from '../assets/css/styles';

export default class EmergencyBtn extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          servicecategorydata:[],
          branddata:[],
          modeldatae:[],
          servicecategoryide:'',
          brandide:'',
          modelide:'',
          servicecategoryidename:'',
          brandidename:'',
          modelidename:'',
          comments:'',
          long:'',
          lat:'',
          userdata:''
        };
      }
    componentDidMount(){
        this.getuserdata();
        this.getbrand();
        this.getservicecategory();
    }
    getbrand=()=>{
        const url = global.url+'/api/brand?offset=0&limit=100';
        axios.get(url).then(response => {
            console.log('====================================');
            console.log(response.data.result.data);
            console.log('====================================');
            this.setState({branddata:response.data.result.data})
        }).catch(err=>{
        console.log(err)
        });
      }
    getservicecategory=()=>{
        const url = global.url+'/api/service-category?offset=0&limit=100';
        axios.get(url).then(response => {
            console.log('==============servicecategorydata======================');
            console.log(response.data.result.data);
            console.log('==============servicecategorydata======================');
            this.setState({servicecategorydata:response.data.result.data})
        }).catch(err=>{
        console.log(err)
        });
    }
    getmodele=()=>{
        const url = global.url+'/api/model?offset=0&limit=100&id='+this.state.brandide;
        axios.get(url).then(response => {
            this.setState({modeldatae:response.data.result.data})
        }).catch(err=>{
        console.log(err)
        });
    }
    getuserdata=()=>{
        var header={
            "Authorization":global.usertoken
        }
        axios.get( global.url + '/api/user/self',{headers:header} ).then(response => {
            console.log(response.data);
            this.setState({userdata:response.data.result.data})
        }).catch(error => {
            console.log(error.response);
        });
    }
    emergency=()=>{
        GetLocation.getCurrentPosition({
            enableHighAccuracy: true,
            timeout: 15000,
        }).then(location => {
            console.log(location);
            this.setState({long:location.longitude,lat:location.latitude})
            setTimeout(() => {
                var header={
                "Content-Type":"application/json",
                "Authorization":global.usertoken
                }
                var body={
                "lattitude": this.state.lat,
                "longitude": this.state.long,
                "buyerName": this.state.userdata.fullName,
                "phoneNumber": this.state.userdata.phoneNumber,
                "category": this.state.servicecategoryidename,
                "brand": this.state.brandidename,
                "model": this.state.modelidename,
                "comment": this.state.comments?this.state.comments:undefined
                }
                console.log(body);
                if(this.state.servicecategoryidename==''){
                    alert('Enter Select Service Category')
                }
                else if(this.state.brandidename==''){
                alert('Please Select Brand')
                }
                else if(this.state.modelidename==''){
                alert('Please Select Model')
                }
                else{
                axios.post( global.url + '/api/shop/send-emergency',body,{headers:header} ).then(response => {
                console.log(response.data);
                alert(response.data.message);
                }).catch(error => {
                console.log(error.response);
                alert(error.response.data.message);
                });
                }
            }, 100);
        }).catch(error => {
            const { code, message } = error;
            console.warn(code, message);
            this.setState({long:'',lat:''})
            alert('Turn on your location to see services nearby');
        })
    }
    render() {
        return (
            <View>
                <TouchableOpacity onPress={()=>{this.EmergencySheet.open()}} style={[styles.shadow,{backgroundColor:'#fff',padding:10,borderRadius:50,position: 'absolute',bottom:15,right:15}]}>
                    <Image resizeMode={'contain'} source={ require('../assets/images/emergencyicon.png') } style={{width:35,height:35 }} />
                </TouchableOpacity>
                <RBSheet ref={ref => { this.EmergencySheet = ref; }} height={450} openDuration={250}  >
                    <View>
                        <View style={{ borderBottomColor:'#d9d9d9',borderBottomWidth:1,position: 'relative', }}>
                        <Text style={[styles.ffb,styles.fs16,styles.colorblack,styles.p20,{textAlign:'center'}]}>Emergency</Text>
                        <View style={{position: 'absolute',right:0}}>
                        </View>
                        </View>
                        <ScrollView contentContainerStyle={{padding:20,paddingBottom:100}}>
                        <DropDownPicker
                            items={[
                                ...(() => {
                                return this.state.servicecategorydata.length > 0
                                    ? [{
                                    label: "Select Service Category",
                                    value: ""
                                    },...this.state.servicecategorydata.map((item) => {
                                        return {
                                        label: item.name,
                                        value: item.id,
                                        };
                                    })]
                                    : [
                                        {
                                        label: "No Service Category",
                                        value: "",
                                        },
                                    ];
                                })(),
                            ]}
                            placeholder="Select Service Category"
                            placeholderStyle={{
                                color:"#333"
                            }}
                            style={{backgroundColor: '#fff'}}
                            selectedLabelStyle={[{color:'#363636'},styles.fs12,styles.ffm]}
                            itemStyle={{
                                justifyContent: 'center'
                            }}
                            arrowSize={22}
                            arrowColor="#333"
                            style={[{width:Dimensions.get('screen').width-40,marginBottom:10}]}
                            dropDownStyle={[{widhth:Dimensions.get('screen').width-40}]}
                            onChangeItem={item => {
                                console.log(item);
                                this.setState({servicecategoryide:item.value,servicecategoryidename:item.label})
                            }}
                            zIndex={9999999}
                        />
                        <DropDownPicker
                            items={[
                                ...(() => {
                                return this.state.branddata.length > 0
                                    ? [{
                                    label: "Select Brand",
                                    value: ""
                                    },...this.state.branddata.map((item) => {
                                        return {
                                        label: item.name,
                                        value: item.id,
                                        };
                                    })]
                                    : [
                                        {
                                        label: "No Brand",
                                        value: "",
                                        },
                                    ];
                                })(),
                            ]}
                            placeholder="Select Brand"
                            placeholderStyle={{
                                color:"#333"
                            }}
                            style={{backgroundColor: '#fff'}}
                            selectedLabelStyle={[{color:'#363636'},styles.fs12,styles.ffm]}
                            itemStyle={{
                                justifyContent: 'center'
                            }}
                            arrowSize={22}
                            arrowColor="#333"
                            style={[{width:Dimensions.get('screen').width-40,marginBottom:10}]}
                            dropDownStyle={[{widhth:Dimensions.get('screen').width-40}]}
                            onChangeItem={item => {
                                console.log(item);
                                this.setState({brandide:item.value,brandidename:item.label});
                                setTimeout(() => {
                                    this.getmodele();
                                }, 500);
                                
                            }}
                            zIndex={9999999}
                        />
                        <DropDownPicker
                            items={[
                                ...(() => {
                                return this.state.modeldatae.length > 0
                                    ? [{
                                    label: "Select Model",
                                    value: ""
                                    },...this.state.modeldatae.map((item) => {
                                        return {
                                        label: item.name,
                                        value: item.id,
                                        };
                                    })]
                                    : [
                                        {
                                        label: "No Model",
                                        value: "",
                                        },
                                    ];
                                })(),
                            ]}
                            placeholder="Select Model"
                            placeholderStyle={{
                                color:"#333"
                            }}
                            style={{backgroundColor: '#fff'}}
                            selectedLabelStyle={[{color:'#363636'},styles.fs12,styles.ffm]}
                            itemStyle={{
                                justifyContent: 'center'
                            }}
                            arrowSize={22}
                            arrowColor="#333"
                            style={[{width:Dimensions.get('screen').width-40,marginBottom:10}]}
                            dropDownStyle={[{widhth:Dimensions.get('screen').width-40}]}
                            onChangeItem={item => {
                                console.log(item);
                                this.setState({modelide:item.value,modelidename:item.label})
                            }}
                            zIndex={9999999}
                        />
                        <TextInput style={{ width:'100%',height:56, backgroundColor:'#ddd',
                            borderRadius: 10,paddingHorizontal: 10, fontSize: 13,color:'black'
                            }}
                            onChangeText={(val)=>this.setState({comments:val})}
                            placeholder={'Comments….'}
                            placeholderTextColor={'#7c7c7c'}
                        />
                        <TouchableOpacity onPress={()=>{this.emergency();
                        this.EmergencySheet.close();}}>
                            <View style={[styles.bgcoloryellow,styles.br10,styles.p10,styles.mt10,{position:'relative'}]}>
                            <Text style={[styles.colorblack,styles.ffm,styles.fs16,{alignSelf:'center'}]}>Send</Text>
                            </View>
                        </TouchableOpacity>
                        </ScrollView>
                    </View>
                    </RBSheet>
            </View>
            
        )
    }
}
