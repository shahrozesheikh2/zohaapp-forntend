import React, { Component } from 'react'
import { TouchableOpacity, Image } from 'react-native'
import BackArrow from '../assets/images/backarrow.png'
export default class BackButtonComponent extends Component {
    constructor(props) {
        super(props);
    }
    handleBackPress = () => {
        this.props.navigation.goBack();
    };
    render() {
        return (
            <TouchableOpacity style={{ marginTop: 15 , marginLeft:20}} onPress={this.handleBackPress}>
                <Image source={BackArrow} style={{ width: 32, height: 32,marginLeft:-5 }} resizeMode={'contain'}/>
            </TouchableOpacity>
        )
    }
}
