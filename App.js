import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image
} from 'react-native';
import { NavigationContainer, DefaultTheme } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import styles from './src/assets/css/styles';
// pages 
import RoleSelection from './src/pages/RoleSelection/RoleSelection';
import Signin from './src/pages/Signin/Signin';
import Bookmarks from './src/pages/Bookmarks/Bookmarks';
import Chat from './src/pages/Chat/Chat';
import ChatDetail from './src/pages/Chat/ChatDetail';
import Home from './src/pages/Home/Home';
import MyAccount from './src/pages/MyAccount/MyAccount';
import Faqs from './src/pages/MyAccount/Faqs';
import FaqsDetail from './src/pages/MyAccount/FaqsDetail';
import GeneralSettings from './src/pages/MyAccount/GeneralSettings';
import OtpVerification from './src/pages/OtpVerification/OtpVerification';
import Feedback from './src/pages/ProductDetail/Feedback';
import ProductDetail from './src/pages/ProductDetail/ProductDetail';
import ShopDetails from './src/pages/ProductDetail/ShopDetails';
import SellerProfile from './src/pages/SellerProfile/SellerProfile';
import AddItem from './src/pages/SellerProfile/AddItem';
import EditProfile from './src/pages/SellerProfile/EditProfile';
import ShopItem from './src/pages/SellerProfile/ShopItem';
import Splash from './src/pages/Splash/Splash';
import Terms from './src/pages/Terms/Terms';
import EditItem from './src/pages/SellerProfile/EditItem';
import ChangePassword from './src/pages/MyAccount/ChangePassword';
import ChangeCityAndName from './src/pages/MyAccount/ChangeCityAndName';
import OurTeam from './src/pages/MyAccount/OurTeam';
import Services from './src/pages/Services/Services';
import Shops from './src/pages/Shops/Shops';
import ServiceDetail from './src/pages/Services/ServiceDetail';
import ServiceBookmarks from './src/pages/Bookmarks/ServiceBookmarks';
import ShopServiceItem from './src/pages/SellerProfile/ShopServiceItem';
import AddServiceItem from './src/pages/SellerProfile/AddServiceItem';
import EditServiceItem from './src/pages/SellerProfile/EditServiceItem';
import messaging from '@react-native-firebase/messaging';
import { Alert } from 'react-native';
import { Linking } from 'react-native';
import openMap from 'react-native-open-maps';
import AllProducts from './src/pages/Home/AllProducts';
import Geocoder from 'react-native-geocoding';
Geocoder.init("AIzaSyAg1KKPZn6_5ZSmyNpyQ37w3jBS6sXh8vM");
// pages 
const MyTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    background:'#ffffff',
    text:'#31363b',
    primary:'#31363b'
  },
};
const Stack = createStackNavigator();
global.url='http://zohaqaphase2-env.eba-3kera2wc.ap-southeast-1.elasticbeanstalk.com';
const Tab = createBottomTabNavigator();
const tabBarOptions = {
  style: {
    height: 60
  }
};
function handleNotification () {
  messaging().onMessage(async (remoteMessage) => {
    var addressComponent;
    if(remoteMessage.data?.buyerName){
      console.log('for Emergency',remoteMessage);
      Geocoder.from(remoteMessage.data.lattitude, remoteMessage.data.longitude).then(json => {
        		addressComponent = json.results[0].formatted_address;
			      console.log(addressComponent);
      }).catch(error => console.warn(error));
      Alert.alert('Emergency',` Buyer Name: ${remoteMessage.data.buyerName} \n Category: ${remoteMessage.data.category} \n Brand: ${remoteMessage.data.brand} \n Model: ${remoteMessage.data.model} \n comment: ${remoteMessage.data.comment}`,
      [
        {
          text: "Cancel",
          onPress: ()=>{}
        },
        {
          text: 'Call',
          onPress: () => {
            Linking.openURL(`tel:+${remoteMessage.data.phoneNumber}`)
          }
        },
        {
          text: 'Locate',
          onPress: () => {
            openMap({
              provider:'google',
              travelType:'drive',
              end:addressComponent,
              navigate:true
            });
          }
        }

    ])
    }
    
  });
};
function App () {
  handleNotification()
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={{ flex: 1,backgroundColor:'#f4f6fa'}}>
      <NavigationContainer theme={MyTheme}>
        <Stack.Navigator screenOptions={{ headerStyle: { backgroundColor: '#f4f6fa' }, headerBackTitleVisible:false,headerShown:false }}>
          <Stack.Screen name="Splash" component={Splash} />
          <Stack.Screen name="RoleSelection" component={RoleSelection} />
          <Stack.Screen name="Signin" component={Signin} />
          <Stack.Screen name="Buyer" component={Buyer} />
          <Stack.Screen name="Seller" component={Seller} />
          <Stack.Screen name="ChatDetail" component={ChatDetail} />
          <Stack.Screen name="Faqs" component={Faqs} />
          <Stack.Screen name="FaqsDetail" component={FaqsDetail} />
          <Stack.Screen name="GeneralSettings" component={GeneralSettings} />
          <Stack.Screen name="OtpVerification" component={OtpVerification} />
          <Stack.Screen name="Feedback" component={Feedback} />
          <Stack.Screen name="ProductDetail" component={ProductDetail} />
          <Stack.Screen name="ShopDetails" component={ShopDetails} />
          <Stack.Screen name="SellerProfile" component={SellerProfile} />
          <Stack.Screen name="AddItem" component={AddItem} />
          <Stack.Screen name="EditProfile" component={EditProfile} />
          <Stack.Screen name="Terms" component={Terms} />
          <Stack.Screen name="EditItem" component={EditItem} />
          <Stack.Screen name="ChangePassword" component={ChangePassword} />
          <Stack.Screen name="ChangeCityAndName" component={ChangeCityAndName} />
          <Stack.Screen name="OurTeam" component={OurTeam} />
          <Stack.Screen name="Bookmarks" component={Bookmarks} />
          <Stack.Screen name="ServiceDetail" component={ServiceDetail} />
          <Stack.Screen name="ServiceBookmarks" component={ServiceBookmarks} />
          <Stack.Screen name="AddServiceItem" component={AddServiceItem} />
          <Stack.Screen name="EditServiceItem" component={EditServiceItem} />
        </Stack.Navigator>
      </NavigationContainer>
      </SafeAreaView>
    </>
  );
};
function Buyer() {
  return (
    <Tab.Navigator tabBarOptions={tabBarOptions}>
          <Tab.Screen name="Home" component={Home} options={{
          tabBarIcon: ({focused}) => {
            return (
              <Image
                resizeMode={'contain'}
                source={
                  focused ? require('./src/assets/images/home_y.png') : require('./src/assets/images/home.png')
                }
                style={{width:32,height:24,marginTop:8}}
              />
            );
          },
          tabBarLabel: ({focused}) => {
            return (
              <Text center style={[styles.ffb,styles.fs10,{color:focused?'#f9af16':'#666666',marginTop:8,paddingBottom:8}]}>
                Products
              </Text>
            );
          },
          }}/>
          <Tab.Screen name="Services" component={Services} options={{
          tabBarIcon: ({focused}) => {
            return (
              <Image
                resizeMode={'contain'}
                source={
                  focused ? require('./src/assets/images/service_y.png') : require('./src/assets/images/service.png')
                }
                style={{width:32,height:24,marginTop:8}}
              />
            );
          },
          tabBarLabel: ({focused}) => {
            return (
              <Text center style={[styles.ffb,styles.fs10,{color:focused?'#f9af16':'#666666',marginTop:8,paddingBottom:8}]}>
                Services
              </Text>
            );
          },
          }}/>
          <Tab.Screen name="Shops" component={Shops} options={{
          tabBarIcon: ({focused}) => {
            return (
              <Image
                resizeMode={'contain'}
                source={
                  focused ? require('./src/assets/images/shop_y.png') : require('./src/assets/images/shop.png')
                }
                style={{width:32,height:24,marginTop:8}}
              />
            );
          },
          tabBarLabel: ({focused}) => {
            return (
              <Text center style={[styles.ffb,styles.fs10,{color:focused?'#f9af16':'#666666',marginTop:8,paddingBottom:8}]}>
                Shops
              </Text>
            );
          },
          }}/>
          <Tab.Screen name="Chats" component={Chat} options={{
          tabBarIcon: ({focused}) => {
            return (
              <Image
                resizeMode={'contain'}
                source={
                  focused ? require('./src/assets/images/chat_y.png') : require('./src/assets/images/chat.png')
                }
                style={{width:32,height:24,marginTop:8}}
              />
            );
          },
          tabBarLabel: ({focused}) => {
            return (
              <Text center style={[styles.ffb,styles.fs10,{color:focused?'#f9af16':'#666666',marginTop:8,paddingBottom:8}]}>
                Chats
              </Text>
            );
          },
          }}/>
          <Tab.Screen name="Account" component={MyAccount} options={{
          tabBarIcon: ({focused}) => {
            return (
              <Image
                resizeMode={'contain'}
                source={
                  focused ? require('./src/assets/images/account_y.png') : require('./src/assets/images/account.png')
                }
                style={{width:32,height:24,marginTop:8}}
              />
            );
          },
          tabBarLabel: ({focused}) => {
            return (
              <Text center  style={[styles.ffb,styles.fs10,{color:focused?'#f9af16':'#666666',marginTop:8,paddingBottom:8}]}>
                Account
              </Text>
            );
          },
          }}/>
        </Tab.Navigator>
  );
}
function Seller() {
  return (
    <Tab.Navigator tabBarOptions={tabBarOptions}>
          <Tab.Screen name="ShopItem" component={ShopItem} options={{
          tabBarIcon: ({focused}) => {
            return (
              <Image
                resizeMode={'contain'}
                source={
                  focused ? require('./src/assets/images/home_y.png') : require('./src/assets/images/home.png')
                }
                style={{width:32,height:24,marginTop:8}}
              />
            );
          },
          tabBarLabel: ({focused}) => {
            return (
              <Text center style={[styles.ffb,styles.fs10,{color:focused?'#f9af16':'#666666',marginTop:8,paddingBottom:8}]}>
                Products
              </Text>
            );
          },
          }}/>
          <Tab.Screen name="Services" component={ShopServiceItem} options={{
          tabBarIcon: ({focused}) => {
            return (
              <Image
                resizeMode={'contain'}
                source={
                  focused ? require('./src/assets/images/service_y.png') : require('./src/assets/images/service.png')
                }
                style={{width:32,height:24,marginTop:8}}
              />
            );
          },
          tabBarLabel: ({focused}) => {
            return (
              <Text center style={[styles.ffb,styles.fs10,{color:focused?'#f9af16':'#666666',marginTop:8,paddingBottom:8}]}>
                Services
              </Text>
            );
          },
          }}/>

          <Tab.Screen name="AllProducts" component={AllProducts} options={{
          tabBarIcon: ({focused}) => {
            return (
              <Image
                resizeMode={'contain'}
                source={
                  focused ? require('./src/assets/images/home_y.png') : require('./src/assets/images/home.png')
                }
                style={{width:32,height:24,marginTop:8}}
              />
            );
          },
          tabBarLabel: ({focused}) => {
            return (
              <Text center style={[styles.ffb,styles.fs10,{color:focused?'#f9af16':'#666666',marginTop:8,paddingBottom:8}]}>
                All Products
              </Text>
            );
          },
          }}/>
          <Tab.Screen name="Chats" component={Chat} options={{
          tabBarIcon: ({focused}) => {
            return (
              <Image
                resizeMode={'contain'}
                source={
                  focused ? require('./src/assets/images/chat_y.png') : require('./src/assets/images/chat.png')
                }
                style={{width:32,height:24,marginTop:8}}
              />
            );
          },
          tabBarLabel: ({focused}) => {
            return (
              <Text center style={[styles.ffb,styles.fs10,{color:focused?'#f9af16':'#666666',marginTop:8,paddingBottom:8}]}>
                Chats
              </Text>
            );
          },
          }}/>
          
          <Tab.Screen name="Account" component={MyAccount} options={{
          tabBarIcon: ({focused}) => {
            return (
              <Image
                resizeMode={'contain'}
                source={
                  focused ? require('./src/assets/images/account_y.png') : require('./src/assets/images/account.png')
                }
                style={{width:32,height:24,marginTop:8}}
              />
            );
          },
          tabBarLabel: ({focused}) => {
            return (
              <Text center  style={[styles.ffb,styles.fs10,{color:focused?'#f9af16':'#666666',marginTop:8,paddingBottom:8}]}>
                Account
              </Text>
            );
          },
          }}/>
        </Tab.Navigator>
  );
}
export default App;
